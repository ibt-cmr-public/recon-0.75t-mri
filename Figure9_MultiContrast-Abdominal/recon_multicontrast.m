% Script produces Figure 9 plots
% Multislice bSSFP and multi-echo GRE with iterative water/fat separation
%
% Brief Description:
% - Performs fake QBC-regularized SENSE recon on 0.75T data
% - Performs iterative water/fat separation on multi-echo data
% - Creates Plot
%
% (c) Christian Guenthner, ETH Zurich, 2023

clear
run('../init.m')

% load raw data and store in mat files
% scan ids
ids = [11 12];
for n=1:length(ids)
    % load from raw data only if we don't have a matfile yet
    if isempty(scannr(ids(n),'data/','.mat'))
        % also perform a pics recon from raw data
        MR{n} = load_and_normalize(scannr(ids(n),'data/','.raw'));
        raw = struct();
        raw.Data = MR{n}.Data;
        raw.TE = MR{n}.Parameter.Scan.TE;
    
        save([MR{n}.Parameter.Filename.Data(1:end-4) '.mat'],'raw')
    end
end

% now load the mat files
for n=1:length(ids)
    data{n} = load(scannr(ids(n),'data/','.mat'),'raw');
    data{n} = data{n}.raw;
end

%%
%clear  S d_raw d r d_roemer qbc_img

for n=1:length(ids)
    % estimate coil sensitivity using ecalib
    S{n} = estim_sensitivities(data{n}.Data);
    
    d_roemer{n} = sum(data{n}.Data .* conj(S{n}),4);

    % let's fake a qbc image via blurring..
    qbc_img{n} = imgaussfilt(abs(d_roemer{n}(:,:,:,1)),20);

    % we did a regularized sense recon with 51 regularizations, then
    % manually picked our favorite. lambda = 100 is the choice. This basically
    % means we're doing a roemer recon and devide by the qbc_image
    d_raw{n} = qbc_reg_sense(data{n}.Data,S{n},qbc_img{n},'lambda',100);
end

%% Do iterative water/fat separation and b0 estimation
[wf,b0_map,dTE] = iterative_wfb0(d_raw{2}, data{2}.TE);

%% plot
% load the mask (to suppress background noise)
load mask
maskghi = double(imgaussfilt(imdilate(roi,strel('disk',10)),5));
maskg = imresize(maskghi,size(d_raw{1},1)./size(d_raw{2},1));

% slices to plot
sl = [3 7 11];

% make figure
try;close(1);catch; end
figure(1);
set(gcf,'Position',[ 680   558   980   420],'Color','w');

% subplot data: (2nd scan volume is slightly larger, correct by manually
% adding 3 to the slice index)
data_to_plot = {...
    maskg .* d_raw{1}(:,:,sl),...
    maskghi .* d_raw{2}(:,:,sl+3,1),...
    maskghi .* d_raw{2}(:,:,sl+3,2),...
    maskghi .* wf(:,:,sl+3,1),...
    maskghi .* wf(:,:,sl+3,2),...
    {'pdff',maskghi .* abs(wf(:,:,sl+3,2))./(sum(abs(wf(:,:,sl+3,:)),4))},...
    {'b0',maskghi.*b0_map(:,:,sl) - 2e3/dTE},...
    };

% do the actual plotting
for n=1:length(data_to_plot)
    for m=1:length(sl)
        h=subtightplot(length(sl),length(data_to_plot),(m-1)*length(data_to_plot) + n)
        if iscell(data_to_plot{n})
            if strcmpi(data_to_plot{n}{1},'b0')
                imagesc(real(data_to_plot{n}{2}(:,:,m)));
                caxis([-150 150]);
                colormap(h,colorcet('C9'));
            else
                imagesc(abs(data_to_plot{n}{2}(:,:,m)));
                caxis([0 1]);
                colormap(h,viridis(256));
            end
        else
            imagesc(abs(data_to_plot{n}(:,:,m)));
            caxis([0 quantile(abs(reshape(data_to_plot{n}(:,:,m),[],1)),.98)]);
            colormap(h,gray(256));
        end
        axis square
        set(gca,'visible',0)
    end
end
%%
export_fig('fig9.png','-m4');
%%
writecolorbar('cb_b0.png',{'-150 Hz','-75 Hz','0 Hz','75 Hz','150 Hz'},'colormap',colorcet('C9'));
writecolorbar('cb_pdff.png',{'0%','50%','100%'},'colormap',viridis(256));
