% Script produces Figure 2 and 3 plots
% Comparisson of B0 and B1+ Maps in Coronal and Transversal Orientations
% in the Abdomen.
%
% Brief Description:
% - Performs fake QBC-regularized SENSE recon on Cartesian and Spiral data
% - Performs iterative water/fat/b0 recon on Cartesian data
% - Performs iterative water/fat/b0/spiral-deblurring recon using MFI
% - Creates the plot
%
% (c) Christian Guenthner, ETH Zurich, 2023

run('../init.m')

% do the spiral multi-frequency reconstruction
ids_b0 = [55 62];

for id = ids_b0
    if isempty(scannr(id,'data/','.mat'))
        raw = mf_spiral(scannr(id,'data/','.raw') ...
            ,'mfi_frequencies',linspace(-200,100,19)'/1000 ...
            ,'fat_frequencies',1 ... we're not doing any w/f separation
            ,'fat_areas',1 ...
            ,'remove_oversampling',false ...
            );
        % NOTE: mfi frequencies are an educated guess - if you have no clue
        % what your B0 field looks like, choose a large domain and refine it
        % once you have a B0 estimate.
        raw.Data = raw.Data(:,:,:,:,:,:,:,:,1:length(raw.TE),:);

        save([raw.filename(1:end-4) '.mat'],'raw')
    end
end

%%
for n=1:length(ids_b0)
    d=load(scannr(ids_b0(n),'data/','.mat'));
    data{n} = d.raw;
    
    % estimate sensitivities
    S{n} = estim_sensitivities(data{n}.Data);
end

% we use different amounts of blurring for the fake-qbc image to account
% for the strong surface flare in transverse orientation compared to the
% relatively benign sensitivity profile in the coronal angulation.
sigma = [10, 20];

% do roemer coil-combination, fake QBC estimation, and regularized sense
for n=1:length(ids_b0)
    d_roemer{n} = sum(data{n}.Data .* conj(S{n}),4);

    % let's fake a qbc image via blurring..
    qbc_img{n} = imgaussfilt(abs(d_roemer{n}(:,:,:,1)),sigma(n));
    % Since the fake body coil image is very low-res anyhow
    % we don't bother about MFI / spiral deblurring here and simply
    % take the 0Hz demodulated, 1st echo of our dataset.

    % we did a regularized sense recon with 51 regularizations, then
    % manually picked our favorite
    d_raw{n} = qbc_reg_sense(data{n}.Data,S{n},qbc_img{n},'lambda',0.3);
end

% Our B0 scans are spiral acquisitions - let's self-consistently reconstruct 
% them with MFI
for n=1:2
[b0_map{n},ecs{n}] = iterative_b0_mfi(d_raw{n}, data{n});
end


%% B1+
ids_b1p = [56 63];
% and get the b1+ maps from DREAM scans:
for n=1:length(ids_b1p)
    if isempty(scannr(ids_b1p(n),'data/','.mat'))
        filename = scannr(ids_b1p(n),'data/');
        [b1p_map,MR] = dream_b1p(scannr(ids_b1p(n),'data/'));
        T = MR.Transform('ijk','xyz');
        save([filename(1:end-4) '.mat'],'b1p_map','T');
    end
end

clear b1p_map
for n=1:length(ids_b1p)
    d=load(scannr(ids_b1p(n),'data/','.mat'));
    
    % and map our b1+ scan to the resolution and fov of our b0 scans:
    szSource = size(d.b1p_map);
    szTarget = size(b0_map{n});
    [Y,X,Z] = ndgrid(1:szTarget(1),1:szTarget(2),1);
    X = pinv(d.T)*data{n}.T*cat(2,X(:),Y(:),Z(:),ones(length(Z(:)),1)).';
    b1p_map{n} = reshape(interp2(1:szSource(1),1:szSource(2),d.b1p_map,X(1,:),X(2,:)),szTarget(1:2));
end

%% ... and plot
% load masks
 mask = {load('mask_tra'),load('mask_cor2')};

%%
p=[.99 .995];
try;close(2); catch; end
figure(2);
set(gcf,'Position',[100 100 600 400],'Color','w')

cnt=1;
ids= 1:2;
for id=ids
% prepare mask
mask{id}.grow   = double(imdilate(mask{id}.mask,strel('disk',9)));
mask{id}.grow_smooth = imgaussfilt(mask{id}.grow,10);
mask{id}.shrink = double(imerode(mask{id}.mask,strel('disk',5)));
qbc_scale = 5;
mag = abs(ecs{id}(:,:,1)) .* (1 - exp(-(qbc_scale * mask{id}.grow_smooth).^2));
subtightplot(2,3,1+(cnt-1)*3);
display_image(mag,p(id));
hold on

% plot anatomic cross-reference lines
C = pinv(data{id}.T)*data{ids(3-cnt)}.T*cat(2,size(data{ids(3-cnt)}.Data,2)/2,size(data{ids(3-cnt)}.Data,1)/2,1,1).';
plot([0.5 size(ecs{id},2)],[1 1]*C(1),'-r')

subtightplot(2,3,2+(cnt-1)*3);
overlay_image(...
    mag...
    ,[0 quantile(abs(ecs{id}(:,:,1)),p(id),'all')]...
    ,ones(size(ecs{id},1))...
    ,gray(256)...
    ,real(b0_map{id})...
    ,[-150 200]...
    ,mask{id}.shrink...
    ,colorcet('C8'));

subtightplot(2,3,3+(cnt-1)*3);
overlay_image(...
    mag...
    ,[0 quantile(abs(ecs{id}(:,:,1)),p(id),'all')]...
    ,ones(size(ecs{id},1))...
    ,gray(256)...
    ,real(b1p_map{id})...
    ,[0.25 1.25]...
    ,mask{id}.shrink...
    ,magma(256));
cnt=cnt+1;
end

export_fig('fig2.png','-m4');

%%
writecolorbar('cb_B0_-150-200.png',arrayfun(@(x)sprintf('%i Hz',x),[-150:50:200],'UniformOutput',false),'colormap',colorcet('C8'),'orientation','vert');
writecolorbar('cb_B1_25-125.png',arrayfun(@(x)sprintf('%i %%',x),[25:25:125],'UniformOutput',false),'colormap',magma(256),'orientation','vert');

%% also plot some maps showin the approximate receive field of our coils

try;close(2); catch; end
figure(2);
set(gcf,'Position',[100 100 1000 400],'Color','w')
cnt=1;
for id = 1:2
ci = (sum(abs(data{id}.Data(:,:,:,:,1)).^2,4));
S0 = abs(data{id}.Data(:,:,:,:,1))./ (ci + 1e1); %(esp ~5% peak signal)
cil = imgaussfilt(ci,5);

p=.98;
subtightplot(2,5,1+5*(cnt-1));
display_image(abs(ci./cil),p);

for n=1:4
    subtightplot(2,5,1+n+5*(cnt-1));
    display_image(abs(S0(:,:,n)),p);
end
cnt=cnt+1;
end

export_fig('fig3.png','-m4');