% setup your bart environment here
setenv('TOOLBOX_PATH','~/BART/bart')
addpath(genpath('~/BART/bart'))

% add mrecon if you want to reconstruct from raw data
addpath(genpath('/usr/local/MATLAB/tools/MRecon'))

% add our shared functions
addpath(genpath('_shared_functions/'))

% set to true if you want to disable png export
if 1
    export_fig = @(varargin)1;
    writecolorbar =@(varargin)1;
end

% test on required libraries:
if exist('viridis','file')==0
    warning('perceptually uniform colormaps not installed. Download from %s',...
        'https://www.mathworks.com/matlabcentral/fileexchange/51986-perceptually-uniform-colormaps');
    % replace by grayscale
    viridis = @(x)gray(x);
end
if exist('magma','file')==0
    warning('perceptually uniform colormaps not installed. Download from %s',...
        'https://www.mathworks.com/matlabcentral/fileexchange/51986-perceptually-uniform-colormaps');
    % replace by grayscale
    magma = @(x)gray(x);
end
if exist('colorcet','file')==0
    warning('colorcet not installed. Download from %s',...
        'http://www.peterkovesi.com/matlabfns/Colourmaps/colorcet.m');
    % replace by grayscale
    colorcet = @(x)gray(256);
end
if exist('subtightplot','file')==0
    warning('subtightplot not installed. Download from %s',...
        'https://mathworks.com/matlabcentral/fileexchange/39664-subtightplot');
    % replace by subplot
    subtightplot = @(x,y,z,varargin)subplot(x,y,z);
end
if exist('snaphu','file')==0
    warnstr = sprintf('snaphu unwrapper binaries are not installed. Download from %s',...
        'https://step.esa.int/main/snap-supported-plugins/snaphu/');
    warning(warnstr);
    % no replacement, throw an error instead
    snaphu = @(varargin)error(warnstr);
end
if exist('bart','file')==0
    warnstr = sprintf('bart is not installed. Download and compile from %s',...
        'https://github.com/mrirecon/bart');
    warning(warnstr);
    % no replacement, throw an error instead
    bart = @(varargin)error(warnstr);
end
if exist('MRecon','file')==0
    warnstr = sprintf('MRecon is not installed. Only necessary if Philips raw data should be read. For more information visit: %s', ...
        'http://www.gyrotools.com/gt/index.php/products/reconframe');
    warning(warnstr);
    % no replacement, throw an error instead
    bart = @(varargin)error(warnstr);
end
