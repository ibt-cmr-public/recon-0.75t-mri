% Script produces Figure 8 plots
% Comparisson of Multi-Acquisition Spiral Dixon with 
% Water/Fat Separated FISP-MRF
%
% Follows approximately the work by
%
% Koolstra K, Webb AG, Veeger TTJ, Kan HE, Koken P, Boernert P. 
% Water/fat separation in spiral magnetic resonance fingerprinting for high
% temporal resolution tissue relaxation time quantification in muscle. 
% Magn. Reson. Med. 2020;84:646-662 doi: https://doi.org/10.1002/mrm.28143.
%
%
% Brief Description:
% - Loads and reconstructions Multi-Acquisition Spiral Dixon for B0 Map
%   (deblurring of spirals) and B1+ DREAM acquisition
% - Loads MRF Sequence from RAW File and Simulates B1+-Resolved EPG 
%   (No slice profile correction)
% - Dictionary compression
% - Water/fat separation, fat spectrum deblurring in k-space and multi-
%   frequency interpolation, regularized SENSE coil combination
% - Matching with prescribed B1+ map
% - Creates the plot
%
% (c) Christian Guenthner, ETH Zurich, 2023

clear
run('../init.m')

%% Let's start with the "pre-scans," i.e. B0 and B1+ Scans
% get the multi-acquisition spiral dixon data
if isempty(scannr(11,'data/','.mat'))
    raw = mf_spiral(scannr(11,'data/','.raw'),...
        'mfi_frequencies',linspace(-200,100,19)'/1000 ...
        );    
    save([raw.filename(1:end-4) '.mat'],'raw')
end

d=load(scannr(11,'data/','.mat'));
data = d.raw;
% estimate sensitivities
data.S = estim_sensitivities(data.Data);
d_roemer = sum(data.Data .* conj(data.S),4);
% let's fake a qbc image via blurring..
qbc_img = imgaussfilt(abs(d_roemer(:,:,:,1)),20);
% do the regularized coil combination
d_raw = qbc_reg_sense(data.Data,data.S,qbc_img,'lambda',0.3);
% and the deblurring / water/fat seperation and b0-map estimation
[wf,b0_map,dTE,d_echoes] = iterative_wfb0_mfi(d_raw,data);

% and get a b1+ map from a DREAM scan:
% do a roemer coil combination
if isempty(scannr(20,'data/','.mat'))
    filename = scannr(20,'data/');
    b1p_map = dream_b1p(scannr(20,'data/'));
    save([filename(1:end-4) '.mat'],'b1p_map');
end
load(scannr(20,'data/','.mat'))
% 

%% Now, extract MRF Sequence Details from raw data and store them
if isempty(scannr(17,'data/','.par.mat'))
    rd = MRecon(scannr(17,'data/','.raw'));
    flip_angle = rd.Parameter.GetValue('VAL01_ACQ_fipri_angles');
    dyns = rd.Parameter.GetValue('VAL01_ACQ_fipri_shots');
    flip_angle = flip_angle(1:dyns);
    TR = rd.Parameter.Scan.TR;
    TE = rd.Parameter.GetValue('MPF_fipri_te_arr');
    TE = TE(1:dyns);

    save([rd.Parameter.Filename.Data(1:end-4) '.par.mat'],'flip_angle','TR','TE');
    clear rd flip_angle dyns TR TE
end
mrf_par = load(scannr(17,'data/','.par.mat'));

%% Create B1+ Resolved MRF Dictionary
if isempty(scannr(17,'data/','.par.mat'))
    % setup B1+ range
    b1p = 0.5:0.05:.85;

    % setup T1/T2 (Philips Research, Hamburg - MRF Workshop Range)
    [T1r,T2r] = meshgrid([2:2:100 100:10:1000 1000:20:2000 2000:40:5000],[2:2:150 150:10:500 500:20:1000 1000:40:2000]);
    I=T1r>T2r;
    T1r=T1r(I); T2r=T2r(I);
    
    % Create the RF pulse matrices for all time-points and
    RFmat=RF(todim(b1p,3).*todim(mrf_par.flip_angle/180*pi,4));
    % there's an adiabatic pulse before our FA train, add it:
    RFmat = cat(4,RF(pi * ones(1,1,length(b1p))),RFmat);
    aq_on = ones(1,1,1,size(RFmat,4));
    aq_on(1)=false;

    % and run epg simulation (takes ~50min to complete)
    S = srepg(RFmat, ones(1,1,1,size(RFmat,4))*mrf_par.TR, todim(exp(-1./T1r),7), todim(exp(-1./T2r),7),...
        'acquisition_on',aq_on,'Nepg',100,'export_configuration',0,'debug',1);
    S = squeeze(S);

    %% propagate to TE
    S = S.*exp(-todim(mrf_par.TE(:),1)./todim(T2r,3));

    %% Let's store the signal before we continue with some processing.
    epg = struct();
    [epg.D,epg.U] = hosvdn(S,1:3,1e-5);
    epg.mrf_par = mrf_par;
    epg.b1p = b1p;
    epg.T1 = T1r;
    epg.T2 = T2r;
    filename = scannr(17,'data/','.raw');
    save([filename(1:end-4) '.sig.mat'],'-v7.3','epg');
end
load(scannr(17,'data/','.sig.mat'));

%% Calculate Dictionary from EPG simulation
% the premise of the technique is that the signal between the first and
% second image is basically the same. so we take the average between the
% two time-points:
U = epg.U;
U{1} = squeeze(sum(reshape(epg.U{1},2,[],size(epg.U{1},2)),1));
% recreate the full signal model
Ssum = tenmul(epg.D,U);
% if you'd like to check the accuracy of our approach, run:
%  sqrt(mean(abs(Ssum(:) - reshape(sum(reshape(S,2,500,8,[]),1),[],1)).^2,'all'))
% this calculates the exact "summed" epg model against the compressed model
% and normalize it
n = sqrt(sum(abs(Ssum).^2,1)); 
Ssum = Ssum./n;
% compress with hosvd again, this time we take the final 1e-4 threshold
dict = epg;
[dict.D,dict.U] = hosvdn(Ssum,1:3,1e-4);
dict.norm = n;

%%
% now that we have the dictionary, we can load the data and perform the mfi
% interpolation

if isempty( scannr(17,'./data/','.data.mat'))
    mfi_frequencies = linspace(-300,150,19).'/1000;
    raw = mf_mrf_spiral( ...
        scannr(17,'./data/'), ...
        'mfi_frequencies',linspace(-300,150,19).'/1000, ...
        'fat_shifts',data.fat_shifts, ...
        'fat_areas',data.fat_areas,...
        'U',dict.U{1});
    save([raw.filename(1:end-4) '.data.mat'],'-v7.3','raw');
end
load(scannr(17,'./data/','.data.mat'));

%% Correct our b0 map by the difference in resonance frequencies of the scans)
b0 = b0_map - (data.HWResonanceFreq - raw.HWResonanceFreq); %[Hz]
% and correct for linear shims
b0 = b0 + get_shim_field(raw.shims, raw.fov, raw.T, size(raw.Data));
% now get mfi interpolation coefficients for current b0_map
Cmfi = mfi_interpolation_map(raw.mfi_frequencies, raw.tAcq, b0/1e3);
Cmfi = permute(Cmfi, [2:10 1]);

% perform multi-frequency interpolation step
dmfi = sum(Cmfi .* raw.Data(:,:,:,:,:,:,:,:,:,2:end,:),10);

% and perform sense coil combination with body coil regularization
mrf_sense = qbc_reg_sense(dmfi,data.S,qbc_img,'lambda',0.3);

%% do the matching
match = mrf_match(dict, b1p_map, mrf_sense);

%% ... and the plotting
normabs = @(x) abs(x)./median(abs(x(:)),'omitnan');
% let's get a crude mask for our parametric maps, otherwise the low signal
% areas are going to make it hard to see structures 
mask = {...
    5*(max(min(.4,normabs(match.v(:,:,1))/2.5),.2)-.2) > 0,...
    5*(max(min(.4,normabs(match.v(:,:,2))/7),.2)-.2) > 0};

try; close(1); catch; end
figure(1)
set(gcf,'color','w','Position',[100 100 4/2*300 350])
sp = @(x,n)subtightplot(2,4,x+4*(n-1));
for n=1:2
sp(1,n); display_image(abs(wf(:,:,n)),1);

sp(2,n); display_image(abs(match.v(:,:,n)),.995);
% we show the dot product with the normalized dictionary instead of the
% true proton density image here, as the noise in the T2 map will propagate
% to the PD as well. 
% sp(2,n); display_image(abs(match.rho(:,:,n)),.92,mask{n});
% colormap(gca,gray(512));

sp(3,n); display_image(mask{n}.*match.T1(:,:,n),1)
colormap(gca,cat(1,[1 1 1],viridis(512)));
if n==1
    caxis([0 1200])
else
    caxis([0 400])
end

sp(4,n); display_image(mask{n}.*match.T2(:,:,n),1)
colormap(gca,cat(1,[1 1 1],magma(512)));
caxis([0 200])
end

export_fig('fig8.png','-m4')
%%
% create high-resolution colorbars
writecolorbar('cb_T1_0-1200.png',arrayfun(@(x)sprintf('%i ms',x),[0 600 1200],'UniformOutput',false),'colormap',viridis(512),'orientation','horz','width',.8);
writecolorbar('cb_T1_0-400.png',arrayfun(@(x)sprintf('%i ms',x),[0 200 400],'UniformOutput',false),'colormap',viridis(512),'orientation','horz','width',.8);
writecolorbar('cb_T2_0-200.png',arrayfun(@(x)sprintf('%i ms',x),[0 100 200],'UniformOutput',false),'colormap',magma(512),'orientation','horz','width',.8);
