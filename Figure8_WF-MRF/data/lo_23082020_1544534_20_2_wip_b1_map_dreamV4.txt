                  File: /home/cguenthn/tmp/git_lofi_paper/recon-0.75t-mri/Figure8_WF-MRF/data/lo_23082020_1544534_20_2_wip_b1_map_dreamV4.raw
         Scan Duration: 00:18.3
      Acquisition Type: cartesian
         Sequence Type: FFE
         Contrast Mode: T1
             Scan Mode: 2D
      Readout Duration: 0.918 ms
       Max. Flip Angle: 20.0°
       Repetition Time: 4.0 ms
           Echo Time 1: 1.1 ms
           Echo Time 2: 2.6 ms
                   FOV: 8.0x350.0x350.0
Acquisition Voxel Size: 4.86x4.86x8.00
             Slice Gap: 0.8 
                 SENSE: 1.00x1.00x1.00
           Nr Averages: 10
           B1 Map Type: DREAM
         B1 Flip Angle: 90.0°
            STE First?: no
