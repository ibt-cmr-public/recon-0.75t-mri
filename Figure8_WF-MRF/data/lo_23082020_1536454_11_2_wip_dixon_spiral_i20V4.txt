                  File: /home/cguenthn/tmp/git_lofi_paper/recon-0.75t-mri/Figure8_WF-MRF/data/lo_23082020_1536454_11_2_wip_dixon_spiral_i20V4.raw
         Scan Duration: 00:06.5
      Acquisition Type: spiral
         Sequence Type: FFE
         Contrast Mode: T1
             Scan Mode: 2D
      Readout Duration: 10.031 ms
        No Interleaves: 22
       Max. Flip Angle: 15.0°
       Repetition Time: 18.8 ms
             Echo Time: 1.1 ms
            Dixon Type: multi-acquisition
   Echo Time Increment: 3.1 ms
                   FOV: 8.0x350.0x350.0
Acquisition Voxel Size: 1.99x15.91x8.00
             Slice Gap: 0.8 
                 SENSE: 1.00x1.00x1.00
           Nr Averages: 4
