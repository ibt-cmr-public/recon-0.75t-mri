% Script produces Figure 6 a-b plots
% Comparisson of Cardiac CINE Scans at 0.75T, 1.5T and 3T
%
% Brief Description:
% - Performs fake QBC-regularized SENSE recon on 0.75T CINE
% - Loads 1.5T and 3T scanner-reconstructed CINE
% - Creates Plot
%
% (c) Christian Guenthner, ETH Zurich, 2023

clear
run('../init.m')

files = {'data/pi_26082020_1041277_5_2_wip_bffe_saV4',...
    'data/pi_17092020_1707584_5_1_wip_saV4_15T',...
    'data/pi_17092020_1851201_5_1_wip_saV4_3T'};

% let's first load 1.5 and 3T data (scanner recon)
for n=2:3
    if exist([files{n} '.mat'],'file')==0
        r = MRecon([files{n} '.par']);
        r.Perform;
        data = r.Data;
        save([files{n} '.mat'],'data');
    end
end

% Load par/rec and save as mat (3.0T)
data15 = load([files{2} '.mat']);
data30 = load([files{3} '.mat']);

% now recon 0.75T data from raw file if not already done.
if exist([files{1} '.mat'],'file')==0
    % reconstruct from raw data with mrecon
    data = load_and_normalize([files{1} '.raw']);
    data = data.Data;
    % estimate sensitivities with ecalib (bart)
    S = estim_sensitivities(data);
    % and save result
    save([files{1} '.mat'],'data','S');
end

%%
% load data and sensitivities
load([files{1} '.mat']);

% do a simple roemer reconstruction
d_roemer = sum(data .* conj(S),4);

% get a fake qbc image from our roemer reconstruction
qbc_img = mean(imgaussfilt(abs(d_roemer),20),6);

% and get a regularized sense recon
d_Reg = qbc_reg_sense(data,S,qbc_img,'lambda',.3);

data075.data = d_Reg;

%% simple plot script
%reg_id = 37; % pick a regularization you like
frame075 = 7;
frame15 = 11;% pick frames for 1.5 and 3T
frame30 = 12; 
try; close(1); catch; end
figure(1);
set(gcf ...
    ,'color','w','Position',[100 100 600 600 * 2 / 3])
% for the .75T data, cut the fov similar to the 1.5T for visuallization
% only.
tmp  = data075.data(:,:,frame075);
tmp(1:round(51/size(data15.data,1)*size(data075.data,1)),:) = 0;
tmp(end:-1:end-round(51/size(data15.data,1)*size(data075.data,1))+1,:) = 0;
% plot
subtightplot(2,3,1); display_image(abs(tmp),.98);
subtightplot(2,3,2); display_image(abs(data15.data(:,:,frame15)),.99);
subtightplot(2,3,3); display_image(abs(data30.data(:,:,frame30)),.995);

% display cardiac zooms
w = 215-86; % .75T cutout
subtightplot(2,3,4); display_image(abs(data075.data(99+(1:w),85+(1:w),frame075)),.98);
% scale width to resolution of 1.5/3T scans
w = round(w / size(data075.data,1) * size(data15.data,1));
subtightplot(2,3,5); display_image(abs(data15.data(179+(1:w),140+(1:w),frame15)),.99);
subtightplot(2,3,6); display_image(abs(data30.data(180+(1:w),140+(1:w),frame30)),.995);

% export to png if export_fig is available
export_fig('fig6.png','-m4')
