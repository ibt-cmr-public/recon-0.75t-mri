% (c) Valery Vishnevskiy, ETH Zurich, 2023
function c = unitarize_coils(coils, dim, deps)
    c = coils ./ (sqrt(sum(abs(coils).^2, dim)) + deps);
end