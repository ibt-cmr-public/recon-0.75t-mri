function x = proximal_euclidean(a, lambda, varargin)
% proximal_euclidean(a, lambda, {dims, check_nan, deps})
% compute proximal Euclidean operator
% dims -- vector dimensions (default 1)
% check_nan -- check nan in output (default true)
% deps -- constant to add to avoid division by 0 (default 0)
%
% minimize for x:
% 1/2 sum_ij (x(i, j) - a(i, j))^2 + lambda sum_j sqrt(sum_i x(i, j))
% 
% (c) Valery Vishnevskiy, ETH Zurich, 2023

    dims = 1;
    check_nan = true;
    deps = 0;
    if numel(varargin) >= 1
        dims = varargin{1};
    end
    if numel(varargin) >= 2
        check_nan = varargin{2};
    end
    if numel(varargin) >= 3
        deps = varargin{3};
    end
    
    if deps == 0
        nrms = sqrt(sum(abs(a).^2, dims));
    else
        nrms = sqrt(sum(abs(a).^2, dims) + deps);
    end
    x = max(1 - lambda ./ nrms, 0) .* a;
    if check_nan
        idxs = ~isnumeric(x);
        x(idxs) = a(idxs);
    end
    
% old code
%     nrms = sqrt(sum(abs(a).^2, dims));
%     idxs = nrms > 0;
%     x = zeros(size(a), 'like', a);
%     tmp = max(1 - lambda ./ nrms(idxs), 0);
%     atmp = a(:, idxs);
%     x(:, idxs) = bsxfun(@times, atmp, tmp);
end    