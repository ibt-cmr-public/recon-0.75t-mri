function x = proximal_l1(a, lambda)
% minimizes for x
%   lambda * |x|_1 + 1/2 * (x - a)^2
% (c) Valery Vishnevskiy, ETH Zurich, 2023
    x =  sign(a) .* max(abs(a) - lambda, 0);
end