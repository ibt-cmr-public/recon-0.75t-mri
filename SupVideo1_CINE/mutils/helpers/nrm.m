% (c) Valery Vishnevskiy, ETH Zurich, 2023
function y = nrm(x)
% nrm=@(x) (x-min(x(:)))/max(x(:)-min(x(:)))
if isempty(x)
    y = [];
else
    y = (x-min(x(:)))/max(x(:)-min(x(:)));
end
end