% (c) Valery Vishnevskiy, ETH Zurich, 2023
function x = expand_dims(x, ndim)
    assert(ndim > 0);
    sz = size(x);
    sz = sz(:).';
    if ndim == 1
        sz = [1, sz];
    elseif ndim > ndims(x) 
        sz = [sz, ones(1, ndim - numel(sz))];
    else
        sz = [sz(1:ndim-1), 1, sz(ndim:end)];
    end
    x = reshape(x, sz);    
end