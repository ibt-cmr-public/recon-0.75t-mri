function [rec, itinfo] = recon_admm_VTV(d, coils, mask, lambda, opts)
% min F(X) = 1/2|M * F * Coils * X  - b|_2^2 + lambda * |Dt X|_1
% d - the k-space sz1-sz2-Nt-Ncoils for
% or              sz1-sz2-sz3-Nt-Ncoils for 3D
% coils size: sz1-sz2-1-Ncoils for 2D
%             sz1-sz2-sz3-1-Ncoils for 3D
% mask should be broadcastable to d
% lambda - regularization coefficient
% opts:
%   absTolX, relTolX - tolerances for terminating optimization
%   tau - fraction to reduce GD step length
%   gd_steplen - initial GD step length
%   debug - print and plot debug information
%   normalize_coils - True or False
%   maxiter - maximum number of iterations
%   fdims - spatial dimensions to perform fft
%   lin_solver: 'cg' or 'gd' to perform the X-update
%       cg_iters: number of iterations for cg
%   rho: the augmentation coefficient
%
% (c) Valery Vishnevskiy, ETH Zurich, 2023

absTolX = getoptions(opts, 'absTolX', 1e-9);
relTolX = getoptions(opts, 'absTolX', 1e-5);
debug = getoptions(opts, 'debug', false);
normalize_coils = getoptions(opts, 'normalize_coils', false);
maxiter = getoptions(opts, 'maxiter', 100);
cg_iters = getoptions(opts, 'cg_iters', 5);
rho = getoptions(opts, 'rho', 0.5);
rho_tau = getoptions(opts, 'rho_tau', 1.05);
spat_w = getoptions(opts, 'spat_w', [0.1, 0.1, 0.1, 1]);

use_gpu = getoptions(opts, 'use_gpu', false);
force_double = getoptions(opts, 'force_double', false);
force_single = getoptions(opts, 'force_single', false);

% input_dtype = class(d);
input_dtype_el = d(1);

if force_single
    d = single(d);
    coils = single(coils);
    mask = single(mask);
end

if force_double
    d = double(d);
    coils = double(coils);
    mask = double(mask);
end

if use_gpu
    d = gpuArray(d);
    coils = gpuArray(coils);
    mask = gpuArray(mask);
end

if (ndims(d) == 4 && size(coils, 4)==size(d, 4)) || ndims(d) == 3
    nd = 2;
    coil_dim = 4;
    temp_dim = 3;
%     if ~isfield(opts, 'fdims')
        fdims = 1;
%     end
else
    nd = 3;
    temp_dim = 4;
    coil_dim = 5;
%     if ~isfield(opts, 'fdims')
        fdims = [2,3];
%     end
end
fdims = getoptions(opts, 'fdims', fdims);

itinfo = [];
itinfo.fvals = zeros(1, 1);
itinfo.f_reg = zeros(1, 1);
itinfo.f_data = zeros(1, 1);
itinfo.relTolX = zeros(1, 1);
itinfo.absTolX = zeros(1, 1);

if normalize_coils
    coils = unitarize_coils(coils, coil_dim, 1e-12);
end

Fb = sum(conj(coils).*k2i(mask.*d, fdims), coil_dim);
x = Fb;

Fx = Top(x, temp_dim, spat_w);
z = Fx;

u = z - Fx;
szx = size(Fb);
for iter = 1:maxiter
    rho = rho * rho_tau;
    mat_op = @(x) sq_op(x, coils, mask, rho, szx, fdims, coil_dim, temp_dim, spat_w);
    
    x_prev = x;
    % X - update
    % solves min_x .5 * |M*F*C*X - b|^2 + rho/2 |Dt X - Z + U|^2
    rhs = Fb + rho * AdjTop(z + u, temp_dim, spat_w);
    [x,~,~,~,~] = pcg(mat_op, rhs(:), 1e-9, cg_iters, [], [], x(:));
    x = reshape(x, szx);
    Fx = Top(x, temp_dim, spat_w);

    % Z - update
    % solves min_Z lambda*|Z|_1 + rho/2 |Dt*X - Z + U|^2
    tmp = Fx - u;
    
    
%     z = proximal_l1(tmp, lambda/rho);
    z = proximal_euclidean(tmp, lambda/rho, 5);

    % dual update
    u = u + z - Fx;

    res1 =  mask .* (i2k(x.*coils, fdims)  - d);
    f_data =  sum( abs(res1(:)).^2)/2;
    reg_cost = sum(abs(Fx(:)));
    itinfo.fvals(end+1) = gather(f_data + reg_cost * lambda);
    itinfo.f_reg(end+1) = gather(reg_cost);
    itinfo.f_data(end+1) = gather(f_data);
    
    if debug
        fprintf('%d fv=%e\n', iter, itinfo.fvals(end));
    end

    % use tolX for optimality conditions, faster to evaluate
    itinfo.absTolX(iter) = gather(max( abs(x(:) - x_prev(:))));
    itinfo.relTolX(iter) = gather(sqrt(sum( abs(x(:) - x_prev(:)).^2 )) / sqrt(sum(abs(x(:)).^2 )));
    if (iter > 40) && ( itinfo.absTolX(iter) < absTolX || itinfo.relTolX(iter) < relTolX)
        break;
    end

    if debug
        if nd == 2
            subplot(141);
            imagesc(squeeze(abs(x(:, round(end/2), :))));
            title('Z');
            subplot(142);
            imagesc(abs(x(:,:, round(end/2))));
            title('X');
            subplot(143);
            plot(log(itinfo.fvals));
            title('log F(x)');
            subplot(144);
            plot(fl(itinfo.f_reg));
            title('|DX|_1');
            pause(0.01);
        elseif nd == 3
%             nnz(abs(z))
            subplot(151);
            imagesc(abs(z(:,:, round(end/2), round(end/2))));
            title('Z');
            subplot(152);
            imagesc(abs(x(:,:, round(end/2), round(end/2))));
            title('X');
            subplot(153);
            imagesc( squeeze(abs(x(round(end/2), :, round(end/2), :))) );
            title('X');
            subplot(154);
            plot(log(itinfo.fvals));
            title('log F(x)');
            subplot(155);
            plot(fl(itinfo.f_reg));
            title('|DX|_1');
            pause(0.01);
        end
    end
end
% rec = cast(x, input_dtype);
rec = cast(x, 'like', input_dtype_el);
end

function [f, grad] = acq_op_func(X, b, coils, mask, fdims, coil_dim)
    res = mask .* (i2k(X.*coils, fdims)  - b);
    grad = sum(conj(coils) .* k2i(res, fdims), coil_dim);
    f = sum( abs(res(:)).^2 ) / 2;
end

function y = sq_op(x, coils, M, rho, sz, fdims, coil_dim, temp_dim, spat_w)
    x = reshape(x, sz);
    tmp = k2i(M.*i2k(coils.*x, fdims), fdims);
    tmp = sum(conj(coils) .* tmp, coil_dim);
    y = tmp + rho * AdjTop(Top(x, temp_dim, spat_w), temp_dim, spat_w);
    y = y(:);
end



function Dx = Top(x, dims, spat_w, varargin)
    % sz = [10, 8, 9];
    % op = @(x) Top(reshape(x, sz), [], 1, false);
    % A = get_matrix_from_operator(op, prod(sz));
    % 
    % sz = [10, 8, 9, 3];
    % op = @(x) AdjTop(reshape(x, sz), [], 1, false);
    % AT = get_matrix_from_operator(op, prod(sz));
    % 
    % imagesc(abs(A-AT'))
% 

    cyclic = true;%false;
    if numel(varargin) >= 1
        cyclic = varargin{1};
    end
    Dx1 = (circshift(x, 1, 1) - x)/2 * spat_w(1);
    Dx2 = (circshift(x, 1, 2) - x)/2 * spat_w(2);
    Dx3 = (circshift(x, 1, 3) - x)/2 * spat_w(3);
    Dx4 = (circshift(x, 1, 4) - x)/2 * spat_w(4);
    if ~cyclic
        Dx1(1, :, :, :) = 0;
        Dx2(:, 1, :, :)= 0;
        Dx3(:, :, 1, :)= 0;
        Dx4(:, :, :, 1) = 0;
    end
    Dx = cat(5, Dx1, Dx2, Dx3, Dx4);
end

function y = AdjTop(x, dims, spat_w, varargin)
    cyclic = true;%false;
    if numel(varargin) >= 1
        cyclic = varargin{1};
    end
    if ~cyclic
        x(1, :, :, :, 1) = 0;
        x(:, 1, :, :, 2) = 0;
        x(:, :, 1, :, 3) = 0;
        x(:, :, :, 1, 4) = 0;
    end
    y1 = (circshift(x(:,:,:,:, 1), -1, 1) - x(:,:,:,:, 1)) / 2 * spat_w(1);
    y2 = (circshift(x(:,:,:,:, 2), -1, 2) - x(:,:,:,:, 2)) / 2 * spat_w(2);
    y3 = (circshift(x(:,:,:,:, 3), -1, 3) - x(:,:,:,:, 3)) / 2 * spat_w(3);
    y4 = (circshift(x(:,:,:,:, 4), -1, 4) - x(:,:,:,:, 4)) / 2 * spat_w(4);

    y = y1 + y2 + y3 + y4;
end