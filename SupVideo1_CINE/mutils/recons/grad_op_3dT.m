% (c) Valery Vishnevskiy, ETH Zurich, 2023
function Dx = grad_op_3dT(x, weights)
    Dx1 = (circshift(x, 1, 1) - x)/2 * weights(1);
    Dx2 = (circshift(x, 1, 2) - x)/2 * weights(2);
    Dx3 = (circshift(x, 1, 3) - x)/2 * weights(3);
    Dx4 = (circshift(x, 1, 4) - x)/2 * weights(4);
    Dx = cat(5, Dx1, Dx2, Dx3, Dx4);
end