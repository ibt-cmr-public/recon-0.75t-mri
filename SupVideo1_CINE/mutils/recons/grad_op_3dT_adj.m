% (c) Valery Vishnevskiy, ETH Zurich, 2023
function y = grad_op_3dT_adj(x, weights)

    y1 = (circshift(x(:,:,:,:, 1), -1, 1) - x(:,:,:,:, 1)) / 2 * weights(1);
    y2 = (circshift(x(:,:,:,:, 2), -1, 2) - x(:,:,:,:, 2)) / 2 * weights(2);
    y3 = (circshift(x(:,:,:,:, 3), -1, 3) - x(:,:,:,:, 3)) / 2 * weights(3);
    y4 = (circshift(x(:,:,:,:, 4), -1, 4) - x(:,:,:,:, 4)) / 2 * weights(4);

    y = y1 + y2 + y3 + y4;
end