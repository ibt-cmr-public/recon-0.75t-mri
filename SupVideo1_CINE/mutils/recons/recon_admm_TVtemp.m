function [rec, itinfo] = recon_admm_TVtemp(d, coils, mask, lambda, opts)
% min F(X) = 1/2|M * F * Coils * X  - b|_2^2 + lambda * |Dt X|_1
% d - the k-space sz1-sz2-Nt-Ncoils for
% or              sz1-sz2-sz3-Nt-Ncoils for 3D
% coils size: sz1-sz2-1-Ncoils for 2D
%             sz1-sz2-sz3-1-Ncoils for 3D
% mask should be broadcastable to d
% lambda - regularization coefficient
% opts:
%   absTolX, relTolX - tolerances for terminating optimization
%   tau - fraction to reduce GD step length
%   gd_steplen - initial GD step length
%   debug - print and plot debug information
%   normalize_coils - True or False
%   maxiter - maximum number of iterations
%   fdims - spatial dimensions to perform fft
%   lin_solver: 'cg' or 'gd' to perform the X-update
%       cg_iters: number of iterations for cg
%   rho: the augmentation coefficient
%
% (c) Valery Vishnevskiy, ETH Zurich, 2023

absTolX = getoptions(opts, 'absTolX', 1e-9);
relTolX = getoptions(opts, 'absTolX', 1e-5);
tau = getoptions(opts, 'tau', 0.7);
gd_steplen = getoptions(opts, 'gd_steplen', 1.);
debug = getoptions(opts, 'debug', false);
normalize_coils = getoptions(opts, 'normalize_coils', true);
maxiter = getoptions(opts, 'maxiter', 100);
lin_solver = getoptions(opts, 'lin_solver', 'cg'); % or 'gd'
cg_iters = getoptions(opts, 'cg_iters', 5);
rho = getoptions(opts, 'rho', 0.5);
gd_attemps = 5;

if (ndims(d) == 4 && size(coils, 4)==size(d, 4)) || ndims(d) == 3
    nd = 2;
    coil_dim = 4;
    temp_dim = 3;
    if ~isfield(opts, 'fdims')
        fdims = 1;
    end
else
    nd = 3;
    temp_dim = 4;
    coil_dim = 5;
    if ~isfield(opts, 'fdims')
        fdims = [2,3];
    end
end
fdims = getoptions(opts, 'fdims', fdims);

itinfo = [];
itinfo.fvals = zeros(1, 1);
itinfo.f_reg = zeros(1, 1);
itinfo.relTolX = zeros(1, 1);
itinfo.absTolX = zeros(1, 1);

if normalize_coils
    coils = unitarize_coils(coils, coil_dim, 1e-12);
end

Fb = sum(conj(coils).*k2i(mask.*d, fdims), coil_dim);
x = Fb;
Fx = Top(x, temp_dim);
z = Fx;
u = z - Fx;
szx = size(Fb);
mat_op = @(x) sq_op(x, coils, mask, rho, szx, fdims, coil_dim, temp_dim);
for iter = 1:maxiter
    x_prev = x;
    % X - update
    % solves min_x .5 * |M*F*C*X - b|^2 + rho/2 |Dt X - Z + U|^2
    if strcmp(lin_solver, 'gd')
        [f0, dx_dat] = acq_op_func(x, d, coils, mask, fdims, coil_dim);
        f0 = f0 + rho/2 * sum(abs(fl(Fx - z - u)));
        Fx = Top(x, temp_dim);
        dx = dx_dat + rho * AdjTop(Fx - z - u) ;
        for jj = 1 : gd_attemps
            xx = x - gd_steplen * dx;
            Fx = Top(xx, temp_dim);
            res1 =  mask .* (i2k(xx.*coils, fdims)  - d);
            res2 = Fx - z - u;
            f1 = sum( abs(res1(:)).^2 )/2 + rho/2 * sum(abs(res2(:)).^2);
            if f1 <= f0 || iter == 1
                break;
            else
                gd_steplen = gd_steplen * tau;
            end
        end
        x = xx;
    elseif strcmp(lin_solver, 'cg')
        rhs = Fb + rho * AdjTop(z + u, temp_dim);
        [x,~,~,~,~] = pcg(mat_op, rhs(:), 1e-5, cg_iters, [], [], x(:));
        x = reshape(x, szx);
        Fx = Top(x, temp_dim);
    end

    % Z - update
    % solves min_Z lambda*|Z|_1 + rho/2 |Dt*X - Z + U|^2
    tmp = Fx - u;
    z = proximal_l1(tmp, lambda/rho);

    % dual update
    u = u + z - Fx;

    res1 =  mask .* (i2k(x.*coils, fdims)  - d);
    reg_cost = sum(abs(Fx(:)));
    itinfo.fvals(end+1) = gather( sum( abs(res1(:)).^2)/2 +  reg_cost* lambda );
    itinfo.f_reg(end+1) = gather( reg_cost );
    if debug
        fprintf('%d fv=%e\n', iter, itinfo.fvals(end));
    end

    % use tolX for optimality conditions, faster to evaluate
    itinfo.absTolX(iter) = gather(max( abs(x(:) - x_prev(:))));
    itinfo.relTolX(iter) = gather(sqrt(sum( abs(x(:) - x_prev(:)).^2 )) / sqrt(sum(abs(x(:)).^2 )));
    if (iter > 40) && ( itinfo.absTolX(iter) < absTolX || itinfo.relTolX(iter) < relTolX)
        break;
    end

    if debug
        if nd == 2
            subplot(141);
            imagesc(squeeze(abs(x(:, round(end/2), :))));
            title('Z');
            subplot(142);
            imagesc(abs(x(:,:, round(end/2))));
            title('X');
            subplot(143);
            plot(log(itinfo.fvals));
            title('log F(x)');
            subplot(144);
            plot(fl(itinfo.f_reg));
            title('|DX|_1');
            pause(0.01);
        elseif nd == 3
%             nnz(abs(z))
            subplot(151);
            imagesc(abs(z(:,:, round(end/2), round(end/2))));
            title('Z');
            subplot(152);
            imagesc(abs(x(:,:, round(end/2), round(end/2))));
            title('X');
            subplot(153);
            imagesc( squeeze(abs(x(:,round(end/2), round(end/2), :))) );
            title('X');
            subplot(154);
            plot(log(itinfo.fvals));
            title('log F(x)');
            subplot(155);
            plot(fl(itinfo.f_reg));
            title('|DX|_1');
            pause(0.01);
        end
    end
end
rec = x;
end

function [f, grad] = acq_op_func(X, b, coils, mask, fdims, coil_dim)
    res = mask .* (i2k(X.*coils, fdims)  - b);
    grad = sum(conj(coils) .* k2i(res, fdims), coil_dim);
    f = sum( abs(res(:)).^2 ) / 2;
end

function y = sq_op(x, coils, M, rho, sz, fdims, coil_dim, temp_dim)
    x = reshape(x, sz);
    tmp = k2i(M.*i2k(coils.*x, fdims), fdims);
    tmp = sum(conj(coils) .* tmp, coil_dim);
    y = tmp + rho * AdjTop(Top(x, temp_dim), temp_dim);
    y = y(:);
end


function Dx = Top(x, dim)
    Dx = (circshift(x, 1, dim) - x)/2;
end

function y = AdjTop(x, dim)
    y = (circshift(x, -1, dim) - x) / 2;
end