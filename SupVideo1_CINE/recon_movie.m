% Script produces Supplementary Video 1
% Comparison of retrospectively accelerated CINE Scans R=2..8
%
% Brief Description:
%
% (c) Valery Vishnevskiy & Christian Guenthner, ETH Zurich, 2023

run('../recon-0.75t-mri/init.m')
% CINE ADMM recon specific utilities
addpath(genpath('mutils'))

% load data
run_from_raw_data = false;
ids = [2 3 4];
for n=1:length(ids)
    if run_from_raw_data || isempty(scannr(ids(n),'data/','.dat.mat'))
        % THIS REQUIRES MRECON and BART
        % load data
        MR = load_and_normalize(scannr(ids(n),'data/'));
        % strip last dynamic (noise scan)
        data = MR.Data(:,:,1,:,1,:);
        % estimate coils
        coils= estim_sensitivities(data);
        % get roemer reconstruction
        roemer = sum(data .* conj(coils),4);
        % get fake body coil images and average them over heart cycles
        qbc_img = mean(apply2df(roemer,@(x)imgaussfilt(real(x),20)) + 1i*apply2df(roemer,@(x)imgaussfilt(imag(x),20)),6);
        qbc_img = qbc_img  ./ sqrt(mean(abs(qbc_img(:)).^2));
        data = permute(data,[1 2 6 4 3 5]);

        save([MR.Parameter.Filename.Data(1:end-4) '.dat.mat'],'data','coils','qbc_img')
    end
end

% load data after MRecon
dats = cell(1,length(ids));
for n=1:length(ids)
    dats{n} = load(scannr(ids(n),'data/','.dat.mat'));
end

%% generate retrospective sampling experiments
clear rec
% set the seed of our random number generator (make reproducible)
rng(0);

if gpuDeviceCount()==0
    fprintf('Running on CPU\n');
else
    fprintf('Trying to run on GPU\n');
end

% target acceleration factors
target_R = [1 2 4 8];
% define variable density sampling (exp. factor)
sgms     = [0 1.25 1.75 10]; 
target_us = 1./target_R;

% define how many lines we require to be fully sampled:
Ntraining = 3;

% get our zero-filled k-space
i1=1;
ksp = i2k(dats{i1}.data./sqrt(mean(abs(dats{i1}.data).^2,'all')), 1);
% and find original samples (less than image resolution due to zero filling)
Nk     =  (abs(ksp(:,1)) > 1e-6);
% set Nk_act to Nk to calculate final acceleration rate only over our
% actually acquired k-space samples
Nk_act = Nk;

fprintf('Phase Encode Lines: %i\n',sum(Nk));

sz = size(dats{1}.data);
n1 = linspace(-1,1, sz(1));

masks = zeros([sz(1), 1, sz(3),  numel(sgms)]);


% iterate over undersampling factors
for i2 = 1 : numel(sgms)
    sgm = sgms(i2);

    % make an exponential probability to sample
    gs = exp(-abs(n1) * sgm);
    gs = gs(:);
    gs = gs / sum(gs(:));
    gs = gs / max(gs(:));
    gs1 = reshape(gs, [sz(1),1, 1, 1]);
    n1  = reshape(n1, [sz(1),1,1, 1]);
    
    % devide an conquer approach to find the right threshold
    iter = 1;
    pau = 5;
    pau_lower = 0;
    pau_upper = 10;
    fprintf('Iter  Current  Target   Threshold\n');
    while true
        % define training area
        training = zeros(sz(1),1,sz(3));
        training(ceil(sz(1)/2)+(fix(-(Ntraining-1)/2):fix(Ntraining/2)),:, :, :) = 1;
        
        % get a random undersampling mask weighted by an exponential
        % (gs1)
        mask = rand(sz(1), 1, sz(3)) < pau .* gs1 .* target_us(i2);
        
        % set training area to 1
        mask(training==1) = 1;

        % remove anything outside of original k-space
        mask(~Nk,:,:) = 0;
        
        % get undersampling factor
        current_us = mean(mask(Nk,:,:),'all');
        
        % print stats
        fprintf(' %3i  %3.1f%%    %3.1f%%    %f\n',iter,current_us*100,target_us(i2)*100, pau);
        
        % good enough?
        if 0.001 > abs(current_us - target_us(i2)) 
            break;
        end
        
        % else devide an conquer
        if current_us > target_us(i2)
            pau_upper = pau;
            pau = (pau_lower + pau_upper)/2;
        elseif current_us < target_us(i2)
            pau_lower = pau;
            pau = (pau_lower + pau_upper)/2;
        end
        iter = iter + 1;
    end
    masks(:,:,:, i2) = mask;
end
fprintf('\n');
%
actual_R = 1./squeeze(mean(masks(Nk_act,:,:,:,:), [1,3, 5]));

fprintf('Acceleration Factors\n');
fprintf('Actual    Target    No. PE Lines\n');
for i=1:numel(sgms)
    fprintf('%.4f    %.4f    %i\n',actual_R(i),target_R(i),sum(masks(:,1,:,i),'all'));
end

% plot the undersampling patterns
try; close(1); catch; end
figure(1);
for i = 1 : numel(sgms)
    subplot(1,numel(sgms), i);
    imagesc(squeeze(masks(:,:,:, i, 1)));
    colormap(gca,gray(256));
    caxis([0 1]);
end
drawnow

%% now run the retrospective experiments
for i1=1:numel(dats)
    fprintf('Recon %i/%i\n',i1,numel(dats));

for i2=1:numel(sgms)
    fprintf('Acceleration %i/%i\n',i2,numel(sgms));

    % get the k-space
    ksp = i2k(dats{i1}.data./sqrt(mean(abs(dats{i1}.data).^2,'all')), 1);
    Nt = size(ksp,3);


    % copy arrays to gpu if possible
    try
        g_coils = gpuArray(single(dats{i1}.coils));
        g_mask = gpuArray(single(masks(:,:,1:Nt, i2, 1)));
        g_ksp = gpuArray(single(ksp)) .* g_mask;
    catch ME
        warning(ME.identifier,'%s',ME.message);
        %    fprintf('Running on CPU\n');
        g_coils = (single(dats{i1}.coils));
        g_mask = (single(masks(:,:,1:Nt, i2, 1)));
        g_ksp = (single(ksp)) .* g_mask;
    end

    % set optimizer options
    opts = [];
    opts.gd_steplen = 0.8;
    opts.debug = false;
    opts.maxiter = 100;
    opts.normalize_coils = false;
    opts.rho = 0.5;
    % weights total variation dimensions against time
    % (spatial_x, spatial_y, time)
    opts.spat_w = [0.05, 0.05, 1, 0];
    opts.cg_iters = 3;

    % regularization weight
    lambda = .15;

    % run optimizer
    [recVTV, itinfo] = recon_admm_VTV(g_ksp, g_coils, g_mask, lambda, opts);
    rec{i1}(:,:,:,i2) = gather(recVTV);
end
end

%% Now do the plotting
export_movie = true;

% setup field of views
x_cuts = {[28:285],[1:260], [70:280]};
y_cuts = {[36:266],[67:284],[45:292]};

clear nrec
deps      = 1e-12;
qbc_scale = [1 1 1]*20;
lim = [];
for i1=1:numel(dats)
    % for visualization purposes: devide recon by body coil image
    % and multiply by a Gaussian mask to supress noise amplification
    nrec{i1}(:,:,:,:) = rec{i1}./(deps+abs(dats{i1}.qbc_img)).* (1-exp(-qbc_scale(i1) .* abs(dats{i1}.qbc_img).^2));
    % we upsample the images to a common width (height is variable due to
    % the different FOVs)
    nrec{i1} = apply2df(nrec{i1},@(x)imresize(abs(x(y_cuts{i1},x_cuts{i1})),320/length(x_cuts{i1})));
    % and obtain the 5%-97% quantiles for plotting
    lim(i1,:) = [quantile(abs(nrec{i1}),.01,'all') quantile(abs(nrec{i1}),.95,'all')];
end

if export_movie
v = VideoWriter('supmov1_cycl.avi');
v.Quality = 97;
v.FrameRate = Nt;
v.open;
end

% width of a white separator bar between angulations
sep = 10;

% figure size and font scaling factor
sz=  1;
% findout size of resulting image:
h=sum(cellfun(@(x)size(x,1),nrec))+(length(nrec)-1)*sep;
w=size(nrec{1},2)*numel(sgms);

% open a new figuren1
try; close(2); catch; end
figure(2)
set(gcf,'Position',[100 100 sz*w sz*h],'color','w')

% some human readable strings to label the plots
hr = {'Long-Axis View','Four-Chamber View','Short-Axis View'};

%
resort = [2 3 1];

% go thru frames
for frame=1:Nt
    % we build one image per frame 
    im_scaled=[];
    sizes = [];

    for i1=resort
        im_scaled_row= [];
        % first build one row
        for i2=1:numel(sgms)
            im_scaled_row = cat(2,im_scaled_row ,nrec{i1}(:,:,frame,i2));
        end
        % scale it according to colormap limits
        im_scaled_row = min(max((im_scaled_row - lim(i1,1))./diff(lim(i1,:)),0),1);
        
        % store its size for text label plotting
        sizes(i1) = size(im_scaled_row,1);
    
        % append row to image
        im_scaled = cat(1,im_scaled, im_scaled_row);
        % and add the separator
        if i1~=resort(end)
            im_scaled = cat(1,im_scaled,ones(sep,size(im_scaled,2)));
        end
    end
    
    % plot..
    subtightplot(1,1,1,0,[0 .05],[0.05, 0]);
    display_image(im_scaled,1);
    caxis([0 1]);
    axis image
    % plot column labels
    for i2=1:numel(sgms)
        text((i2-.5)/numel(sgms),1,sprintf('R = %i (%.1f s)',round(actual_R(i2)),16/actual_R(i2)),'Units','normalized','HorizontalAlignment','Center','VerticalAlignment','bottom','FontSize',20*sz);
    end
    % plot row labels
    cs = 0;
    for i1=resort
        cs = cs +sizes(i1)/2;
        text(0,1-cs./size(im_scaled,1),hr{i1},'Units','normalized','HorizontalAlignment','Center','VerticalAlignment','bottom','Rotation',90,'FontSize',20*sz);
        cs = cs + sizes(i1)/2 + sep;
    end 

    % draw and append frame to video
    drawnow
    if export_movie; v.writeVideo(getframe(gcf));end
    if ~export_movie; break; end
end

if export_movie;close(v);end


