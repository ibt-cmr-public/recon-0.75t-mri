# 0.75T Low-Field MRI - Recon Source Code
**Author:** Christian Guenthner, ETH Zurich, 2023

This repository contains the reconstruction sources for the publication

> **Ramping Down a Clinical 3T Scanner – A Journey into MRI and MRS at 0.75T**
>
> Christian Guenthner, Sophie M. Peereboom, Hannes Dillinger, Charles McGrath, Mohammed M. Albannay, Valery Vishnevskiy, Max Fuetterer, Roger Luechinger, Theo Jenneskens, Urs Sturzenegger, Johan Overweg, Peter Koken, Peter Börnert, Sebastian Kozerke
>
> Magn Reson Mater Phy (2023). https://doi.org/10.1007/s10334-023-01089-9

The publication is publicly available at: https://link.springer.com/article/10.1007/s10334-023-01089-9

In support of the authors: Please cite the above publication if you are reusing or modifying the source code. Thank you!

Table of Contents:
- [Brief Description](#brief-description)
- [Getting Started](#getting-started)
- [External Libraries](#external-libraries)
- [References](#references)


## Brief Description
The repository provides source code and raw data for four 0.75T scans:
- [Field Mapping](#field-mapping)
- [CINE MRI](#cine-mri-comparison)
- [Dixon MRI](#waterfat-separated-mri-at-075t)
- [Water/Fat FISP-MRF](#waterfat-separated-fisp-mr-fingerprinting-on-075t)
- [bSSFP and ME-FFE](#abdominal-bssfp-and-three-echo-dixon-mri)

All figures are directly generated from MATLAB and only show the bare images and parametric maps. Colorbars are generated alongside as high-resolution pngs and were added to the published figures in a post-processing step.

### Field-Mapping
[`Figure2-3_Fieldmaps/recon_fieldmaps.m`](Figure2-3_Fieldmaps/recon_fieldmaps.m) reconstructs 0.75T spiral B0 maps in a self-consistent, iterative manner using multi-frequency interpolation and field-map estimation from dual-echo images. 
In addition, B1+ DREAM scans are reconstructed and interpolated to match the B0 map geometry.

Result is Figure 2: ![Comparison of B0/B1+ in Transversal and Coronal Angulation at 0.75T](Figure2-3_Fieldmaps/fig2.png)

Colorbar for B0 Map:

<img src="Figure2-3_Fieldmaps/cb_B0_-150-200.png" height="200"/>

Colorbar for B1+ Map:

<img src="Figure2-3_Fieldmaps/cb_B1_25-125.png" height="200"/>

Morover, approximate receive fields are mapped by dividing single coil, 1st echo images of the B0 scans by their sum of squares.
Result is Figure 3: ![Comparison of Receive Fields in Transversal and Coronal Angulation at 0.75T](Figure2-3_Fieldmaps/fig3.png)

### CINE MRI Comparison
[`Figure6_CMR-CINE/recon_cine.m`](Figure6_CMR-CINE/recon_cine.m) reconstructs 0.75T retrospective Cardiac CINE MRI scans and compares them to 1.5T and 3T data. 

- Fakes a bodycoil image by downsampling Roemer-combined images
- Performs bodycoil-regularized SENSE recon on 0.75T CINE
- Loads 1.5T and 3T scanner-reconstructed CINE

Result is Figure 6 (Left to right: 0.75T, 1.5T, 3T): ![Comparison of 0.75T, 1.5T and 3T Cine MRI.](Figure6_CMR-CINE/fig6.png)


[`SupVideo1_CINE/recon_movie.m`](SupVideo1_CINE/recon_movie.m) retrospectively undersamples Cardiac CINE MRI Scans and reconstructs them with a vectorial total variation reconstruction.

Result is Supporting Video 1: ![Retrospectively Undersampled Cardiac CINE.](SupVideo1_CINE/supmov1_cycl.avi)

### Water/Fat-separated MRI at 0.75T
[`Figure7_MA-ME-Abdominal/recon_dixon.m`](Figure7_MA-ME-Abdominal/recon_dixon.m) performs iterative water/fat separation with B0 estimation on multi-acquisition Cartesian, multi-echo Cartesian, and multi-acquisition spiral two-point Dixon abdominal acquisitions.

- Performs fake body-coil-regularized SENSE recon on Cartesian and Spiral data
- Performs iterative water/fat/b0 recon on Cartesian data
- Performs iterative water/fat/b0/spiral-deblurring recon using multi-frequency interpolation

Result is Figure 7 (Left: Water, Middle: Fat, Right: B0 Map): ![Comparison of MA and ME Dixon on 0.75T.](Figure7_MA-ME-Abdominal/fig7.png)

Colorbar for B0 Map:

<img src="Figure7_MA-ME-Abdominal/cb_B0_150Hz.png" height="200"/>

### Water/Fat Separated FISP-MR Fingerprinting on 0.75T
[`Figure8_WF-MRF/recon_wf_mrf.m`](Figure8_WF-MRF/recon_wf_mrf.m) performs quantitative imaging on water/fat-separated images using dictionary matching.


- Loads and reconstructs a multi-acquisition spiral Dixon scan to extract a B0 Map for deblurring of the MRF spiral scan
- Loads and reconstructs a B1+ DREAM scan for B1+ informed matching
- Loads the MRF sequence from Philip's raw data and simulates a B1+-resolved EPG
- Performs dictionary compression
- Performs water/fat separation, fat spectrum deblurring in k-space 
- Spiral-deblurring using multi-frequency interpolation
- Fake bodycoil-regularized SENSE coil combination
- MRF matching with a prescribed B1+ map

The result is Figure 8 (Left most column: comparison to multi-acquisition spiral; followed by water/fat images, T1 and T2 maps): ![Left-most Column: Spiral Dixon Water/Fat separated maps; Water/Fat-separated FISP-MRF](Figure8_WF-MRF/fig8.png)

Colorbar for T1 water channel:

<img src="Figure8_WF-MRF/cb_T1_0-1200.png" height="50"/>

Colorbar for T1 fat channel:

<img src="Figure8_WF-MRF/cb_T1_0-400.png" height="50"/>

Colorbar for T2:

<img src="Figure8_WF-MRF/cb_T2_0-200.png" height="50"/>

### Abdominal bSSFP and Three-Echo Dixon MRI
[`Figure9_MultiContrast-Abdominal/recon_multicontrast.m`](Figure9_MultiContrast-Abdominal/recon_multicontrast.m) reconstructs abdominal bSSFP and multi-echo FFE volumes.

- Performs fake bodycoil-regularized SENSE recon
- Performs iterative water/fat separation on multi-echo FFE data

![Multi-contrast Abdominal MRI](Figure9_MultiContrast-Abdominal/fig9.png)

Colorbar for proton-density fat-fraction map: 

<img src="Figure9_MultiContrast-Abdominal/cb_pdff.png" height="200"/>

Colorbar for B0 map: 

<img src="Figure9_MultiContrast-Abdominal/cb_b0.png" height="200"/>

## Getting Started
Make sure you have the LFS (Large File Storage) extension to GIT installed (on Ubuntu: `apt-get install git-lfs`). To clone the full repository, run (this will be ~2GiB!)
```
git clone https://gitlab.ethz.ch/cguenthn/recon-0.75t-mri.git
```


If you want to skip downloading the large raw files and only need the source files, consider disabling LFS before cloning using
```
GIT_LFS_SKIP_SMUDGE=1 git clone https://gitlab.ethz.ch/cguenthn/recon-0.75t-mri.git
```

Please run `init.m` first to initialize path and environment variables. Please make sure you have the mandatory libraries installed before attempting to run the scripts (see below, [External Libraries](#external-libraries)).

Each `Figure..` directory contains one `recon....m` file, which runs the entire reconstruction from Philips raw data to final publication figure. The code has been structured such that we can bypass the proprietary MRecon/Reconframe license by loading intermediate reconstruction results from `.mat` files. Similarly, the EPG simulation result for the dictionary matching has been pre-calculated (requires ~60min on our machine).

For optional libraries, we define fallbacks (such as gray-scale colormaps) using anonymous functions. In case you decide to install optional libraries after running `init.m` make sure to `clear` your workspace.

Feel free to use the code for your own projects - a list of references this code is based on is found at the end of this readme ([References](#refrences)), as well as in the source code.

# External Libraries

## Mandatory - Not Included - Require Manual Installation
### SNAPHU
Statistical cost unwrapping for phase images. Doenload the binaries from the ESA webpage: https://step.esa.int/main/snap-supported-plugins/snaphu/

You can download the snaphu binary using `wget`. Make sure you are located in the base directory of the repository, then run
```
cd _shared_functions/snaphu/
wget http://step.esa.int/thirdparties/snaphu/1.4.2-2/snaphu-v1.4.2_linux.zip
unzip -p snaphu-v1.4.2_linux.zip snaphu-v1.4.2_linux/bin/snaphu > snaphu
rm snaphu-v1.4.2_linux.zip
chmod +x snaphu
```
This will only unpack the binary from the archive and then delete the zip file again. 

There's a wrapper script located in `_shared_functions/snaphu/` that calls the ESA binaries.

### BART
We use bart mainly for coil calibration. Download and compile it from https://github.com/mrirecon/bart.

To clone and compile BART, run  the following from your home directory:

```
mkdir BART
cd BART
git clone https://github.com/mrirecon/bart.git
make
```
For instructions on how to use BART on windows, please look out for BART and WSL (Windows Sub-System for linux).

Make sure to set the environment variable (`setenv('TOOLBOX_PATH','<pathtobart>')`) each time you run MATLAB and also add the bart directory to the current search path (`addpath(genpath('<pathtobart>'))`). In our case, replace `<pathtobart>` by `~/BART/bart`.

## Optional - Not Included - Require Manual Installation
### MRecon (Gyrotools LLC, Optional)
Proprietary MATLAB library for loading and reconstructing Philips Raw datasets including scan meta data.

This toolbox is only necessary if processing should be started from the original data. MATLAB files with processed scan data are available for those that do not possess a valid MRecon license.

For more information visit: http://www.gyrotools.com/gt/index.php/products/reconframe

### Perceptually Uniform Colormaps
Required if colormaps such as viridis and magma should be used. Download from matlab central and store in `_shared_functions/`. Automatically replaced by `gray`-scale colormap if not present.

Ander Biguri (2023). Perceptually uniform colormaps (https://www.mathworks.com/matlabcentral/fileexchange/51986-perceptually-uniform-colormaps), MATLAB Central File Exchange. Retrieved February 9, 2023. 

### colorcet
Colorcet is another colormap function with a plethora of maps, here primarily used for phase-difference plots. Donwload for MATLAB available from https://colorcet.com/download/index.html (direct link: http://www.peterkovesi.com/matlabfns/Colourmaps/colorcet.m) and store in `_shared_functions/`. Automatically replaced by `gray`-scale colormap if not present.

### subtightplot
Like subplot only with more configurable plot margins, gaps, etc. Download from matlab central and store in `_shared_functions/`. Automatically replaced by `subplot` if not present.

Felipe G. Nievinski (2023). subtightplot (https://www.mathworks.com/matlabcentral/fileexchange/39664-subtightplot), MATLAB Central File Exchange. Retrieved February 9, 2023. 

## Included

### export_fig
For exporting high-resolution png images from MATLAB. Export_fig is included as a subproject and cloned directly from github. https://github.com/altmany/export_fig

### Hybrid Bloch-EPG
For calculation of the state-graph for dictionary generation. Prepackaged, original files at https://doi.org/10.5905/ethz-1007-437

Original Reference:
Guenthner, C., Amthor, T., Doneva, M. et al. A unifying view on extended phase graphs and Bloch simulations for quantitative MRI. Sci Rep 11, 21289 (2021). https://doi.org/10.1038/s41598-021-00233-6


# References

## Water/Fat Separation

Dixon WT. Simple proton spectroscopic imaging. Radiology 1984;153:189–194 doi: https://doi.org/10.1148/radiology.153.1.6089263.

Glover GH. Multipoint dixon technique for water and fat proton and susceptibility imaging. J. Magn. Reson. Imaging 1991;1:521–530 doi: https://doi.org/10.1002/jmri.1880010504.

Glover GH, Schneider E. Three-point dixon technique for true water/fat decomposition withB0 inhomogeneity correction. Magn. Reson. Med. 1991;18:371–383 doi: https://doi.org/10.1002/mrm.1910180211.

Brodsky EK, Holmes JH, Yu H, Reeder SB. Generalized K-space decomposition with chemical shift correction for non-Cartesian water-fat imaging. Magn. Reson. Med. 2008;59:1151–1164 doi: https://doi.org/10.1002/mrm.21580.

Yu H, Shimakawa A, McKenzie CA, Brodsky E, Brittain JH, Reeder SB. Multiecho water-fat separation and simultaneous R 2* estimation with multifrequency fat spectrum modeling. Magn. Reson. Med. 2008;60:1122–1134 doi: https://doi.org/10.1002/mrm.21737.


## MRF Matching / Dictionary Generation 
Ma, D. et al. Magnetic resonance fingerprinting. Nature 495, 187–192 (2013).

Jiang, Y., Ma, D., Seiberlich, N., Gulani, V. & Griswold, M. A. MR fingerprinting using fast imaging with steady state precession (FISP) with spiral readout. Magn. Reson. Med. 74, 1621–1631 (2015).

Koolstra K, Webb AG, Veeger TTJ, Kan HE, Koken P, Börnert P. Water–fat separation in spiral magnetic resonance fingerprinting for high temporal resolution tissue relaxation time quantification in muscle. Magn. Reson. Med. 2020;84:646–662 doi: https://doi.org/10.1002/mrm.28143.

Vannieuwenhoven, N.; Vandebril, R.; Meerbergen, K. (2012-01-01). "A New Truncation Strategy for the Higher-Order Singular Value Decomposition". SIAM Journal on Scientific Computing. 34 (2): A1027–A1052. doi: https://doi.org/10.1137/110836067

Guenthner, C., Amthor, T., Doneva, M. et al. A unifying view on extended phase graphs and Bloch simulations for quantitative MRI. Sci Rep 11, 21289 (2021). https://doi.org/10.1038/s41598-021-00233-6

Nehrke K, Börnert P. DREAM-a novel approach for robust, ultrafast, multislice B1 mapping. Magn. Reson. Med. 2012;68:1517–1526 doi: https://doi.org/10.1002/mrm.24158.

## Reconstruction
BART Toolbox for Computational Magnetic Resonance Imaging. doi: https://doi.org/10.5281/zenodo.592960.

Martin Uecker and Michael Lustig. Estimating Absolute-Phase Maps Using ESPIRiT and Virtual Conjugate Coils. Magnetic Resonance in Medicine 77:1201-1207 (2017)

Man L-C, Pauly JM, Macovski A. Multifrequency interpolation for fast off-resonance correction. Magn. Reson. Med. 1997;37:785–792 doi: https://doi.org/10.1002/mrm.1910370523.

"SENSE reconstruction using feed forward regularization" Fuderer, van den Brink, Jurrissen; ISMRM 2004
