                  File: /home/cguenthn/tmp/git_lofi_paper/recon-0.75t-mri/Figure7_MA-ME-Abdominal/data/lo_23082020_1535070_9_2_wip_dixonV4.raw
         Scan Duration: 00:25.7
      Acquisition Type: cartesian
         Sequence Type: FFE
         Contrast Mode: T1
             Scan Mode: 2D
      Readout Duration: 2.632 ms
       Max. Flip Angle: 15.0°
       Repetition Time: 11.5 ms
             Echo Time: 2.9 ms
            Dixon Type: multi-acquisition
   Echo Time Increment: 3.1 ms
                   FOV: 8.0x350.0x350.0
Acquisition Voxel Size: 1.99x1.99x8.00
             Slice Gap: 0.8 
                 SENSE: 1.00x1.00x1.00
           Nr Averages: 4
