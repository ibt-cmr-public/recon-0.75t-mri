% Script produces Figure 7 plots
% Comparisson of Multi-Acquisition/Multi-Echo Cartesian and
% Multi-Acq. Spiral Dixon scans
%
% Brief Description:
% - Performs fake QBC-regularized SENSE recon on Cartesian and Spiral data
% - Performs iterative water/fat/b0 recon on Cartesian data
% - Performs iterative water/fat/b0/spiral-deblurring recon using MFI
% - Creates the plot
%
% (c) Christian Guenthner, ETH Zurich, 2023

clear
run('../init.m')

% scan nrs 
ids = [9 10 11];

% try to load the data either directly from mat files or via mrecon from
% raw data. let's start with the Cartesian
for n=1:2
    if ~exist(scannr(ids(n),'data/','.mat'),'file')
        MR{n} = load_and_normalize(scannr(ids(n),'data/','.raw'));
        raw = struct();
        raw.Data = MR{n}.Data;
        % depending on scan type, we need to extract the echo times
        % differently:
        if strcmpi(MR{n}.Parameter.GetValue('EX_ACQ_dixon_acq'),'multi-echo')
            raw.TE = todim(MR{n}.Parameter.Scan.TE,10);
        else
            TE  = unique(MR{n}.Parameter.Labels.TE);
            dTE = MR{n}.Parameter.GetValue('EX_ACQ_dixon_delta_TE');
            echoes = MR{n}.Parameter.GetValue('EX_ACQ_dixon_acquisitions');
            raw.TE = todim(TE + dTE * (0:(echoes-1)),10);
            clear dTE TE echoes
        end
    
        save([MR{n}.Parameter.Filename.Data(1:end-4) '.mat'],'raw')
    end
end

% ... and then the spiral multi-frequency reconstruction
if isempty(scannr(11,'data/','.mat'))
    raw = mf_spiral(scannr(11,'data/','.raw'),...
        'mfi_frequencies',linspace(-200,100,19)'/1000 ...
        );
    % NOTE: mfi frequencies are an educated guess - if you have no clue
    % what your B0 field looks like, choose a large domain and refine it
    % once you have a B0 estimate.
    
    save([raw.filename(1:end-4) '.mat'],'raw')
end

% load data from mat files and get sensitivities with bart
for n=1:3
    d=load(scannr(ids(n),'data/','.mat'));
    data{n} = d.raw;
    % estimate sensitivities
    S{n} = estim_sensitivities(data{n}.Data);
end

% do roemer coil-combination, fake QBC estimation, and regularized sense
for n=1:3
    d_roemer{n} = sum(data{n}.Data .* conj(S{n}),4);

    % let's fake a qbc image via blurring..
    qbc_img{n} = imgaussfilt(abs(d_roemer{n}(:,:,:,1)),20);
    % NOTE: for n=3 we're dealing with multi-frequency demodulated spiral
    % reconstructions.
    % Since the fake body coil image is very low-res anyhow
    % we don't bother about MFI / spiral deblurring here and simply
    % take the 0Hz demodulated, 1st echo of our dataset.

    % we did a regularized sense recon with 51 regularizations, then
    % manually picked our favorite
    d_raw{n} = qbc_reg_sense(data{n}.Data,S{n},qbc_img{n},'lambda',0.3);
end

%% Do iterative water/fat separation and b0 estimation on Cartesian Data
for n = 1:2
    [wf{n},b0_map{n},dTE{n}] = iterative_wfb0(d_raw{n}, data{n}.TE);
end

%% now perform the same for the spiral data with concurrent multi-frequency
% interpolation based on the estimated field map.
n=3;
[wf{n},b0_map{n},dTE{n},d_echoes{n}] = iterative_wfb0_mfi(d_raw{n},data{n});

%% ... and plot
p=1;
try;close(1); catch; end
figure(1);
set(gcf,'Position',[100 100 600 600],'Color','w')

for id=1:length(wf)
subtightplot(length(wf),3,1+(id-1)*3);
display_image(abs(wf{id}(:,:,1)),p);

h=subtightplot(length(wf),3,2+(id-1)*3);
display_image(abs(wf{id}(:,:,2)),p);

h=subtightplot(length(wf),3,3+(id-1)*3);
imagesc(real(b0_map{id}))
caxis([-150 150])
colormap(h,colorcet('C8'));
axis square
set(gca,'visible','off')
end

export_fig('fig7.png','-m4')

%% and create a colorbar for the B0 map
writecolorbar('cb_B0_150Hz.png',arrayfun(@(x)sprintf('%i Hz',x),[-150:50:150],'UniformOutput',false),'colormap',colorcet('C8'),'orientation','vert')