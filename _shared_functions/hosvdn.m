function [r,u,svals] = hosvdn(r, dim,svd_tol)
%HOSVDn Performs sequentially truncated higher-order SVD with singular
%value thresholding along the specified dimensions.
% 
% Input:
%   r           tensor to decompose
%   dim         dimension order to perform svds along
%   svd_tol     svd relative cutoff value
% Output:
%   r           core tensor
%   u           transformation matrices
%   svals       singular values along each dimension before thresholding
%
% Based on:
% A NEW TRUNCATION STRATEGY FOR THE HIGHER-ORDER SINGULAR VALUE DECOMPOSITION
% NICK VANNIEUWENHOVEN , RAF VANDEBRIL , AND KARL MEERBERGEN
% SIAM Journal on Scientific Computing. 34 (2): A1027-A1052. 
% doi:10.1137/110836067
%
% (c) Christian Guenthner, ETH Zurich, 2023

if nargin<2; dim=1:ndims(r); end
if nargin<3; svd_tol=[]; end

    for n=dim
        order = [n setdiff(1:ndims(r),n)];
        r= permute(r,order);

        sz = size(r);

        % flatten to matrix and do svd
        r = reshape(r,sz(1),[]);
        [u{n},s,v] = svd(r,'econ');
        
        % do thresholding
        s0 = abs(diag(s));
        svals{n} = s0;
        if isempty(svd_tol)
            I = length(s0);
        else
            s0 = s0 ./ max(s0(:));
    
            I = find(s0 < svd_tol,1,'first');
            if isempty(I); I = length(s0); end
        end
        sz(1) = I; 
        u{n} = u{n}(:,1:I);

        % project to subspace and reshape to tensor    
        r = u{n}' * r;
        r = ipermute(reshape(r,sz),order);
    end

end