function writecolorbar( filename, names, varargin )
%WRITECOLORBAR Renders a colorbar and saves it as a highresolution image
% 
% filename      The filename with extension to write to. If empty, the colorbar is being displayed instead.
% names         A cell array of label strings, e.g. {"0%","50%","100%"}.
%
% Optional Name/Value Pairs
% 'colormap'    Set the colormap of the colorbar. Can be either a string identifying a default colormap, or a color array
% 'color'       Sets the background color
% 'ticks'       Sets where ticks should be drawn. Must be an array equal in length to 'names' with values between 0..1 (inclusive)
% 'orientation' Sets the orientation of the colorbar. Must be either 'vertical' or 'horizontal'
% 'fontsize'    Sets the font size of the colorbar
% 'size'        Set the w/h of the figure in relative units
% 'width'       Sets the width of the colorbar relative to the full size
%
% Requires: export_fig
%
% AUTHOR: Christian Guenthner, guenthner@biomed.ee.ethz.ch
% STARTED: 2018-07-05
%
% (c) Christian Guenthner, ETH Zurich, 2023

% Definition of some colormaps (similar to explore)
map1 = interp1([0;.5;1],[130, 190, 30; 31, 64, 122; 234, 236, 238]/255,linspace(0,1,256));
map2 = interp1([0;.5;1],[31, 64, 122; 130, 190, 30; 234, 236, 238]/255,linspace(0,1,256));
map3 = interp1([0;.5;1],[31, 64, 122; 234, 236, 238; 130, 190, 30]/255,linspace(0,1,256));
map4 = interp1([0;.5;1],[133, 52, 59; 255, 255, 255; 52, 56, 133]/255,linspace(0,1,255));
map5 = interp1([0;.25;.5;.75;1]	,[0, 0, 0; 90, 255, 73; 29,34,112; 218, 27, 27;255 255 255]/255,linspace(0,1,255));
map6 = interp1([0;.25;.5;.75;1],[52, 56, 133; 133, 52, 59 ; 255, 255, 255; 52, 133, 59 ; 52, 56, 133]/255,linspace(0,1,255));
map7 = interp1([0;.5;1],[52, 56, 133; 133, 52, 59; 255, 255, 255]/255,linspace(0,1,255));
map8 = interp1([0;1],[0, 0, 0; 255, 255, 255]/255,linspace(0,1,255));
map9 = interp1([0;.2;3/6;4/6;5/6;1],[255 255 255; 0, 0, 0; 255 255 255; 255 0 0; 0 0 255; 255 255 255]/255,linspace(0,1,256));
map10 = interp1([0;.5;1],[255, 77, 77; 0, 0, 0; 77, 126, 255]/255,linspace(0,1,255));
map11 = interp1([0;.33;.66;1],[0, 0, 0; 52, 56, 133; 255, 255, 255; 133, 52, 59]/255,linspace(0,1,256));
map12 = interp1([0;.25;.5;.75;1],[1 1 1; 0 0 0; 22/255 164/255 234/255; 241/255 225/255 53/255; 241/255 53/255 54/255],linspace(0,1,256));

try
custommaps = {'eth','eth1','eth2','diff','amp','phase','brw','grayhires','bw','inferno','fakeparula','magma','plasma','viridis','jet','diffdark','stiffhad','stiff','hothires'};
colormaps = {'parula','default','gray','hot','copper','pink','hsv',map1,map2,map3,map4,map5,map6,map7,map8,map9,inferno(256),fake_parula(256),magma(256),plasma(256),viridis(256),jet(256),map10,map11,map12,hot(256)};
catch
    custommaps = {'eth','eth1','eth2','diff','amp','phase','brw','grayhires','bw','jet','diffdark','stiffhad','stiff','hothires'};
colormaps = {'parula','default','gray','hot','copper','pink','hsv',map1,map2,map3,map4,map5,map6,map7,map8,map9,jet(256),map10,map11,map12,hot(256)};
end
% defaults
def_cm_index = 1; % colormap index
fs = 20;
color=[1 1 1]; % background color
ticks = linspace(0,1,length(names)); % standard ticks run between 0 and 1 in number of labels provided
ori = 'vertical'; % orientation is by default vertical
defwh = []; 
defa = 0;
defpos = 'in';
def_width = 0.9;

% let's interpret varargin (the bad = manual way; here p = inputParser; would be better; checkout explore.m if you want to know how one can use it.)
if nargin > 2
    if mod(nargin-2,2) ~= 0
        error('data needs to be followed by an even number of arguments');
    end
    
    for n=1:(nargin-2)/2
        aid=2*n-1;
        %test type of argument
        switch( varargin{aid} )
            case 'tickpos'
                defpos = varargin{aid + 1};
            case 'tickangle'
                defa = varargin{aid + 1};
            case 'width'
                def_width=  varargin{aid + 1};
            case 'size'
                defwh = varargin{aid + 1};
            case 'colormap'
                cm = varargin{aid + 1};
                if ~ischar(cm)
                    if ismatrix(cm)
                        colormaps = [colormaps cm];
                        def_cm_index = length(colormaps);
                    else
                        def_cm_index = cm;
                    end
                else
                    cm2 = find(strcmpi(cm,colormaps));
                    if isempty(cm2)
                        cm2 = find(strcmpi(cm,custommaps));
                        if ~isempty(cm2)
                            def_cm_index = length(colormaps)-length(custommaps)+cm2(1);
                        else                            
                            error('unknown colormap "%s". Use one of %s',cm,strjoin(colormaps,', '));
                        end
                    else
                        def_cm_index = cm2(1);
                    end
                end
            case 'color'
                color = varargin{aid+1};
            case 'ticks'
                ticks = varargin{aid+1};
                assert(length(ticks)==length(names));
            case 'orientation'
                ori = varargin{aid+1};
            case 'fontsize'
                fs = varargin{aid+1};
            otherwise
                error('unknown option "%s" is being ignored.',varargin{aid});
        end
    end
end

switch ori
case {'vertical','vert','v'}
    fig1=figure('Visible', 'off');
    for iter=1:3
        %set(gcf, 'Visible', 'on');
        set(gcf, 'color', color);

        width=140 ; height=500;
        hbar = def_width;
        wbar = .2; %
        wmarg = .1; hmarg = (1-hbar)/2;
        if ~isempty(defwh)
            wbar = wbar /defwh(1);
            hbar = hbar /defwh(2);
            
            wmarg = wmarg / defwh(1);
            hmarg = hmarg / defwh(2);
            
            width = width*defwh(1);
            height = height*defwh(2); 
        end

        left=100; bottom=100 ; 
        pos=[left bottom width height];

        clf
        colormap(colormaps{def_cm_index});
        h=colorbar('WestOutside');
        caxis([0 1])
        axis off
        set(h,'Position',[1-wbar-wmarg 1-hbar-hmarg wbar hbar]) 
        
        set(h,'FontSize',fs,'AxisLocation','in','AxisLocationMode','manual')
        set(h,'TickLabelInterpreter','tex','Ticks',ticks,'TickLabels',names,'TicksMode','manual','TickDirection',defpos);
        h.Ruler.TickLabelRotation=defa;
        set(gcf,'OuterPosition',pos) 
        drawnow;
    end
    if ~isempty(filename)
        % calls export_fig with 8x upsampling for colormap export.
        export_fig(gcf,filename,'-m8','-nocrop')
        close(gcf);
    else
        set(gcf, 'Visible', 'on');
    end
case {'hoizontal','horz'}
    fig1=figure('Visible', 'off');
    if ~isempty(defwh)           
        defwh=flip(defwh);
    end
    for iter=1:3
        %set(gcf, 'Visible', 'off');
        set(gcf, 'color', color);
                
        width=450 ; height=130+fs; % horizontal
        wbar = def_width;
        hbar = .2;
        wmarg = (1-wbar)/2; hmarg = .1;
        if ~isempty(defwh)
            wbar = wbar /defwh(1);
            hbar = hbar /defwh(2);
            
            wmarg = wmarg / defwh(1);
            hmarg = hmarg / defwh(2);
            
            width = width*defwh(1);
            height = height*defwh(2); 
        end

        left=100; bottom=100 ; 
        pos=[left bottom width height];
        clf
        
        colormap(colormaps{def_cm_index});
        h=colorbar('SouthOutside');
        caxis([0 1])
        axis off
    
        set(h,'Position',[1-wbar-wmarg 1-hbar-hmarg wbar hbar])
        %set(h,'Position',[.1 .7 .8 .2]) 
        set(h,'FontSize',fs,'AxisLocation','in','AxisLocationMode','manual')
        set(h,'TickLabelInterpreter','tex','Ticks',ticks,'TickLabels',names,'TicksMode','manual','TickDirection',defpos);
        h.Ruler.TickLabelRotation=defa;
        set(gcf,'OuterPosition',pos);
        
    end
    if ~isempty(filename)
        % calls export_fig with 8x upsampling for colormap export and transparency
        export_fig(gcf,filename,'-m8','-nocrop','-transparent')
        close(gcf);
        %set(gcf, 'Visible', 'on');
    else
        set(gcf, 'Visible', 'on');
    end
otherwise
    error('Unsupported Orientation. (%s)',ori);
end

end
