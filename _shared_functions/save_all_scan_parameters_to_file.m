% short script that triggers .txt scan parameter file creation for all .raw
% data files.
%
% (c) Christian Guenthner, ETH Zurich, 2023

% find all raw data files
filelist = dir(fullfile('../', '**/*.raw'));  %get list of files and folders in any subfolder
filelist = filelist(~[filelist.isdir]);

fprintf('Found %i .raw files. Generating parameter lists...\n',length(filelist));
%
for n=1:length(filelist)
    fprintf('%s\n',fullfile(filelist(n).folder,filelist(n).name));
    % trigger parameter .txt file creation
    print_scan_parameter(fullfile(filelist(n).folder,filelist(n).name),true);
end