function MR = load_and_normalize(rawfile, varargin)
%% load_and_normalize short wrapper function to load Philip's raw data files
% Normalizes raw data by noise channel, disables geometry correct and
% coil-combination, ensures that slices are always in the 3rd dimension
% (multi-slice data is found in dimension 8)
% NB: this is untested for multi-stack w/ multi-slice). 
%
% This function requires a valid MRecon license!
%
% (c) Christian Guenthner, ETH Zurich, 2023
p = inputParser;
p.addOptional('remove_oversampling','YES');
p.parse(varargin{:});

MR=MRecon(rawfile);
MR.Parameter.Recon.CoilCombination='NO';
MR.Parameter.Recon.GeometryCorrection='NO';
MR.Parameter.Recon.RemoveMOversampling=p.Results.remove_oversampling;
MR.Parameter.Recon.RemovePOversampling=p.Results.remove_oversampling;

MR.ReadData;
MR.RandomPhaseCorrection;
MR.RemoveOversampling;
MR.PDACorrection;
MR.DcOffsetCorrection;
MR.MeasPhaseCorrection;
MR.SortData; 
MR.GridData;
MR.RingingFilter;
MR.ZeroFill;
MR.K2IM;
MR.EPIPhaseCorrection;
MR.K2IP;
MR.GridderNormalization;

if iscell(MR.Data)
    noiselevel = std(MR.Data{5},[],1);
    MR.Data{1} = MR.Data{1} ./ noiselevel;
end

%r.SENSEUnfold;
MR.PartialFourier;
MR.ConcomitantFieldCorrection;
MR.DivideFlowSegments;
%MR.CombineCoils;
%MR.Average;
%MR.GeometryCorrection;
MR.RemoveOversampling;
MR.FlowPhaseCorrection;
MR.ReconTKE;
MR.ZeroFill;
MR.RotateImage;

if iscell(MR.Data)
    % strip remaining data
    MR.Data = MR.Data{1};
end

% and permute 8-th dimension to 3-rd (multi-slice to 3D)
if size(MR.Data,3)==1
    MR.Data = permute(MR.Data,[1 2 8 4:7 3 9:ndims(MR.Data)]);
end