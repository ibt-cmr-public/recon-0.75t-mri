function  snaphu_writecomplex(fn,D)
%SNAPHU_WRITECOMPLEX Writes complex data to file
D(isnan(D)) = 0;

f=fopen(fn,'wb');
D = reshape([real(D(:)),imag(D(:))].',[],1);
fwrite(f,D(:),'float',0,'ieee-le');
fclose(f);
end

