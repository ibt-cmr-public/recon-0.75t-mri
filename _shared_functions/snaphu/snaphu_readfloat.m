function D = snaphu_readfloat(fn,sz)
%snaphu_readcomplex Reads complex data to file

f=fopen(fn,'rb');
D = fread(f,prod(sz),'float',0,'ieee-le');
fclose(f);


D=reshape(D,sz);
end

