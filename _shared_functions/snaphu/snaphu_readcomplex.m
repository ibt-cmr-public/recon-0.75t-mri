function D = snaphu_readcomplex(fn,sz)
%snaphu_readcomplex Reads complex data to file

f=fopen(fn,'rb');
D = fread(f,prod(sz)*2,'float',0,'ieee-le');
fclose(f);


D=reshape(D,2,[]);
D=complex(D(1,:),D(2,:));
D=reshape(D,sz);
end

