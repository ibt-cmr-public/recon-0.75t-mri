function  snaphu_writefloat(fn,D)
%SNAPHU_WRITECOMPLEX Writes complex data to file
D(isnan(D)) = 0;
assert(all(isreal(D(:))),'Data needs to be real.');

f=fopen(fn,'wb');
fwrite(f,D(:),'float',0,'ieee-le');
fclose(f);
end

