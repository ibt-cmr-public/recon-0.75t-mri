function U=unwrap_snaphu_mag(S, chunks)
%%unwrap_snaphu_mag Calls SNAPHU to unwrap phase of complex image
% Input: 
%   S   Complex magnitude/phase image to unwrap
% Output:
%   U   Unwrapped phase
%
% Author: Christian Guenthner, guenthner@biomed.ee.ethz.ch
% Date: 2018-08-14

%obtain filepath    
path = (mfilename('fullpath'));
path = fileparts(path);

%obtain temporary filenames
tmp = tempname;
tmpdata = [tmp '.in'];
tmpuw = [tmp '.out'];

% store input size
sz = size(S);

if nargin<2 || isempty(chunks)
    chunks = ceil(prod(sz(2:end))/16000+.1); 
    if chunks>1
        fprintf('Splitting Unwrap Task into %i Chunks\n',chunks);
    end
end

% get chunk sizes
tot = prod(sz(3:end));
maxchunklen = ceil(tot / chunks);
S = S(:,:,:);
chkindex=1;
index=cell(1,chunks);

U=zeros(size(S));
for n=1:chunks
    if chunks>1
        fprintf('Unwrapping Chunk %i/%i\n',n,chunks);
    end
    index{n} = chkindex:min(size(S,3),chkindex+maxchunklen);
    chkindex = chkindex + maxchunklen;
    if isempty(index{n}); warning('Somethings wrong with the chunking code...'); break; end

    % write data to temp file
    snaphu_writecomplex(tmpdata,S(:,:,index{n}));

    % call snaphu (smooth; complex input)
    ret = unix(sprintf('%s/snaphu %s %i -o %s -s',path,tmpdata,sz(1),tmpuw));
    if ret>0
       delete(tmpdata);
       error('Call to SANPHU failed.');
    end

    % read data (real: magnitude, imag = unwrapped phase)
    U(:,:,index{n}) = imag(snaphu_readaltline(tmpuw,[sz(1) sz(2) length(index{n})]));
end
U = reshape(U,sz);

% remove tempfiles
delete(tmpdata);
delete(tmpuw);

end