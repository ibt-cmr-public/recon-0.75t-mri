function out=apply2df(data,func)
sz = size(data);
data = reshape(data,[sz(1) sz(2) prod(sz(3:end))]);

sz_out = size(func(data(:,:,1)));
out = zeros(prod(sz_out),prod(sz(3:end)));
sz_out = cat(2,sz_out,sz(3:end));

for n=1:size(data,3)
    out(:,n) = reshape(func(data(:,:,n)),[],1);
end

out = reshape(out,sz_out);

end