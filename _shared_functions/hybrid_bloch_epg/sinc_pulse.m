function rf = sinc_pulse(varargin)
%% sinc_pulse Hann Apodized Sinc-Pulse for Slice-Profile Simulations
% 
% Copyright (c) 2021 ETH Zurich, Christian Guenthner, Thomas Amthor, Mariya Doneva, Sebastian Kozerke

p = inputParser;
p.KeepUnmatched = 1;
p.addRequired('FA');             % flip angle in radians
p.addParameter('n',   1024);     % number of temporal sampling points
p.addParameter('tbwp',   5*2);   % approximate time-bandwidth product = number of total zero crossings
p.addParameter('gamma',  42577.469*2*pi/1000); % gamma = 2*pi*42.577469 MHz/T
p.addParameter('slice_thickness',.001); % slice thickness (default: 1 mm)
p.addParameter('pulse_duration' ,1);    % duration of the pulse (default: 1ms)
p.addParameter('Nsp',101);       % number of sample points for slice profile
p.addParameter('fov',.003);      % fov of slice profile calculation
p.addParameter('reference_point',[]);% focus point, if empty, calculated via global optimization. else a number between 0...1
p.parse(varargin{:});

% copy input parameter
flipangle = p.Results.FA;
n = p.Results.n;
tbwp = p.Results.tbwp;
gamma = p.Results.gamma;
gammabar = gamma / 2 /pi; %MHz/T

% number of zero crossings left and right of the central lobe
nz = tbwp / 2;

% create time axis
t = linspace(-nz, nz, n);
dt = t(2)-t(1);

% create cosine filter
alpha = 0.5;
filter = (1-alpha)+ alpha*cos(pi*t/nz);

% multiply sinc with filter
b1p = filter .* sinc(t);

% set signal zero outside apod limits
b1p(t<-nz) = 0;
b1p(t>nz)  = 0;

% calculate effective flip angle
fapulse = gamma * trapz(b1p) * dt;

% scale pulse to get right flip angle
b1p = b1p(t>=-nz & t<=nz) / fapulse * flipangle;

% set RF pulse struct
rf = struct();
rf.flip_angle = flipangle;
rf.nsamples   = length(b1p);
rf.peak       = max(b1p);
rf.gammabar   = gammabar;
rf.gamma      = gamma;
rf.slice_thickness = 1;
rf.bandwidth_ex = tbwp;

% set pulse duration & slice thickness
rf.T_ex = p.Results.pulse_duration;
rf.dt = rf.T_ex / rf.nsamples;
rf.slice_thickness = p.Results.slice_thickness;
% set shape according to pulse duration
rf.shape      = b1p ./ rf.dt * dt;

% obtain slice selection gradient strength
rf.G_ex = rf.bandwidth_ex / rf.gammabar / rf.slice_thickness / rf.T_ex;

% assume symmetric pulse for a good start to determine the reference time
rf.A_pre = 0;
rf.A_post = 0;

if isempty(p.Results.reference_point)
    % if no user defined focus_point is given, calculate it via global optimization. 
    % determine slice profile
    [rfmat,pos] = RF_matrix(rf,rf.flip_angle,'Nsp',p.Results.Nsp,'fov',p.Results.fov);

    % FID signal after excitation of magnetization
    signal = @(x)abs(trapz(exp(-1i.*gamma*pos.*rf.T_ex .* rf.G_ex .* x).*rfmat(1,3,:),3));
    % find optimal reference time point
    problem=createOptimProblem('fmincon',...
        'objective',@(x)-signal(x),...
        'x0',0.5,'options',optimoptions(@fmincon,'Algorithm','sqp','Display','Off'));
    problem.lb = 0;
    problem.ub = 1;
    rng(14,'twister'); % for reproducibility
    % run optimizer
    gs = GlobalSearch('Display','off');
    x0 = gs.run(problem);
else
    x0 = p.Results.reference_point;
end

% add solution
rf.reference_point = x0;
% and update rf matrix with new reference point calculation
rf.A_post = -rf.reference_point.*rf.T_ex.*rf.G_ex;

% and define the prephaser symmetrically:
rf.reference_point_echo = 1 - rf.reference_point;
rf.A_pre = -rf.reference_point_echo.*rf.T_ex.*rf.G_ex;

% final update of the slice profile for storage
[rfmat,pos] = RF_matrix(rf,rf.flip_angle,'Nsp',p.Results.Nsp,'fov',p.Results.fov);

% store it.
rf.slice_profile = rfmat;
rf.pos = pos;

end



