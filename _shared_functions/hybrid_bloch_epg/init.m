%% Initializes fallback options in case of missing optional packages
%
% Copyright (c) 2021 ETH Zurich, Christian Guenthner, Thomas Amthor, Mariya Doneva, Sebastian Kozerke

% setup output directory
outdir = 'output/';
mkdir(outdir)

% Setup fallback options for optional packages:
% viridis
clear viridis magma inferno
if ~exist('viridis'); viridis = @(x)hot(x); end
if ~exist('magma'); magma = @(x)jet(x); end
if ~exist('inferno'); inferno = @(x)parula(x); end

% colorcet ()
clear colorcet
if ~exist('colorcet'); colorcet = @(x)hsv(256); end

% Nievinski's subtightplot (https://ch.mathworks.com/matlabcentral/fileexchange/39664-subtightplot)
clear subtightplot
if ~exist('subtightplot'); subtightplot = @(m,n,o,varargin)subplot(m,n,o); end

% Woodford's & Altmany's export_fig (https://github.com/altmany/export_fig)
clear export_fig
if ~exist('export_fig')    
    export_fig = @(file,varargin)warning('''export_fig'' not present. Image %s not written.',file);
    warning('export_fig is not in the search path. High-Resolution images are not exported.');
end

mkdir([outdir '/figure4'])
mkdir([outdir '/figure6'])