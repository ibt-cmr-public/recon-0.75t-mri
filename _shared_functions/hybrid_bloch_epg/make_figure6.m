%% make_figure6 Signal magnitude & phase vs. time plot for bSSFP and configuration state imaging vs. offresonance
%
% Copyright (c) 2021 ETH Zurich, Christian Guenthner, Thomas Amthor, Mariya Doneva, Sebastian Kozerke

configuration = {[] -2 -1 0 1};
ksp_plot      = 2*pi /slice_thickness * 4 * [0 1 1 1 1];
R2p           = 0;
omega_for_plot = todim(linspace(-pi,pi,501)/TR,7);
omega_for_plot = omega_for_plot(1:end-1);

clear Splot
for n=1:5    
    Splot(:,:,:,n) = squeeze(hybrid_bloch_epg(S,rf.pos,kstates,E2,ksp_plot(n),TR,TE,configuration{n},omega_for_plot,R2p));

    if n>1
        Sref = squeeze(hybrid_bloch_epg(S,rf.pos,kstates,E2,inf,TR,TE,configuration{n},omega_for_plot,R2p));
        Splot(:,:,:,n) = Splot(:,:,:,n) ./ Sref .* abs(Sref);
    else
        % correct for RF pulse phase:
        Splot(:,:,:,n) = Splot(:,:,:,n) .* todim(exp(1i*angle(FA(2:end))-1i*pi/2),1);
    end
end

ii = abs(omega_for_plot * TR / pi);
colors = interp1(linspace(0,1,256),colorcet('C9'),ii(:));
colors(:,4) = .05;

figure
set(gcf,'Position',[100 100 1600 600]);%./squeeze(Sfisp(:,Id,Ie))
set(gcf,'Color',[1 1 1]);

magscales = {[1 2.5 2.5 2.5 2.5],[1 2.5 2.5 2.5 2.5]};

globscale = max(max(max(abs(Splot(:,:,:,:)),[],1),[],3),[],4);

for Ids = [1 2]; % dictionary index
    magscale      = magscales{Ids}./max(globscale);
    
for n=1:5
    subtightplot(2,5,n,.025,.1,.1);
    
    for m=1:size(Splot,3)
        p = plot(squeeze(abs(Splot(:,Ids,m,n))) * magscale(n),'LineWidth',1,'Color',colors(m,:)); hold on
    end
    hold off
    
    box on
    grid on
    set(gca,'FontSize',16);
    xlim([0 inf]);
    
    ylim([0 round(globscale(Ids)/max(globscale) * 100+5)/100]);
    xticklabels([])

    xticks([0:25:150])
    if n>1
        yticklabels([])
        title(sprintf('Spoiled SSFP - F(%i)',configuration{n}));
    else
        ylabel('Signal Magnitude')
        title('Balanced SSFP')
    end
    axis square

    subtightplot(2,5,n + 5,.025,.1,.1);
    for m=1:size(Splot,3)
        p = plot(squeeze(unwrap(angle(Splot(:,Ids,m,n)))),'LineWidth',1,'Color',colors(m,:)); hold on
    end
    hold off
    
    box on
    grid on
    set(gca,'FontSize',16);
    xlim([0 inf]);
    ylim([-5 5]);
    yticks([-3/2*pi:pi/2:3/2*pi])
    xticks([0:25:150])
    yticklabels({['-3' char(960) '/2'],['-' char(960)],['-' char(960) '/2'],'0',[char(960) '/2'],char(960),['3' char(960) '/2']})
    axis square
    xlabel('Time-Point')

    if n>1
        yticklabels([])
    else
        ylabel('Unwrapped Signal Phase')
    end
    
    drawnow
end

export_fig(sprintf('%s/figure6/figure6_Id%i.png',outdir,Ids),'-m4');

end
