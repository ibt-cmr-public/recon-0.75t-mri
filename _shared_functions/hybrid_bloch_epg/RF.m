function out=RF(alpha)
% RFmat(alpha) Rotation Matrix for EPG/Bloch Simulations
% alpha   complex flip angle
% -      |alpha|  flip angle in radians
% - angle(alpha)  RF pulse phase
%
% Supports high dimensional input of flipangles, first two dimensions must
% be singleton (alpha = 1x1x...). The output will then be a 3x3x... array.
%
% from: 
% Weigel, M. Extended phase graphs: Dephasing, RF pulses, and echoes - pure and simple. (2015)
% 
% Copyright (c) 2021 ETH Zurich, Christian Guenthner, Thomas Amthor, Mariya Doneva, Sebastian Kozerke

    phi = angle(alpha);
    alpha = abs(alpha);
  
    out = [(cos(alpha/2)).^2 exp(2*1i*phi).*(sin(alpha/2)).^2 -1i*exp(1i*phi).*sin(alpha);
      exp(-2*1i*phi).*(sin(alpha/2)).^2 (cos(alpha/2)).^2 1i*exp(-1i*phi).*sin(alpha);
      -1i/2*exp(-1i*phi).*sin(alpha) 1i/2*exp(1i*phi).*sin(alpha)      cos(alpha)];
end