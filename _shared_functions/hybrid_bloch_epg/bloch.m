function [S]=bloch(varargin)
%% bloch(FA,rf,TR,E1,E2,...) Performs a spatially resolved EPG Simulation with 
% unit dephasing at the end of each TR
%
% Sequence: (RF - Readout - Relaxation - Spoiling)
%
% Copyright (c) 2021 ETH Zurich, Christian Guenthner, Thomas Amthor, Mariya Doneva, Sebastian Kozerke

p = inputParser;
p.KeepUnmatched = 1;
if nargin~=0
    % for default struct.
    p.addRequired('FA');
    p.addRequired('rf');
    p.addRequired('TR');
    p.addRequired('E1');
    p.addRequired('E2');
end
p.addParameter('pos',[]);
p.addParameter('ksp', 0); 
p.addParameter('acquisition_on',[]);
p.addParameter('export_configuration',[]);
p.addParameter('gamma',42577.469*2*pi/1000); % gamma = 2*pi*42.577469 MHz/T
p.addParameter('debug',0);
p.addParameter('relax_during_pulse',1);
p.parse(varargin{:});

% enable/disable debugging/profiling statements
dispdebug = @dispdebug_internal;
if p.Results.debug==0; dispdebug=@(x)0; end

% create a default struct we can fill (no arguments provided).
if nargin==0
    S = p.Results;    
    if nargout==0
        fprintf('Call bloch(FA,rf,TR,E1,E2,...). Where ... are parameters with defaults returned by this call.\n');
    end
    return;
end

% copy to local variables
FA = p.Results.FA;
rf = p.Results.rf;
TR = p.Results.TR;
E1 = p.Results.E1;
E2 = p.Results.E2;
ksp = p.Results.ksp;
pos = p.Results.pos;
aq = p.Results.acquisition_on;
cfg = p.Results.export_configuration;
gamma = p.Results.gamma;               % rad MHz/T

N = length(FA);
if length(TR)==1
    TR = ones(1,1,1,N) * TR;
end

if isstruct(pos)
    % create linear position array
    pos = (1:pos.N).*pos.dx;
    pos = pos - mean(pos);
    pos.positions = pos;
end

% defaults based on other inputs
if isempty(aq); aq=ones(1,1,1,N); end
if isempty(cfg); cfg=0; end

assert(length(aq)==N,'Length acquisition_on must equal RFmat[4]');
assert(all(cfg == sort(cfg)),'Configuration state output vector must be in ascending order');

% size of study parameters
szstudy = combinedsize(10,E1,E2);
assert(all(szstudy(2:5)==1),'Some dimensions are wrong for input paramters (dim 2:5 must be singleton)');
assert(all(szstudy(10:end)==1),'Maximum non-singleton dimension 9');

% number isochromats
Nz = size(pos,3);

% define size of magnetization vector
% 1 x 3 x Isochromats in Slice Direction x 1 x 1 x study parameters
szF = [1,3,Nz,1,1,szstudy(6:9),1];
F = zeros(szF);

% define reshape sizes for the different matrix operations
szRF = [prod(szF(1:2)) prod(szF(3:end))];

% Set Equilibrium Magnetization
F(1,3,:,:,:,:,:,:,:,1)=1; %< equilibrium magnetization (Z(:)=1)

% define indices for configuration state export and their sorting
ICfg = 1:length(cfg);

% how many states do we export?
Ncfgexport = length(cfg);

% signal output vector
szS = [sum(aq==1) 1 Nz,1,1,szstudy(6:9),Ncfgexport];
S = zeros(szS);

% simulation loop
tic;
for n=1:N    
    % time tracker
    time = 0;
    
    dispdebug('RF');
    
    % prepare AM shape & apply RF pulse
    if abs(abs(FA(n))-pi) < 10*eps
        % adiabatic 180° pulse
        F = reshape(RF(pi) * reshape(F,szRF),szF);
    else
        %let's use sinc pulses for everything else
        % apply pre-winder gradient
        F(1,1,:,:,:,:,:,:,:,1) = F(1,1,:,:,:,:,:,:,:,1) .* exp( 1i * rf.A_pre.*pos.*gamma);
        F(1,2,:,:,:,:,:,:,:,1) = F(1,2,:,:,:,:,:,:,:,1) .* exp(-1i * rf.A_pre.*pos.*gamma);
        
        % define shape and normalize to flip angle
        B1 = rf.shape;
        fapulse = gamma .* trapz(B1) .* rf.dt;
        B1 = B1 ./ fapulse .* FA(n);

        % iteratve over RF steps
        for m=1:length(B1)
            % apply tiny-hardpulse
            F = reshape(RF(gamma*B1(m)*rf.dt/2) * reshape(F,szRF),szF);

            % apply slice select gradient
            % And Spoil it.
            F(1,1,:,:,:,:,:,:,:,1) = F(1,1,:,:,:,:,:,:,:,1) .* exp( 1i * rf.G_ex.*pos.*gamma.*rf.dt);
            F(1,2,:,:,:,:,:,:,:,1) = F(1,2,:,:,:,:,:,:,:,1) .* exp(-1i * rf.G_ex.*pos.*gamma.*rf.dt);

            if p.Results.relax_during_pulse
                % relaxation during the pulse
                F = cat(2,E2.^(rf.dt),conj(E2).^(rf.dt), E1.^(rf.dt)) .* F;
                F(1,3,:,:,:,:,:,:,:,1) = F(1,3,:,:,:,:,:,:,:,1) + (1 - E1.^rf.dt);    
                time = time + rf.dt;
            end
            
             % apply tiny-hardpulse
            F = reshape(RF(gamma*B1(m)*rf.dt/2) * reshape(F,szRF),szF);
        end
        
        % apply re-winder gradient
        F(1,1,:,:,:,:,:,:,:,1) = F(1,1,:,:,:,:,:,:,:,1) .* exp( 1i * rf.A_post.*pos.*gamma);
        F(1,2,:,:,:,:,:,:,:,1) = F(1,2,:,:,:,:,:,:,:,1) .* exp(-1i * rf.A_post.*pos.*gamma);
    end
    
    dispdebug('AQ');
    % Acquire: We directly acquire after the readout
    % This means: we neglect T2* effects & relaxation until TE
    if aq(n)==1
        S(sum(aq(1:n)==1),:,:,:,:,:,:,:,:,ICfg) = F(1,1,:,:,:,:,:,:,:,1) .* exp(-1i .* cfg .* pos .* ksp);
    end
    
    dispdebug('RELAX');
    % Now Relax magnetization
    F = cat(2,E2.^(TR(n)-time),conj(E2).^(TR(n)-time), E1.^(TR(n)-time)).* F;
    F(1,3,:,:,:,:,:,:,:,1) = F(1,3,:,:,:,:,:,:,:,1) + (1 - E1.^(TR(n)-time));    
    
    dispdebug('SPOIL');
    % And Spoil it.
    F(1,1,:,:,:,:,:,:,:,1) = F(1,1,:,:,:,:,:,:,:,1) .* exp( 1i * ksp .* pos);
    F(1,2,:,:,:,:,:,:,:,1) = F(1,2,:,:,:,:,:,:,:,1) .* exp(-1i * ksp .* pos);
end

cfg = todim(cfg,10);

function dispdebug_internal(str)
    % prints a debug statement for rapid & simple profiling
    t=toc;
    fprintf('%s (Iteration: %i, dt=%f s)\n',str,n,t);
    tic;
end

end



