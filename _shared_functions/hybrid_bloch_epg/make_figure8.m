%% make_figure8 Signal magnitude and phase dependency on off-resonance and spoiling moment.
%
% Copyright (c) 2021 ETH Zurich, Christian Guenthner, Thomas Amthor, Mariya Doneva, Sebastian Kozerke

% Off-Resonance Dependency
echoes=-2:1;
figure
set(gcf,'Position',[100 100 1215 600])
cnt=1;

sp_fac        = linspace(-4,4,201);
ksp_plot      = todim(2*pi /slice_thickness * sp_fac,12);
msig = inf;
R2p = 0;
for ec=1:length(echoes)
for prep=1:2
   
    h=subtightplot(2,4,cnt+(prep-1)*4,.025,.1,.1);

    sig = squeeze(hybrid_bloch_epg(S(It,:,:,:,:,Id,:,:,:,:),rf.pos,kstates,E2(Id),ksp_plot,TR,TE,echoes(ec),omega,R2p));
    if ec==1
        msig = max(abs(sig(:)));
    end

    if prep==1
        imagesc(abs(sig).','XData',omega(:)/pi*TR,'YData',sp_fac(:));

        colormap(h,gray(512));
        caxis([0 msig]);
    else
        imagesc(angle(sig).','XData',omega(:)/pi*TR,'YData',sp_fac(:));

        colormap(h,colorcet('C9'));
        caxis([-pi pi]);
    end
    
    if ec==1
        if prep==1
            ylabel({'{\bf Signal Magnitude}','Spoiling Moment / (2\pi/\delta)'})
        else
            ylabel({'{\bf Signal Phase}','Spoiling Moment / (2\pi/\delta)'})
        end
    else
        yticklabels({}); 
    end
    if prep == 1
        xticklabels({});
    else
        xlabel('Off-Resonance (\pi/TR)');
    end
    
    axis square
       
    if prep==1
        title(sprintf('Spoiled SSFP - F(%i)',echoes(ec)));
    end
    
    a=gca;
    a.FontSize = 16;
    
    grid on
    if prep==2
        cnt=cnt+1;
    end
end
end

export_fig(sprintf('%s/figure8.png',outdir),'-m4');
