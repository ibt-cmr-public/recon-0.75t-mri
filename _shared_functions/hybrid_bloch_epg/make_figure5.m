%% make_figure5 Interpretation of bSSFP and GRE sequences in the k-vs-configuration state plot
%
% Copyright (c) 2021 ETH Zurich, Christian Guenthner, Thomas Amthor, Mariya Doneva, Sebastian Kozerke

%
figure
set(gcf,'Position',[100 100 600 600])
set(gcf,'Color',[1 1 1]);

echoes = todim(kstates,11);   
Nk = (size(S,10)-1)/2;
dk=linspace(-Nk,Nk,5001) * 2*pi/slice_thickness * 2;
mod_func_noshift = sinc(dz.*(dk)/2./pi).*exp(-1i.*dk.*pos);
q = -Nk:Nk;

imagesc(log10(abs(squeeze(sum(mod_func_noshift.*S(It,1,:,1,1,Id,1,1,1,:),3)))),'XData',flip(kstates(:)),'YData',flip(dk(:)));

hold on
plot(q,-2*pi/slice_thickness .* q,'linewidth',3,'color',[190 40 30]/255)       % 1x spoiled SSFP
plot(q,+2*pi/slice_thickness .* q,'linewidth',2,'color',[190 40 30]/255)       % -1x spoiled SSFP
plot(q,-2*pi/slice_thickness .* (q+1),'--','linewidth',2,'color',[239 192 78]/255) % PSIF
plot(q,-2*2*pi/slice_thickness .* q,'linewidth',2,'color',[190 40 30]/255)  % 2x spoiled SSFP
plot(q,-0*2*pi/slice_thickness .* q,'linewidth',3,'color',[130 190 30]/255)    % balanced SSFP
plot(q,-0*2*pi/slice_thickness .* q - 2*pi/slice_thickness/3,'--','linewidth',3,'color',[130 190 30]/255)    % phase encoding, bSSFP
plot(q,-1e4*2*pi/slice_thickness .* q,'linewidth',3,'color',[18 105 176]/255)    % pEPG
hold off

colormap(viridis(512));
caxis([-10 0]);
axis square
axis([-Nk Nk min(dk) max(dk)]);
%xticklabels({})
%yticklabels({})
set(gca,'FontSize',16)
ylabel('Spatial Fourier Transformation k'' (rad/m)')
xlabel('Configuration State k')

export_fig(sprintf('%s/figure5.png',outdir),'-m4');


%% Zoom in
echoes = todim(kstates,11);
figure
set(gcf,'Position',[100 100 400 400])
set(gcf,'Color',[1 1 1]);
   
Nk = (size(S,10)-1)/2;
dk=linspace(-Nk,Nk,5001) * 2*pi/slice_thickness * 2;
mod_func_noshift = sinc(dz.*(dk)/2./pi).*exp(-1i.*dk.*pos);
q = -Nk:Nk;

imagesc(log10(abs(squeeze(sum(mod_func_noshift.*S(It,1,:,1,1,Id,1,1,1,:),3)))),'XData',flip(kstates(:)),'YData',flip(dk(:)));

hold on
plot(q,-2*pi/slice_thickness .* q,'linewidth',3,'color',[190 40 30]/255)       % 1x spoiled SSFP
plot(q,+2*pi/slice_thickness .* q,'linewidth',2,'color',[190 40 30]/255)       % -1x spoiled SSFP
plot(q,-2*pi/slice_thickness .* (q+1),'--','linewidth',2,'color',[239 192 78]/255) % PSIF
plot(q,-2*2*pi/slice_thickness .* q,'linewidth',2,'color',[190 40 30]/255)  % 2x spoiled SSFP
plot(q,-0*2*pi/slice_thickness .* q,'linewidth',3,'color',[130 190 30]/255)    % balanced SSFP
plot(q,-0*2*pi/slice_thickness .* q - 2*pi/slice_thickness / 3,'--','linewidth',3,'color',[130 190 30]/255)    % phase encoding, bSSFP
plot(q,-1e4*2*pi/slice_thickness .* q,'linewidth',3,'color',[18 105 176]/255)    % pEPG
hold off

colormap(viridis(512));
caxis([-10 0]);
axis square
state_range = 3;
axis([-1 1 -2*pi/slice_thickness 2*pi/slice_thickness]*state_range);
%xticklabels({})
%yticklabels({})
%set(gca,'Visible','off')
set(gca,'FontSize',16)
%ylabel('Spatial Fourier Transformation k'' (rad/m)')
%xlabel('Configuration State k')

export_fig(sprintf('%s/figure5_zoomin.png',outdir),'-m4');