function [S,cfg]=srepg(varargin)
%%srepg(RFmat,TR,E1,E2,...) Performs a spatially resolved EPG Simulation with 
% unit dephasing at the end of each TR
%
% Sequence: (RF - Readout - Relaxation - Spoiling)
%
% Copyright (c) 2021 ETH Zurich, Christian Guenthner, Thomas Amthor, Mariya Doneva, Sebastian Kozerke

p = inputParser;
p.KeepUnmatched = 1;
if nargin~=0
    % for default struct.
    p.addRequired('RFmat');
    p.addRequired('TR');
    p.addRequired('E1');
    p.addRequired('E2');
end
p.addParameter('Nepg',[]);
p.addParameter('acquisition_on',[]);
p.addParameter('export_configuration',[]);
p.addParameter('export_longitudinal',0);
p.addParameter('gamma',42577.469*2*pi/1000); % gamma = 2*pi*42.577469 MHz/T
p.addParameter('debug',0);
p.parse(varargin{:});

% enable/disable debugging/profiling statements
dispdebug = @dispdebug_internal;
if p.Results.debug==0; dispdebug=@(x)0; end

% create a default struct we can fill (no arguments provided).
if nargin==0
    S = p.Results;    
    if nargout==0
        fprintf('Call epgmat(RFmat,RFphase,TR,E1,E2,...). Where ... are parameters with defaults returned by this call.\n');
    end
    return;
end

% copy to local variables
RFmat = p.Results.RFmat;
TR = p.Results.TR;
E1 = p.Results.E1;
E2 = p.Results.E2;
Nepg = p.Results.Nepg;
aq = p.Results.acquisition_on;
cfg = p.Results.export_configuration;
gamma = p.Results.gamma;               % rad MHz/T

N = size(RFmat,4);
if length(TR)==1
    TR = ones(1,1,1,N) * TR;
end

% defaults based on other inputs
if isempty(Nepg); Nepg=floor(N/2); end
if isempty(aq); aq=ones(1,1,1,N); end
if isempty(cfg); cfg=-(Nepg-1):(Nepg-1); end

assert(length(aq)==N,'Length acquisition_on must equal RFmat[4]');
assert(all(cfg == sort(cfg)),'Configuration state output vector must be in ascending order');

% size of study parameters
szstudy = combinedsize(10,E1,E2);
assert(all(szstudy(2:5)==1),'Some dimensions are wrong for input paramters (dim 2:5 must be singleton)');
assert(all(szstudy(10:end)==1),'Maximum non-singleton dimension 9');

assert(N==size(TR,4),'Length of RFmat[4] must equal TR');
szr = size(RFmat);

% make RFmat a sparse, blockdiagonal matrix
k = 1:prod(szr(1:3));
j = floor((k-1) / 3)+1;
i = mod(k-1,3)+1 + floor((k-1) / (szr(1)*szr(2))) * szr(2);
for r=1:size(RFmat,4)
    RFsparse{r} = sparse(i,j,double(reshape(RFmat(:,:,:,r),[],1))); 
end

RFmat = RFsparse;
Nz = szr(3);
clear i j k szr RFsparse


% FIXED VECTOR Dimensions:
% 1: 3 Vector Dimensions
% 2: 3 Vector Dimensions
% 3: Spatial Slice Profile
% 4: B1+ Transmit Field
% 5: RF Spoiling
% 6: T1/T2 Dictionary
% 7: Reserved
% 8: Reserved
% 9: Reserved
% 10: EPG States
szF = [1,3,Nz,1,1,szstudy(6:9),Nepg];
F = zeros(szF);

% define reshape sizes for the different matrix operations
szRF = [prod(szF(1:3)) prod(szF(4:end))];

% Equilibrium Magnetization
F(1,3,:,:,:,:,:,:,:,1)=1; %< equilibrium magnetization (Z(k=0)=1)

% define indices for configuration state export and their sorting
IP = cfg(cfg>=0)+1;
IN = abs(cfg(cfg<0))+1;
INr = 1:length(IN);
IPr = (length(IN)+1):length(cfg);

% how many states do we export?
Ncfgexport = length(cfg);

% do we export longitudinal magnetization?
if p.Results.export_longitudinal
    cfgMz = unique(abs(cfg));
    % increment number of exported states
    Ncfgexport = Ncfgexport + length(cfgMz);
    IMz = cfgMz+1;
    IMzr = IPr(end)+(1:length(IMz));
end

% signal output vector
szS = [sum(aq==1) 1 Nz,1,1,szstudy(6:9),Ncfgexport];
S = zeros(szS);


% simulation loop
tic;
for n=1:N    
    % apply RF pulse
    F = reshape(RFmat{n} * reshape(F,szRF),szF);
    dispdebug('RF');
    
    % Acquire: We directly acquire after the readout
    % This means: we neglect T2* effects / relaxation until readout
    if aq(n)==1
        
        S(sum(aq(1:n)==1),:,:,:,:,:,:,:,:,IPr) = F(1,1,:,:,:,:,:,:,:,IP);
        S(sum(aq(1:n)==1),:,:,:,:,:,:,:,:,INr) = conj(F(1,2,:,:,:,:,:,:,:,IN));
        if p.Results.export_longitudinal
            S(sum(aq(1:n)==1),:,:,:,:,:,:,:,:,IMzr) = F(1,3,:,:,:,:,:,:,:,IMz);
        end
    end
    dispdebug('AQ');
    
    % Now Relax magnetization
    F = cat(2,E2.^TR(n),conj(E2).^TR(n), E1.^TR(n)) .* F;
    % T1 recovery:
    F(1,3,:,:,:,:,:,:,:,1) = F(1,3,:,:,:,:,:,:,:,1) + (1 - E1.^TR(n));    
    dispdebug('RELAX');
    
    % And Spoil it.
    F(1,1,:,:,:,:,:,:,:,2:end)   =      F(1,1,:,:,:,:,:,:,:,1:end-1);
    F(1,2,:,:,:,:,:,:,:,1:end-1) =      F(1,2,:,:,:,:,:,:,:,2:end);
    F(1,2,:,:,:,:,:,:,:,end)     = 0;
    F(1,1,:,:,:,:,:,:,:,1)       = conj(F(1,2,:,:,:,:,:,:,:,1)); 
    dispdebug('SPOIL');
end

cfg = todim(cfg,10);

function dispdebug_internal(str)
    % prints a debug statement for rapid & simple profiling
    t=toc;
    fprintf('%s (Iteration: %i, dt=%f s)\n',str,n,t);
    tic;
end
end



