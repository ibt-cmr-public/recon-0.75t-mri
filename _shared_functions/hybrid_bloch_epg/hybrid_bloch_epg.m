function S=hybrid_bloch_epg(S,pos,kstates,E2,dk,TR,TE,echoes,omega,R2p, I0)
%% hybrid_bloch_epg(S,pos,kstates,TR,TE,echoes,omega,R2p) evaluates the
%Hybrid Bloch-EPG model for a state graph assuming constant TR and TE.
%
% The Hybrid Bloch-EPG model allows to extract the slice-profile corrected
% signal for both balanced and gradient-spoiled sequences as well as for a
% multitude of different approximations.
% 
% for Balanced SSFP: set dk = 0 and echoes = [];
% > Slice-Profile Corrected Bloch (via EPG):
%   The default behavior is Slice-Profile Corrected Bloch.
%   (I0=[] or omitted)
%
% > No Slice-Profile Correction (via EPG):
%   set I0 to the center position of the slice, e.g. by
%      I0=find(abs(pos)<eps)
%   Only the central position of the slice will be evaluated. Since by
%   definition, the flip angle is exactly reached there and no phase can be
%   accumulated through spoiling or imbalanced slice pre- and rewinder
%   gradients, the phase graph at the central position is equivalent to the
%   Fourier Series representation of the bSSFP profile spectrum (See Ref. Leupold)
%
% for spoiled GRE / unbalanced SSFP: set dk ~= 0
% > Slice-Profile Corrected EPG:
%   This is the default option for dk~=0. Here, we take configuration state
%   mixing into account based on the width of the discretization bins.
%
%   Fully evaluated and with special care in setting up the pulse 
%   discretization, this becomes equivalent to the slice-selective EPG
%   (SS-EPG, See Ref. Ostenson)
%
% > Spatially-Resolved EPG:
%   set dk=inf.
%   Now, state mixing is disabled and only the requested echo orders are
%   returned. This can be compared to the case of strong spoiling gradients
%   (>>2pi/slicethickness, e.g. x20) and is equivalent with the
%   spatially-resolved EPG (See Ref. Malik)
%
% > No Slice-Profile Correction:
%   set I0 to the center position of the slice, e.g. by
%      I0=find(abs(pos)<eps)
%   This method will fail unless dk = inf here, since the Slice-Profile
%   Corrected version requires a slice profile. Again, only the central
%   position is evaluated, which is equivalent to a EPG simulation without
%   slice profile effects (See Ref. Weigel).
%
% I0 can additionally be used to study the influence of the resolution of
% the slice profile (I0 = 1:2:end to e.g. coarsen the resultion by a factor
% 2) or to change the field-of-view of the simulation (e.g
% I0=30:length(pos)-29).
%
% Inputs: 
% S        spatially-resolved extended state graph
% pos      position array within the slice profile [m]
% kstates  dephasing order for the 10th dimension of S
% E2       exp(-1/T2) for transverse signal decay to echo time point
% dk       spoiling moment [rad/m]
% TR       repetition time [ms]
% TE       echo time [ms]
% echoes   refocussed echoes
% omega    offresonance [rad/ms]
% R2p      R2 prime decay (assume Lorentzian distribution of microsopic
%          offresonances).
% (I0)     optional index array to evaluate a subset of the slice profile
%          or a single position, e.g. the profile center
%          if not specified, use all available positions.
%
% Output: Signal Matrix
%
% References: see references.txt
%
% Copyright (c) 2021 ETH Zurich, Christian Guenthner, Thomas Amthor, Mariya Doneva, Sebastian Kozerke

if nargin<11 || isempty(I0); I0=1:size(S,3); end
assert(size(kstates,10) == size(S,10),'Dephasing order must be in 10th dimension. Use todim(..,10).');

% select the subset of indices
pos = todim(pos(I0),3);
S   = S(:,:,I0,:,:,:,:,:,:,:,:,:,:,:,:,:,:,:,:,:,:,:,:,:);

% assume constant distance between discretization points
if length(I0)==1
    % this will result in failure of the hybrid model since we need a slice
    % profile for it. (results in nan's for the output signal)
    dz = inf;
else
    dz = pos(2)-pos(1);
end

% setup modulation functions.

% The modulation functions are central to the hybrid-bloch-epg concept: 
% In the hybrid bloch-epg simulation, we have two summations, one over the
% slice profile and one over configuration states. Thus we define two
% functions, which allow to perform *first* the summation over the slice
% profile and then over the states, which reduces memory load.
% 
% (1) Spatial Modulation Function: 
% Effectively accounts for the discretization of the slice profile into 
% constant sized bins (sinc function) and its incomplete spoiling and 
% applies the Fourier shift theorem to obtain the correct phase at each 
% position in the slice. With this we are effectively performing a Fourier 
% transformation of the slice profile. The term (echoes-kstates) in the 
% Fourier shift term, is reminiscent of shifting the Fourier transformation
% of the slice profile in the frequency domain. This makes the Ansatz
% comparable to the Slice-Selective EPG.
%
if dk==0 
    assert(isempty(echoes),'Echos must be empty for bSSFP');
    % Special case bSSFP: In the case of balanced SSFP, dk=0. Thus,
    % mod_func_spatial becomes 1, which is easily verified. Let's hardcode this
    % special case for performance reasons. 
    mod_func_spatial = 1;
elseif dk==inf
    % Special case SR-EPG: If dk==inf, the hybrid-bloch-epg will converge
    % to the SR-EPG, thus no state mixing occurs (See Ref. Malik).
    mod_func_spatial = kstates == echoes;
else
    mod_func_spatial = sinc(dz.*dk.*(echoes-kstates)/2./pi).*exp(-1i.*dk.*pos(I0).*(echoes-kstates));
end
%
% (2) Configuration State Modulation Function: After summation over the slice
% profile, we combine the configurations taking offresonance (omega),
% R2'-decay (lorentzian, microscopic frequency distribution) and T2 decay
% to the echo time-point into account. This is equivallent for both FISP
% and bSSFP sequences (See Ref. Leupold).
mod_func_config = exp(1i.*omega.*(kstates.*TR + TE) - abs(kstates.*TR + TE).*R2p).*E2.^(TE);

% run our hybrid signal model
S = sum(sum( S.* mod_func_spatial, 3) .* mod_func_config ,10);

end