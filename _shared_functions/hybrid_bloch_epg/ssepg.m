function [S,cfg]=ssepg(varargin)
%ssepg(FA,rf,TR,E1,E2,...) Performs a slice-selective EPG Simulation with 
% unit dephasing at the end of each TR. Here, instead of splitting the
% spatial domain to separately simulate spoiling and RF pulse effects as in 
% the hybrid Bloch-EPG, everything is performed in the Fourier domain.
%
% Sequence: (RF - Readout - Relaxation - Spoiling)
%
% Reimplementation of: 
%  Ostenson, J., Smith, D. S., Does, M. D. & Damon, B. M.
%  Slice-selective extended phase graphs in gradient-crushed, transient-state free precession sequences: An application to MR fingerprinting. 
%  Magn. Reson. Med. mrm.28381 (2020). 
%  doi:10.1002/mrm.28381 
%
% Copyright (c) 2021 ETH Zurich, Christian Guenthner, Thomas Amthor, Mariya Doneva, Sebastian Kozerke

p = inputParser;
p.KeepUnmatched = 1;
if nargin~=0
    % for default struct.
    p.addRequired('FA');
    p.addRequired('rf');
    p.addRequired('TR');
    p.addRequired('E1');
    p.addRequired('E2');
end
p.addParameter('Nepg',[]);
p.addParameter('Nepg_max',128*20); % maximum states after which phase graph stops to grow. set to inf to grow indefinitely
p.addParameter('ksp',0);
p.addParameter('acquisition_on',[]);
p.addParameter('export_configuration',[]);
p.addParameter('export_longitudinal',0);
p.addParameter('gamma',42577.469*2*pi/1000); % gamma = 2*pi*42.577469 MHz/T
p.addParameter('debug',0);
p.addParameter('relax_during_pulse',1);
p.addParameter('export_full_phasegraph_frame',[]);
p.parse(varargin{:});

% enable/disable debugging/profiling statements
dispdebug = @dispdebug_internal;
if p.Results.debug==0; dispdebug=@(x,varargin)0; end

% create a default struct we can fill (no arguments provided).
if nargin==0
    S = p.Results;    
    if nargout==0
        fprintf('Call epgmat(RFmat,RFphase,TR,E1,E2,...). Where ... are parameters with defaults returned by this call.\n');
    end
    return;
end

% copy to local variables
FA = p.Results.FA;
rf = p.Results.rf;
TR = p.Results.TR;
E1 = p.Results.E1;
E2 = p.Results.E2;
ksp = p.Results.ksp; % spoiling moment
Nepg = p.Results.Nepg;
aq = p.Results.acquisition_on;
cfg = p.Results.export_configuration;
gamma = p.Results.gamma;               % rad MHz/T

N = length(FA);
if length(TR)==1
    TR = ones(1,1,1,N) * TR;
end

% defaults based on other inputs
if isempty(Nepg); Nepg=floor(N/2); end
if isempty(aq); aq=ones(1,1,1,N); end
if isempty(cfg); cfg=-(Nepg-1):(Nepg-1); end

assert(length(aq)==N,'Length acquisition_on must equal RFmat[4]');
assert(all(cfg == sort(cfg)),'Configuration state output vector must be in ascending order');

% size of study parameters
szstudy = combinedsize(10,E1,E2);
assert(all(szstudy(2:5)==1),'Some dimensions are wrong for input paramters (dim 2:5 must be singleton)');
assert(all(szstudy(10:end)==1),'Maximum non-singleton dimension 9');



% FIXED VECTOR Dimensions:
% 1: 3 Vector Dimensions
% 2: 3 Vector Dimensions
% 3: Spatial Slice Profile
% 4: B1+ Transmit Field
% 5: RF Spoiling
% 6: T1/T2 Dictionary
% 7: Reserved
% 8: Reserved
% 9: Reserved
% 10: EPG States
szF = [1,3,1,1,1,szstudy(6:9),1];
F = zeros(szF);

% define reshape sizes for the different matrix operations
szRF = [prod(szF(1:3)) prod(szF(4:end))];

% Equilibrium Magnetization
F(1,3,:,:,:,:,:,:,:,1)=1; %< equilibrium magnetization (Z(k=0)=1)

% define indices for configuration state export and their sorting
IP = cfg(cfg>=0)+1;
IN = abs(cfg(cfg<0))+1;
INr = 1:length(IN);
IPr = (length(IN)+1):length(cfg);

% how many states do we export?
Ncfgexport = length(cfg);

% do we export longitudinal magnetization?
if p.Results.export_longitudinal
    cfgMz = unique(abs(cfg));
    % increment number of exported states
    Ncfgexport = Ncfgexport + length(cfgMz);
    IMz = cfgMz+1;
    IMzr = IPr(end)+(1:length(IMz));
end

% signal output vector
szS = [sum(aq==1) 1 1,1,1,szstudy(6:9),Ncfgexport];
S = zeros(szS);

% time resolution of the RF pulse is the smallest k-space increment. So it
% determines the state spacing. If we want a finer resolved SS-EPG, we need
% to increase the temporal resolution of the RF pulse
dk = gamma * rf.G_ex * rf.dt;
assert(dk > 0,'Slice Select Gradient should be positive. Please invert all gradients accordingly.');
assert(ksp> 0,'Currently, only positive spoiling moments are allowed.');

% determines number of states for the spoiling gradient
Nspoil = round(ksp / dk);

% simulation loop
tic;
for n=1:N    
    time = 0;
    % apply RF pulse
    dispdebug('RF');
    
    % prepare AM shape & apply RF pulse
    if abs(abs(FA(n))-pi) < 10*eps
        % adiabatic 180° pulse
        F = reshape(RF(pi) * reshape(F,szRF),szF);
    else
        %let's use sinc pulses for everything else
        % apply pre-winder gradient
        F = spoil(F, round(rf.A_pre / dk * gamma));
        
        % define shape and normalize to flip angle
        B1 = rf.shape;
        fapulse = gamma .* trapz(B1) .* rf.dt;
        B1 = B1 ./ fapulse .* FA(n);

        % iteratve over RF steps
        for m=1:length(B1)
            % apply tiny-hardpulse
            szF = size(F);
            F = reshape(RF(gamma*B1(m)*rf.dt/2) * reshape(F,3,[]),szF);

            % apply slice select gradient
            % And Spoil it.
            F = spoil(F, round(gamma * rf.G_ex * rf.dt / dk));

            if p.Results.relax_during_pulse
                % relaxation during the pulse
                F = cat(2,E2.^(rf.dt),conj(E2).^(rf.dt), E1.^(rf.dt)) .* F;
                F(1,3,:,:,:,:,:,:,:,1) = F(1,3,:,:,:,:,:,:,:,1) + (1 - E1.^rf.dt);    
                time = time + rf.dt;
            end
            
            % apply tiny-hardpulse
            szF = size(F);
            F = reshape(RF(gamma*B1(m)*rf.dt/2) * reshape(F,3,[]),szF);
        end
        
        % apply rewinder gradient (approximate)
        F = spoil(F, round(rf.A_post / dk * gamma));
    end
    
    
    
    dispdebug('AQ');
    % Acquire: We directly acquire after the readout
    % This means: we neglect T2* effects / relaxation until readout
    if aq(n)==1 
        if ~isempty(p.Results.export_full_phasegraph_frame) && p.Results.export_full_phasegraph_frame == sum(aq(1:n)==1)
            S = cat(10,flip(conj(F(1,2,:,:,:,:,:,:,:,2:end)),10),F(1,1,:,:,:,:,:,:,:,1:end));
            return;
        end       
        % calculate approximate configuration state centers and reject all,
        % which have not yet been populated
        IPdk = (IP-1).*Nspoil+1; IPdk(IPdk>size(F,10))=[];
        INdk = (IN-1).*Nspoil+1; INdk(INdk>size(F,10))=[];
        
        S(sum(aq(1:n)==1),:,:,:,:,:,:,:,:,IPr(1:length(IPdk))) = F(1,1,:,:,:,:,:,:,:,IPdk);
        S(sum(aq(1:n)==1),:,:,:,:,:,:,:,:,INr(1:length(INdk))) = conj(F(1,2,:,:,:,:,:,:,:,INdk));
        if p.Results.export_longitudinal
            IMzdk = IMz.*round(ksp/dk); IMzdk(IMzdk>size(F,10))=[];
            S(sum(aq(1:n)==1),:,:,:,:,:,:,:,:,IMzr(1:length(IMzdk))) = F(1,3,:,:,:,:,:,:,:,IMzdk);
        end
    end
    
    dispdebug('RELAX');
    % Now Relax magnetization
    F = cat(2,E2.^(TR(n)-time),conj(E2).^(TR(n)-time), E1.^(TR(n)-time)) .* F;
    F(1,3,:,:,:,:,:,:,:,1) = F(1,3,:,:,:,:,:,:,:,1) + (1 - E1.^(TR(n)-time));    
    
    % And Spoil it.
    F=spoil(F,Nspoil);
end

cfg = todim(cfg,10);

function dispdebug_internal(str,varargin)
    % prints a debug statement for rapid & simple profiling
    t=toc;
    str = sprintf(str,varargin{:});
    fprintf('%s (Iteration: %i, dt=%f s)\n',str,n,t);
    tic;
end
 
    function F=spoil(F,N)
        % And Spoil it.
        % Grow phase graph with spoiling        
        if N>=0
            for ii=1:abs(N)
                F(1,2,:,:,:,:,:,:,:,1:end-1) = F(1,2,:,:,:,:,:,:,:,2:end);
                F(1,2,:,:,:,:,:,:,:,end) = 0;
                
                if(size(F,10)<p.Results.Nepg_max)
                    % grow
                    F(:,:,:,:,:,:,:,:,:,end+1) = 0;
                end        
                F(1,1,:,:,:,:,:,:,:,2:end) = F(1,1,:,:,:,:,:,:,:,1:end-1);                
                F(1,1,:,:,:,:,:,:,:,1) = conj(F(1,2,:,:,:,:,:,:,:,1)); 
            end
        else
            %reverse direction
            for ii=1:abs(N)
                F(1,1,:,:,:,:,:,:,:,1:end-1) = F(1,1,:,:,:,:,:,:,:,2:end);
                F(1,1,:,:,:,:,:,:,:,end) = 0;
                if(size(F,10)<p.Results.Nepg_max)
                    % grow
                    F(:,:,:,:,:,:,:,:,:,end+1) = 0;
                end
                F(1,2,:,:,:,:,:,:,:,2:end) = F(1,2,:,:,:,:,:,:,:,1:end-1);                
                F(1,2,:,:,:,:,:,:,:,1) = conj(F(1,1,:,:,:,:,:,:,:,1));
            end
        end
        dispdebug('SPOIL N=%i; #states=%i',N,size(F,10));
    end

end



