function [Mout, pos] = RF_matrix(rf,FA,varargin)
%RF_matrix Calculates the full RF matrix for an arbitrary
% amplitude-modulated RF pulse by propagating transverse dephasing,
% transverse rephasing and longitudinal magnetization through a discretized
% RF pulse simulation.
%
% Returns a 3x3xNsp matrix corresponding to the RF rotation matrix in each
% of the Nsp positions.
%
% Copyright (c) 2021 ETH Zurich, Christian Guenthner, Thomas Amthor, Mariya Doneva, Sebastian Kozerke

p=inputParser;
p.addOptional('fov',rf.slice_thickness*4,@isnumeric); % field of view to simulate [m]
p.addOptional('Nsp',401,@isnumeric); % number of isochromats to simulate 
p.addOptional('omega',0,@isnumeric); % offresonance during the pulse [rad/ms]
p.addOptional('T1',[]);              % relaxation during the pulse (T1, ms)
p.addOptional('T2',[]);              % relaxation during the pulse (T2, ms)
p.addOptional('A_pre' , rf.A_pre, @isnumeric); % prephaser area (overwrites RF settings)
p.addOptional('A_post', rf.A_post,@isnumeric); % rewinder area  (overwrites RF settings)
p.parse(varargin{:});

%copy parameters
fov = p.Results.fov;
Nsp = p.Results.Nsp;
omega = p.Results.omega;
T1 = p.Results.T1;
T2 = p.Results.T2;
Apre = p.Results.A_pre;
Apost = p.Results.A_post;

% set up relaxation
R1=[];
R2=0;
if ~isempty(T1); R1=1./T1; end
if ~isempty(T2); R2=1./T2; end

% spatial resolution
dz = fov/Nsp;

% setup position array
pos = reshape((1:Nsp).*dz,1,[]);
pos = pos - mean(pos);

% Magnetization in/output
Min = eye(3).*ones(1,1,Nsp);
Mout = zeros(size(Min));
% iterate over the three magnetization inputs (transverse
% dephasing, transverse refocussing, longitudinal)
for n=1:size(Min,2)
    % perform a Bloch simulation for the given magnetization input
    Mout(:,n,:) = hardpulse(rf.shape, rf.dt, pos, rf.G_ex.*ones(1,length(rf.shape)), FA, Apre, Apost, reshape(Min(:,n,:),3,[]),R1,R2+1i*omega);
end
% store the position array
pos = reshape(pos,1,1,[]);
end

