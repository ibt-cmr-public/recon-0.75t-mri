%% make_figure2 plots the flip angle train
%
% Copyright (c) 2021 ETH Zurich, Christian Guenthner, Thomas Amthor, Mariya Doneva, Sebastian Kozerke

%% Plots the FA Sequence
figure;
set(gcf,'Position',[100 100 600 200]);
set(gcf,'Color',[1 1 1]);

% plot the flip angle sequence without the initial 180° pulse
plot(1:(length(FA)-1),abs(FA(2:end)/pi*180),'LineWidth',2);
colors = get(gca,'ColorOrder');
xlabel('Time-Point');
ylabel(['Flip Angle (' char(176) ')'])
grid on
box on
set(gca,'FontSize',12);
xlim([0 inf]);
ylim([0 90]);

% export
export_fig(sprintf('%s/figure1_sequence.png',outdir),'-m4');

% export again in a different size for insets
set(gcf,'Position',[100 100 300 300]);
axis square

export_fig(sprintf('%s/figure1_sequence_inset.png',outdir),'-m4');