%% make_figure10 Comparison of signal-time behavior for Bloch vs. Hybrid Bloch-EPG simulations
%
% Copyright (c) 2021 ETH Zurich, Christian Guenthner, Thomas Amthor, Mariya Doneva, Sebastian Kozerke

echoes = 0;
di = [1 10 20];
clear Shbepg Sbloch Sbloch2
for dii = 1:length(di);
    I0 = find(pos==0);
    I0 = unique([(I0:-di(dii):1), (I0:di(dii):length(pos))]);
    dk = 2*pi/4.8183/1e-3 * 4;
    % setup modulation functions.
    % (a) with the FT of the interpolation kernel (sinc)
    mod_func_spatial = sinc(dz*di(dii).*dk.*(echoes-kstates)/2./pi).*exp(-1i.*dk.*pos(I0).*(echoes-kstates));
    % (b) without the FT of the interpolation kernel
    mod_func_spatial0 = exp(-1i.*dk.*pos(I0).*(echoes-kstates));

    % Let's disregard modulation due to offresonance (omega = 0) and R2'
    mod_func_config = 1;
    
    % run our hybrid signal model
    Shbepg(:,dii) = di(dii)*sum(sum( S(:,:,I0,:,:,Id,:,:,:,:).* mod_func_spatial, 3) .* mod_func_config ,10);
    Snodamping(:,dii) = di(dii)*sum(sum( S(:,:,I0,:,:,Id,:,:,:,:).* mod_func_spatial0, 3) .* mod_func_config ,10);
    
    % and do a bloch simulation
    Sbloch(:,dii) = di(dii)*sum(bloch(FA, rf, TR,E1(Id),E2(Id),'acquisition_on',aqon,'pos',rf.pos(I0),'ksp',dk,'relax_during_pulse',0),3);
end

%% And let's create the comparison plot.
pic = char(hex2dec('03C0'));
figure
a = gca;
colors = a.ColorOrder;
set(gcf,'Position',[100 100 1600 500]);
set(gcf,'Color',[1 1 1])

subtightplot(1,size(Shbepg,2)+1,1,.1,.2,.1)
a = gca;
a.YColor = [0 0 0];
a.ColorOrder = gray(3);
a.ColorOrderIndex=1;
legstr = {};
style = {'-','x','d'};
for n=1:length(di)
    I0 = find(pos==0);
    I0 = unique([(I0:-di(n):1), (I0:di(n):length(pos))]);
    plot(squeeze(pos(I0) * 1000), squeeze(asin(abs(rf_mat(1,3,I0,90)))/pi*180),style{n},'LineWidth', 2); hold on
    legstr{n} = sprintf('Nsp = %i',length(I0));
end
hold off
ylabel('Effective Flip Angle (°)');
xlabel('Position (mm)')
xlim([0 inf]);
title('Slice Profile Discretizations')
grid on
set(gca,'FontSize',12);       
legend(legstr);
axis square
%
for n=1:size(Shbepg,2)    
    subtightplot(1,size(Shbepg,2)+1,(n-1)*1+2,.05,.1,.1)
    sel = @(x,a,b)squeeze(x(a,b,:,n));
        
    a = gca;
    a.YColor = [0 0 0];
    plot(abs(Shbepg(:,1)),'k-','LineWidth', 4); hold on
    plot(abs(Shbepg(:,n))-0*abs(Shbepg(:,1)),'-','LineWidth', 2, 'Color',[1 1 1]*.7); hold on
    plot(abs(Snodamping(:,n))-0*abs(Shbepg(:,1)),':','LineWidth', 2); hold on    
    plot(abs(Sbloch(:,n))-0*abs(Shbepg(:,1)),'--','LineWidth', 2); hold off
    ylabel('Signal Magnitude (a.u.)');
    xlabel('Time-Point')

    grid on
    set(gca,'FontSize',16);        
        
    if n==size(Shbepg,2)
        legend('Reference','Hybrid Bloch-EPG','Hybrid Bloch-EPG w/o Damping','Bloch Simulation')    
        legend('Location','SouthEast');
    end
    title(legstr{n})
end

export_fig(sprintf('%s/figure10.png',outdir),'-m4')
