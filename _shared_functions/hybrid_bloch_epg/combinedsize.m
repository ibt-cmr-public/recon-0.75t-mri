function sz=combinedsize(dim,varargin)
%combinedsize(dim,a,b,...) returns a size array of at least length dim with
% the combined size of all inputed data arrays
%
% Copyright (c) 2021 ETH Zurich, Christian Guenthner, Thomas Amthor, Mariya Doneva, Sebastian Kozerke

    sz = ones(1,dim);
    for m=1:length(varargin)
        szt = size(varargin{m});
        % extend current sz vector to match size of current argument
        sz(end+1:length(szt))=1;
        % find all non singleton dimensions (~=1)
        I = find(szt>1);
        % ensure that these are compliant
        % 1) either sz(I) == 1 -> in which case we take szt(I)
        % 2) or sz(I) == szt(I)
        assert( all((sz(I) == 1) | (sz(I) == szt(I))) ,'Inputs do not agree in size');
        sz(I) = szt(I);
    end
end