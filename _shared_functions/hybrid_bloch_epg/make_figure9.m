%% make_figure9 Off-resonance signal variation vs time against spoiling moment
%
% Copyright (c) 2021 ETH Zurich, Christian Guenthner, Thomas Amthor, Mariya Doneva, Sebastian Kozerke

configuration = {-2 -1 0 1};
sp_fac = linspace(-8,8,601);
ksp_plot      = todim(2*pi /slice_thickness * sp_fac,12);
R2p           = 0;

colors = get(gca,'ColorOrder');

if exist([outdir 'figure9_input.mat'],'file')~=2
    fprintf('Creating the signal response will take a while. Grab a coffee and come back later ...');tic
    Splot = [];
    for n=[1 2 3 4]
    for It=1:size(S,1)    
        Splot(:,:,It,n) = squeeze(hybrid_bloch_epg(S(It,:,:,:,:,Id,:,:,:,:),rf.pos,kstates,E2(Id),ksp_plot,TR,TE,configuration{n},omega,R2p));
    end
    end
    
    save([outdir 'figure9_input.mat'],'Splot')
    fprintf(' done and saved. Elapsed Time: %i Minutes, %i Seconds\n',floor(round(toc())/60),mod(round(toc()),60));
else
    load([outdir 'figure9_input.mat']);
end

%%
figure
set(gcf,'Position',[100 100 1600 500]);%./squeeze(Sfisp(:,Id,Ie))
set(gcf,'Color',[1 1 1]);

tmp = std(abs(Splot),[],1)./mean(mean(abs(Splot),1),3);
for n=1:4 
    h=subtightplot(1,4,n,.025,.1,.1);
    
    imagesc(log10(squeeze(tmp(:,:,:,n))),'XData',1:size(tmp,3),'YData',sp_fac)
    colormap(h,inferno(512));
    caxis([-2 log10(2)]);
    
    xlabel('Time-Point')
    if n==1
        ylabel({'{\bf Off-Resonance Magnitude Variation}','Spoiling Moment / (2\pi/\delta)'})
    else
        yticklabels([]);    
    end
    
    box on
    grid on
    a = gca;
    a.FontSize = 16;
    a.GridColor = [1 1 1];
    
    title(sprintf('Spoiled SSFP - F(%i)',configuration{n}))
    
    drawnow
end

clear colors str
hold off

export_fig(sprintf('%s/figure9.png',outdir),'-m4');