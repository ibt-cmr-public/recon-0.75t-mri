function c = todim(c,dim)
%% todim(c,dim) reshapes c so that c(:) is in dimension dim
%
% Copyright (c) 2021 ETH Zurich, Christian Guenthner, Thomas Amthor, Mariya Doneva, Sebastian Kozerke
    sz = ones(1,max(dim,2));
    sz(dim) = length(c(:));
    c = reshape(c,sz);
end