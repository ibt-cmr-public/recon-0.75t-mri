% This is the main script running all simulations and plot scripts in
% succession.
%      
% Copyright (c) 2021 ETH Zurich, Christian Guenthner, Thomas Amthor, Mariya Doneva, Sebastian Kozerke
%%
clear
close all

if 1
    % for Figure 3, we use a different phase graph to make the SS-EPG
    % results directly comparable to Bloch and SR-EPG / Hybrid-Bloch EPG.
    run init
    run make_figure3
    % We're done. Clear the workspace.
    clear
end
run init
 
% setup an exemplary MRF sequence
% Our test sequence consists of an inversion pulse followed by a linear
% flip angle ramp from 1 to 70 degrees, a plateau of 10-times 70 degrees
% constant, and a short ramp back to 50 degrees, totalling 101 RF pulses
% including the initial inversion.
FA = [180 1:70 ones(1,9)*70 70:-1:50]/180*pi;
% ... and add phase cycling:
FA = FA .* exp(1i * pi * (1:length(FA)));

% by definition the TR is held constant
TR = 15;

% determine which time points to output (skip readout directly after
% inversion pulse), leading to 100 acquired time-points
aqon    = ones(1,1,1,length(FA));
aqon(1) = 0;

% Setup Spatially-Resolved State Graph for Remaining figures
% For the remainder of the plots, we will reduce the simulation domain and
% the number of discretization positions (number isochromats) 
slice_thickness = 0.005;
field_of_view   = 0.015;
number_isochromats = 3001;

% setup T1/T2 for the graphics. We use two exemplary values
T1 = [800 4000];
T2 = [50 400];


% setup E1/E2 indipendent of TR. We'll add it in the EPG simulation
E1 = todim(exp(-1./T1),6);
E2 = todim(exp(-1./T2),6);
fprintf('Total Dictionary Size: %i\n',length(T1));

% setup rf pulse properties based on the maximal flip angle in the MRF
% train (calculates rf shape, slice select/prewinder&rephaser gradient)
rf = sinc_pulse(max(FA(2:end)),'slice_thickness',slice_thickness,'Nsp',number_isochromats ,'fov',field_of_view,'pulse_duration',3);


% calculate rf slice profile matrices for different flipangles assuming
% pure amplitude scaling of the sinc-pulse
fprintf('Precalculating RF Matrices ... ');tic
rf_mat = nan(3,3,number_isochromats,length(FA));
for n=1:length(FA)
    if n==1
        rf_mat(:,:,:,n) = RF(pi).*ones(1,1,number_isochromats);
    else
        [rf_mat(:,:,:,n),pos] = RF_matrix(rf,FA(n),'Nsp',number_isochromats ,'fov',field_of_view);
    end
end
% we've got equally spaced discretization points:
dz = pos(2)-pos(1);
fprintf('done. Elapsed Time: %.1f Seconds\n',toc);


% run SR-EPG simulation (Malik et al. Spatially resolved extended phase
% graphs: Modeling and design of multipulse sequences with parallel
% transmission. (2012)
%
% The SR-EPG runs a *indipendent* EPG simulation for each point in space

% run epg
[S,kstates] = srepg(rf_mat, TR,E1,E2,'acquisition_on',aqon,'Nepg',size(rf_mat,4));

% the output's dimensions of epgmat has the following meaning
% 1     time in the MRF train, signal directly after the RF pulse
% 2     (reserved, singleton dimension)
% 3     spatial position within the slice profile
% 4     (reserved, singleton dimension)
% 5     (reserved, singleton dimension)
% 6     T1/T2 Combinations
% 7     (reserved, singleton dimension)
% 8     (reserved, singleton dimension)
% 9     (reserved, singleton dimension)
% 10    configuration states (F+(k))
%
% kstates holds the dephasing order for the 10th dimension of S

% Setup offresonance array
omega = todim(linspace(-pi,pi,201),5)/TR;

% for the test case, we assume four-fold 2pi / slice thickness spoiling
ksp = 2 * pi / slice_thickness * 4; 

% echo Time
TE = 0;

%%
It = 90;
run make_figure2

Id = 2;  % ditionary entry to use
run make_figure4

%
Id = 1; % ditionary entry to use
run make_figure5
run make_figure6
run make_figure7
run make_figure8
run make_figure9

Id= 2;
run make_figure10
run make_figure11

