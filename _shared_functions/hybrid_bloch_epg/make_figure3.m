%% make_figure3 Creates the overview plot of how bloch, ssepg and srepg play together.
% This requires the calculation of a separate phase graph.
%
% Copyright (c) 2021 ETH Zurich, Christian Guenthner, Thomas Amthor, Mariya Doneva, Sebastian Kozerke


% setup our exemplary MRF sequence
% Our test sequence consists of an inversion pulse followed by a linear
% flip angle ramp from 1 to 70 degrees, a plateau of 10-times 70 degrees
% constant, and a short ramp back to 50 degrees, totalling 101 RF pulses
% including the initial inversion.
FA = [180 1:70 ones(1,9)*70 70:-1:50]/180*pi;
% ... and DO NOT add phase cycling (commented out for visualization purposes):
% FA = FA .* exp(1i * pi * (1:length(FA)));

% by definition the TR is held constant
TR = 15;

% determine which time points to output (skip readout directly after
% inversion pulse), leading to 100 acquired time-points
aqon    = ones(1,1,1,length(FA));
aqon(1) = 0;

% setup slice thickness and the simulation domain (field_of_view)
slice_thickness = 0.005; %[m]
field_of_view   = 0.0725; %[m]
% we setup the number of isochromats to match the SSEPG calculation's
% configuration states:
number_isochromats = 2*20*128-1; 

% setup T1/T2
% For the figure 3, we only use one T1/T2 value
T1 = [800];
T2 = [50];

% setup E1/E2 indipendent of TR. We'll add it in the EPG simulation
E1 = todim(exp(-1./T1),6);
E2 = todim(exp(-1./T2),6);

% spoiling moment
% for the test case, we assume four-fold spoiling
ksp = 2 * pi / slice_thickness * 4; 

% setup precession:
omega = 0; 

% setup rf pulse properties based on the maximal flip angle in the MRF
% train (calculates rf shape, slice select/prewinder&rephaser gradient)
% here, we choose 145 time points and so we'll exactly match the RF pulse resolution
% to the spoiling moment 
rf = sinc_pulse(max(FA(2:end)),'slice_thickness',slice_thickness,'Nsp',number_isochromats ,'fov',field_of_view,'n',145,'pulse_duration',3);

% in addition we round the determined pre- and re-winder areas to multiples
% of the state spacing of the ssEPG to obtain equivalent results with
% the more general Bloch and Hybrid-Bloch-EPG simulations.
rf.A_pre  = round(rf.A_pre ./ (rf.G_ex * rf.dt)) * (rf.G_ex * rf.dt);
rf.A_post = round(rf.A_post ./ (rf.G_ex * rf.dt)) * (rf.G_ex * rf.dt);

% perfrom the same simulation with a direct bloch simulation and neglect
% relaxation during the RF pulse
SSB = bloch(FA, rf, TR,E1.*ones(size(omega)),E2.*exp(1i * omega),'acquisition_on',aqon,'pos',rf.pos,'ksp',ksp,'relax_during_pulse',0);
% the bloch simulation will neglect TE, i.e. it returns the signal at TE =
% 0, which is theoretically at the focus point of the soft pulse.

%% Hybrid Bloch-EPG (neglects relaxation during the pulse!)
% calculate effective rf pulse matrices for different flipangles assuming
% pure amplitude scaling of the sinc-pulse:
fprintf('Precalculating RF Matrices ... ');tic
rf_mat = nan(3,3,number_isochromats,length(FA));
for n=1:length(FA)
    if n==1
        rf_mat(:,:,:,n) = RF(pi).*ones(1,1,number_isochromats);
    else
        [rf_mat(:,:,:,n),pos] = RF_matrix(rf,FA(n),'Nsp',number_isochromats ,'fov',field_of_view);
    end
end
fprintf('done. Elapsed Time: %.1f Seconds\n',toc);

% we've got equally spaced discretization points:
dz = pos(2)-pos(1);

% run spatially-resolved epg
[S,kstates] = srepg(rf_mat, TR,E1,E2,'acquisition_on',aqon,'Nepg',size(rf_mat,4));

% assume the echo time to be zero (of course this would mean we are
% acquiring during the RF pulse)
TE = 0;
% only refocus the 0th configuration
echoes = 0;

%% Set the time frames we want to use
Id = 1;
It = 15;    % time point to plot
Iomega = 1; % We only have one
R2p = 0;    % no R2' decay
Isel = -6:6;% show states -6...6

%% Extract one Frame from ssEPG Simulation to compare with Bloch and Hybrid-Bloch-EPG
% The ssepg code will stop once we reached the frame we want to look at
SSEPG_frame = ssepg(FA, rf, TR, E1(Id).*ones(size(omega)),E2(Id).*exp(1i * omega),'acquisition_on',aqon,'ksp',ksp,'Nepg',1,'export_full_phasegraph_frame',It,'Nepg_max',128*20,'relax_during_pulse',0);
SSEPG_frame = SSEPG_frame * (2*pi/(rf.gamma * rf.G_ex * rf.dt)) / rf.slice_thickness;

%% Figure 2
% SETUP modulation functions for the hybrid Bloch-EPG
% we do it manually here since we will sum over different dimensions later
% to obtain intermediate representations
mod_func_spatial = sinc(dz.*ksp.*(echoes-kstates)/2./pi).*exp(-1i.*ksp.*rf.pos.*(echoes-kstates));
mod_func_config = exp(1i.*omega(Iomega).*(kstates.*TR + TE) - abs(kstates.*TR + TE).*R2p).*E2(Id).^(TE);

% find the respective indices of the requested states (Isel) inside the
% kstates array
Istates = mod(find(todim(kstates,1) == Isel)-1,length(kstates))+1;

% use the viridis & magma colormaps if available
color = viridis(sum(Isel<=0));
color2 = magma(sum(Isel>=0)); 
color = cat(1,flip(color(1:end-1,:),1),color2);
clear color2

% setup figure
figure;
set(gcf,'Position',[100 100 1000 1000]);
set(gcf,'Color','none');
clf

% define a local subplot for easy plotting
subplot_local = @(x,y,z)subtightplot(x,y,z,.08,[.1 .1],[.1 .1]);

% plot the SR-EPG
subplot_local(4,4,[1 2 5 6]);
for n=1:length(Istates)
    col = color(n,:);
    offset = (n-7);%*.03;
    
    % normalize state graph signal to 75% of maximum for plot
    tmp = .75 * squeeze(S(It,1,:,1,1,Id,1,1,1,Istates(n)) ./ max(max(abs(S(It,1,:,1,1,Id,1,1,1,Istates)),[],3),[],10));
    
    plot(imag(tmp) + offset,pos(:)*1000,'LineWidth',2,'Color',col); hold on;
    plot(real(tmp) + offset,pos(:)*1000,':','LineWidth',2,'Color',col); hold on;
end
hold off
set(gca,'FontSize',16);
grid on
xticks(Isel);
yticks((-1:.25:1)*slice_thickness*1000);
set(gca,'XAxisLocation','top');
set(gca,'YAxisLocation','left');
set(gca,'TickLength',[0.01;0.025])
set(gca,'TickDir','both')
xlim([min(Isel)-0.5 max(Isel)+0.5]);
ylim([-1.1 1.1]*slice_thickness*1000);
ylabel('Position z (mm)');
xlabel('Configuration State k');
h=legend('Imaginary-Part','Real-Part');
set(h,'FontSize',10);

% Obtain the spatial profile of each configuration state
Sfisp_states = squeeze(S(It,:,:,:,:,Id,:,:,:,:).*mod_func_config .* mod_func_spatial);


% [BLOCH] Intermediate Representation of the spatial profiles for each
% configuration state
subplot_local(4,4,[3 7]);
for n=1:length(Istates)
    col = color(n,:);
    plot(imag(Sfisp_states(:,Istates(n))),pos(:)*1000,'LineWidth',2,'Color',col); hold on
    plot(real(Sfisp_states(:,n)),pos(:)*1000,':','LineWidth',2,'Color',col); hold on
end
hold off
    
ylim([-1.1 1.1]*slice_thickness*1000);
yticks((-1:.25:1)*slice_thickness*1000);
yticklabels({});
set(gca,'FontSize',16);
set(gca,'TickLength',[0.01;0.025])
set(gca,'TickDir','both')
xlabel('M^+(z)')
set(gca,'XAxisLocation','top');
grid on

% [BLOCH] Superimpose the states tocompare the spatial profile to the result 
% of the Bloch simulation

% Obtain the spatial profile of the transverse magnetization by summing all states
Sfisp_sp = squeeze(sum(S(It,:,:,:,:,Id,:,:,:,:).*mod_func_config .* mod_func_spatial,10));
subplot_local(4,4,[4 8]);
plot(imag(Sfisp_sp),pos(:)*1000,'k','LineWidth',2); hold on
plot(imag(squeeze(SSB(It,:,:,:,:,Id,Iomega))),pos(:)*1000,'b','LineWidth',1); hold on
plot(real(Sfisp_sp),pos(:)*1000,'k:','LineWidth',2); hold on
plot(real(squeeze(SSB(It,:,:,:,:,Id,Iomega))),pos(:)*1000,'b','LineWidth',1); hold on
hold off

ylim([-1.1 1.1]*slice_thickness*1000);
yticks((-1:.25:1)*slice_thickness*1000);
set(gca,'FontSize',16);
set(gca,'TickLength',[0.01;0.025])
set(gca,'TickDir','both')
ylabel('Position z (mm)')
set(gca,'YAxisLocation','right');
xlabel('M^+(z)')
h=legend('Hybrid Bloch-EPG','Bloch');
set(h,'Location','SouthEast');
set(h,'FontSize',10);
set(gca,'XAxisLocation','top');
grid on

% [KSPACE VIEW] 
% k-space axis for the SR-EPG
Sfisp_ks = ((0:(length(Sfisp_sp)-1)) - fix(length(Sfisp_sp)/2)) * 2 * pi / (field_of_view);
% for the ssEPG, k-states are configurations. Thus, k-space k' = -k.
% We do this by flipping signs:
SSframe_ks = -(((0:(size(SSEPG_frame,10)-1)) - fix(size(SSEPG_frame,10)/2)) * (rf.gamma * rf.G_ex * rf.dt));
[X,Y] = meshgrid(rf.pos(1:end-1),Sfisp_ks);

% [Hybrid Bloch-EPG] Perform Fourier Transformations
% - of the spatial profile for each configuration
Sfisp_k_states = exp(1i*X.*Y)*Sfisp_states(1:end-1,:) / length(Sfisp_sp) * max(rf.pos) * 2 / rf.slice_thickness;
% - of the summed configurations
Sfisp_k = exp(1i*X.*Y)*Sfisp_sp(1:end-1) / length(Sfisp_sp) * max(rf.pos) * 2 / rf.slice_thickness;

%%[Hybrid Bloch-EPG - k-Space Representation] 
% Plot k-space view of the configurations before superimposing them
subplot_local(4,4,[9 10]);
for n=1:length(Istates)
    col = color(n,:);
    plot((Sfisp_ks ./ ksp),(imag(Sfisp_k_states(:,Istates(n)))),'LineWidth',2,'Color',col); hold on;
    plot((Sfisp_ks ./ ksp),(real(Sfisp_k_states(:,Istates(n)))),':','LineWidth',2,'Color',col); hold on;
end
hold off

xlim([min(Isel)-0.5 max(Isel)+.5]);
xticks(Isel);
xticklabels({});
set(gca,'FontSize',16);
set(gca,'TickLength',[0.01;0.025])
set(gca,'TickDir','both')
ylabel('M^+(k'')')
grid on

% [ssEPG] Plot the Hybrid Bloch-EPG against the ssEPG solution
subplot_local(4,4,[13 14]);
plot(Sfisp_ks ./ ksp,imag(Sfisp_k),'k','LineWidth',2); hold on;
plot(SSframe_ks ./ ksp,squeeze(imag(SSEPG_frame(1,1,1,1,1,1,Iomega,1,1,:))),'r','LineWidth',1); hold on;
plot(Sfisp_ks ./ ksp,real(Sfisp_k),'k:','LineWidth',2); hold on;
plot(SSframe_ks ./ ksp,squeeze(real(SSEPG_frame(1,1,1,1,1,1,Iomega,1,1,:))),'r:','LineWidth',1); hold off;

xlim([min(Isel)-0.5 max(Isel)+.5]);
xticks(Isel);
set(gca,'FontSize',16);
set(gca,'TickLength',[0.01;0.025])
set(gca,'TickDir','both')
set(gca,'Color',[1 1 1 1]);
xlabel(['k-Space k'' (4\cdot{}2' char(960) '/Slice Thickness)'])
ylabel('M^+(k'')')
h=legend('Hybrid Bloch-EPG','ssEPG');
set(h,'FontSize',10);
grid on
clear subplot
%

% export figure if package export_fig is installed
export_fig(sprintf('%s/figure3.png',outdir),'-m6');%,'-transparent');

%% Error Metrics
% RMSE
MEAN_SIGNAL_BLOCH = abs(mean(squeeze(SSB(It,:,:,:,:,Id,Iomega))));
RMSE_Bloch = sqrt(mean(abs(Sfisp_sp - squeeze(SSB(It,:,:,:,:,Id,Iomega))).^2));
% we need to flip Sfisp_k (defined in k-space) to conform to the configuration state definition
% of the ssEPG
RMSE_SSEPG = sqrt(mean(abs(flip(Sfisp_k,1) - squeeze(SSEPG_frame(1,1,1,1,1,1,Iomega,1,1,:))).^2));

MEAN_SIGNAL_HYBRID = abs(mean(Sfisp_sp));
MEAN_SIGNAL_SSEPG = abs(SSEPG_frame(1,1,1,1,1,1,Iomega,1,1,find(SSframe_ks == 0))) / ( max(rf.pos) * 2 / rf.slice_thickness );

fprintf('nRMSE Hybrid Bloch-EPG\n');
fprintf(' - to Bloch:  %.2f %%\n', RMSE_Bloch/MEAN_SIGNAL_BLOCH * 100);
fprintf(' - to ssEPG: %.2f %%\n', RMSE_SSEPG/MEAN_SIGNAL_BLOCH * 100);

fprintf('Predicted Signal\n');
fprintf(' - 1 - Hybrid Bloch-EPG / Bloch:  %.2e\n', 1-MEAN_SIGNAL_HYBRID/MEAN_SIGNAL_BLOCH );
fprintf(' - 1 - ssEPG / Bloch: %.2e %%\n', 1- MEAN_SIGNAL_SSEPG/MEAN_SIGNAL_BLOCH);
