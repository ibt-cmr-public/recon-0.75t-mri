function mag = hardpulse(B1, dt, pos, G, FA, Apre, Apost, mag, R1, R2, gamma)
%HARDPULSE Simulate slice-selective RF pulse using consecutive hard-pulses
%mixed with precession 
%
%   B1          RF pulse shape, will be scaled to achieve FA in the isocenter [a.u.]
%   dt          time resolution (RF duration / length(B1)) [ms]
%   pos         isochromat positions to simulate [m]
%   G           slice selection gradient [mT/m]
%   dx          spatial resolution [m]
%   FA          iso flip anlge (to properly normalize B1) [rad]
%   Apre        pre-phaser area [mT/m ms]
%   Apost       rephaser area [mT/m ms]
%   mag         (Optional) Magnetization vector (3xN) 
%   nepg        (Optional) Number of EPG states         default: 1
%
% Copyright (c) 2021 ETH Zurich, Christian Guenthner, Thomas Amthor, Mariya Doneva, Sebastian Kozerke

if nargin<09 || isempty(R1); R1=0; end
if nargin<10 || isempty(R2); R2=0; end
if nargin<11 || isempty(R2); gamma = 42577.469*2*pi/1000; end
E1 = exp(-conj(R1).*dt);
E2 = exp(-conj(R2).*dt);

% make inputs the same size
assert(length(G)==length(B1),'B1 and G must be of the same length');
  
% get flip angle of passed pulse and scale it accordingly
fapulse = gamma .* trapz(B1) .* dt;
B1 = B1 ./ fapulse .* FA;

% repeat magnetization vector
if size(mag,2)==1
    mag = repmat(mag,1,length(pos));
end
assert(length(mag) == length(pos),'Magnetization and Position Vector must be of the same length');

% apply pre-winder gradient
mag = grad(mag,sum(Apre.*pos,1).*gamma);

% perform step-by-step simulation of RF pulse
for n=1:length(B1)       
    % apply tiny-hardpulse
    mag = RF(gamma*B1(n)*dt/2) * mag;

    % apply slice select gradient
    mag = grad(mag,sum(G(:,n).*pos,1).*gamma.*dt);
    
    % relaxation during the pulse
    mag(1,:) = mag(1,:).*E2;
    mag(2,:) = mag(2,:).*conj(E2);
    mag(3,:) = mag(3,:).*E1 + (1-E1);
    
    % apply tiny-hardpulse again
    mag = RF(gamma*B1(n)*dt/2) * mag;
end

% apply rewinder-gradient
mag = grad(mag,sum(Apost.*pos,1).*gamma);
end

function m=grad(m,phi)
% apply precession by phase array phi
    m(1,:) = m(1,:).*exp( 1i.*phi);
    m(2,:) = m(2,:).*exp(-1i.*phi);
end