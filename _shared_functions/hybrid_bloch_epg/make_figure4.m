%% make_figure4 Visualization of the signal recovery using the SR-EPG and the hybrid Bloch-EPG
%
% Copyright (c) 2021 ETH Zurich, Christian Guenthner, Thomas Amthor, Mariya Doneva, Sebastian Kozerke

%% (1st Column) Spatially Resolved EPG

% create log magnitude plot of k-vs-configuration state
for Ids=1:2
figure;
set(gcf,'Color',[1 1 1]);
imagesc(log10(abs(squeeze(S(It,1,:,1,1,Ids,1,1,1,:)))),'XData',flip(kstates(:))-0.5,'YData',pos(:)*1000-dz/2);
colormap(viridis(512));
caxis([-10 0]);
axis([-100 100 -7.5 7.5]);
%xticklabels({})
%yticklabels({})
set(gca,'FontSize',16)
ylabel('Position (mm)')
xlabel('Configuration States')
export_fig(sprintf('%s/figure4/view_labeled_z-k_t%i_d%i_withlabel.png',outdir,It,Ids),'-m4');

% and without label
figure;
imagesc(log10(abs(squeeze(S(It,1,:,1,1,Ids,1,1,1,:)))),'XData',flip(kstates(:))-0.5,'YData',pos(:)*1000-dz/2);
colormap(viridis(512));
caxis([-10 0]);
axis([-100 100 -7.5 7.5]);
%xticklabels({})
%yticklabels({})
set(gca,'Visible','Off')
export_fig(sprintf('%s/figure4/view_labeled_z-k_t%i_d%i.png',outdir,It,Ids),'-m4');
end

%% (2nd Column) K-Space vs Configuration States
echoes = todim(kstates,11);
figure
set(gcf,'Position',[100 100 1500/3 1500])
clf
fac=[0 1 10000000000];
for n=1:length(fac)
subtightplot(4,1,n+1,[],[],.2);    
    
dk=ksp/4 * fac(n);
mod_func_spatial = sinc(dz.*(dk).*(echoes-kstates)/2./pi).*exp(-1i.*dk.*pos.*(echoes-kstates));

imagesc(log10(abs(squeeze(sum(mod_func_spatial.*S(It,1,:,1,1,Id,1,1,1,:),3))).'),'XData',flip(kstates(:))-0.5,'YData',flip(echoes(:))-.5);
colormap(viridis(512));
caxis([-10 0]);
axis square
%xticklabels({})
%yticklabels({})
set(gca,'FontSize',16)
ylabel('Echo Order (q)')
if n==3
    xlabel('Configuration State (k)')
else
    xticklabels({});
end
end
export_fig(sprintf('%s/figure4/view_labeled_alli.png',outdir),'-m4');

%% (3rd Column) Plots Combined Modulation Function and Selected Echo Traces
% 
echoes = todim(0,11);
figure
set(gcf,'Position',[100 100 1500/3 1500])
set(gcf,'Color',[1 1 1]);
clf

fac=[0 1 10000000000];
for n=1:length(fac)
    subtightplot(4,1,n+1,[],[],.2);
dk=ksp/4 * fac(n);
mod_func_spatial = sinc(dz.*(dk).*(echoes-kstates)/2./pi).*exp(-1i.*dk.*pos.*(echoes-kstates));
sig = squeeze(sum(mod_func_spatial.*S(It,1,:,1,1,Id,1,1,1,:),3)) .* dz / slice_thickness;

   
yyaxis left
plot(kstates(:),imag(sig),'-x','LineWidth',2,'MarkerSize',5,'Color',[0 0 0]);
set(gca,'FontSize',16)
ylabel('Imaginary Part')
ylim([-.12 .1]);

yyaxis right
grid on
plot(kstates(:), abs(sig),'.-','LineWidth',2,'MarkerSize',15);

ylabel('Amplitude')
ylim([1e-10 100])

xlim([-50 50])    
xticks([-50:25:50])

a=gca;
a.YAxis(2).Scale='log';
a.YMinorGrid='on';
a.FontSize=16;
%a.YAxis(2).Color = a.YAxis(1).Color;
a.YAxis(1).Color = [ 0 0 0];
grid on

axis square

if n==3
    xlabel('Configuration State (k)')
else
    xticklabels({});
end

end

omega_for_plot = [pi 3*pi/4 pi/2 pi/4 0]/TR; %linspace(-pi,pi,201)/TR;
TE = 0;
R2p = 1./[100:50:300];
mod_func_config = exp(1i.*omega_for_plot.*(kstates.*TR + TE) - abs(kstates.*TR + TE).*R2p).*E2(Id).^(TE);

sig = squeeze(mod_func_config);
%
subtightplot(4,1,1,.1,[],.2);

yyaxis left
plot(kstates(:),real(sig)+2*(-2:2)','-x','LineWidth',2,'MarkerSize',5,'Color',[0 0 0]);
set(gca,'FontSize',16)
ylabel('Real-Part')
ylim([-5 5])
yticks([-5:1:5]);
xlabel('Configuration State (k)')
yyaxis right
plot(kstates(:), imag(sig)+2*(-2:2)','.-','LineWidth',2,'MarkerSize',15);
ylabel('Imaginary-Part')
ylim([-5 5])
xlim([-50 50])
xticks([-50:25:50])
yticks([-5:1:5]);
a=gca;
a.YMinorGrid='on';
a.FontSize=16;
%a.YAxis(2).Color = a.YAxis(1).Color;
a.YAxis(1).Color = [ 0 0 0];
grid on

export_fig(sprintf('%s/figure4/modfunc_and_states.png',outdir),'-m4');

%% (4th Column) SSFP Frequency Profiles
figure
set(gcf,'Position',[100 100 1500/3 1500])
set(gcf,'Color',[1 1 1]);

fac=[0 1 10000000000];
for n=1:length(fac)
subtightplot(4,1,n+1,[],[],.2);
dk=ksp/4 * fac(n);
    
mod_func_spatial = sinc(dz.*(dk).*(echoes-kstates)/2./pi).*exp(-1i.*dk.*pos.*(echoes-kstates));

omega_for_plot = linspace(-pi,pi,501)/TR;
TE = 0;
R2p = 1./100;
mod_func_config = exp(1i.*omega_for_plot.*(kstates.*TR + TE) - abs(kstates.*TR + TE).*R2p).*E2(Id).^(TE);
sig = squeeze(sum(mod_func_config.*sum(mod_func_spatial.*S(It,1,:,1,1,Id,1,1,1,:),3),10)) .* dz / slice_thickness;

%

yyaxis left
plot(omega_for_plot(:)/pi*TR,abs(sig),'-','LineWidth',2,'MarkerSize',5,'Color',[0 0 0]);
set(gca,'FontSize',16)
ylabel('Signal Magnitude')
ylim([0 0.17]);
yyaxis right
tmp = unwrap(angle(sig));
tmp = tmp - tmp(find(omega_for_plot==0));
plot(omega_for_plot(:)/pi*TR, tmp,'-','LineWidth',2,'MarkerSize',15);
ylabel('Signal Phase (rad)')
ylim([-pi pi]);
yticks([-pi,-pi/2,0,pi/2,pi]);
yticklabels({['-' char(960)],['-' char(960) '/2'], '0',[char(960) '/2'], char(960)});

a=gca;
a.YMinorGrid='on';
a.FontSize = 16;
axis square
%a.YAxis(2).Color = a.YAxis(1).Color;
a.YAxis(1).Color = [ 0 0 0];
if n==3
    xlabel('Off-Resonance (\pi/TR)')
else
    xticklabels({});
end
grid on
    
end

export_fig(sprintf('%s/figure4/signal_response.png',outdir),'-m4');