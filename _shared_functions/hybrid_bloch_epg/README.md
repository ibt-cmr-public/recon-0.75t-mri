# A Unifying View on Extended Phase Graphs and Bloch Simulations for Quantitative MRI
Please cite the following publication, when using the code:

> Christian Guenthner, Thomas Amthor, Mariya Doneva, and Sebastian Kozerke. <br />
> A Unifying View on Extended Phase Graphs and Bloch Simulations for Quantitative MRI. <br />
> Scientific Reports, 2021. <br />
> doi: 10.1038/s41598-021-00233-6

A good place to start is the `example_figures.m`, which will reproduce the main figures of the publication. `make_figure...m` produces the MATLAB parts of each of the publication figures. These files cannot be run standalone but require the initialization part of `example_figures.m` to be run before (this pre generates the phase graph shared between all the figures).

The central files are:
- `sinc_pulse.m` generates a hann-apodized sinc pulse for the simulation
- `hardpulse.m` simulates an RF pulse by alternating hardpulses and precession
- `RF_matrix.m` constructs the spatial RF operator by running `hardpulse.m` for M+, M-, and Mz magnetization
- `srepg.m` performs an SR-EPG calculation for a gradient echo sequence with constant TR
- `ssepg.m` a reimplementation of Jason Ostenson's slice-selective EPG
- `bloch.m` an implementation of the rotation operator algorithm that directly solves the Bloch equation
- `hybrid_bloch_epg.m` evaluates the analytical hybrid Bloch-EPG equation for a given pre-calculated SR-EPG.
- `init.m` replaces missing packages with placeholder functions / fallbacks (see optional external codes)

## References
> Leupold, J. <br />
> Steady-state free precession signals of arbitrary dephasing order and their sensitivity to T2*. <br />
> Concepts Magn. Reson. Part A Bridg. Educ. Res. 46A, (2017). <br />
> doi: 10.1002/cmr.a.21435 

> Malik, S. J., Padormo, F., Price, A. N. & Hajnal, J. V. <br />
> Spatially resolved extended phase graphs: Modeling and design of multipulse sequences with parallel transmission.<br />
> Magn. Reson. Med. 68, 1481:1494 (2012). <br />
> doi: 10.1002/mrm.24153 

> Weigel, M. <br/>
> Extended phase graphs: Dephasing, RF pulses, and echoes - pure and simple. <br />
> J. Magn. Reson. Imaging 41, 266:295 (2015). <br />
> doi: 10.1002/jmri.24619 

> Ostenson, J., Smith, D. S., Does, M. D. & Damon, B. M. <br />
> Slice-selective extended phase graphs in gradient-crushed, transient-state free precession sequences: An application to MR fingerprinting. <br />
> Magn. Reson. Med. mrm.28  381 (2020). <br />
> doi: 10.1002/mrm.28381 

## Optional External Codes
We used a couple of external libraries to generate the plots for our publication. These are optional and not strictly required to run the code.

### export_fig
by Oliver Woodford and Yair Altman. https://github.com/altmany/export_fig

We used `export_fig` to save MATLAB figures as highresolution pngs for publication.
If not in the search path, figures will not be exported.

### subtightplot
by Felipe G. Nievinski. https://ch.mathworks.com/matlabcentral/fileexchange/39664-subtightplot

We used `subtightplot` for better control of subplot placements. Download it from matlab central if needed.
The script is automatically replaced by matlab's `subplot`.

### colorcet
by Peter Kovesi. https://colorcet.com/download/index.html
> Peter Kovesi. Good Colour Maps: How to Design Them. arXiv:1509.03700 [cs.GR] 2015

We use colorcet 'C9' mainly for phase maps (a cyclic color maps). 
Its replaced by matlab's `hsv` if not on the search path.

### Perceptually uniform colormaps (viridis, magma, inferno)
by  Ander Biguri. https://ch.mathworks.com/matlabcentral/fileexchange/51986-perceptually-uniform-colormaps

We use these maps in various places of our work. They are automatically replaced by `jet`, `hot`, and `parula`.