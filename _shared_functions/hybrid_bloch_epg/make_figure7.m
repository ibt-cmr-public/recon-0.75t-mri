%% make_figure7 Dependency of signal amplitude and phase on echo-time and off-resonance
%
% Copyright (c) 2021 ETH Zurich, Christian Guenthner, Thomas Amthor, Mariya Doneva, Sebastian Kozerke

T2ps = [10]
for T2p = T2ps    
    
echoes=-2:1;
figure

set(gcf,'Position',[100 100 1600 600])
cnt=1;

% spoiling moments (as factors of 2pi/slice thickness)
fac=[0 4];
msig=inf;

% iterate over spoiling factors, echoes, and plot type (abs / angle)
for n=1:length(fac)
for ec=1:length(echoes)
for prep=1:2
    % skip unrealistic combos
    if n==1 && ec~=1; continue; end
    if n==3 && ec==1; cnt=cnt+1; end

    % make sure echo is empty for bSSFP:
tmp_echo = echoes(ec);
if fac(n)==0; tmp_echo = []; end

h=subtightplot(2,5,cnt+(prep-1)*5,.025,.1,.1);
% calculate spoiling moment
dk=ksp/4 * fac(n);    

% span offresonance and echo time:
omega = linspace(-pi,pi,501)/TR;
TE_plot = todim(linspace(0,1,301) * TR,7);

sig = squeeze(hybrid_bloch_epg(S(It,:,:,:,:,Id,:,:,:,:),pos,kstates,E2(Id),dk,TR,TE_plot,tmp_echo,omega,1./T2p));
if n==1; msig=max(abs(sig(:))); end

    if prep==1
        imagesc(abs(sig).','XData',omega(:)/pi*TR,'YData',TE_plot(:)/TR);

        colormap(h,gray(512));
        caxis([0 msig]);
    else
        imagesc(angle(sig).','XData',omega(:)/pi*TR,'YData',TE_plot(:)/TR);

        colormap(h,colorcet('C9'));
        caxis([-pi pi]);
    end
    
    if n==1
        if prep==1
            ylabel({'{\bf Signal Magnitude}','Echo Time / TR'})
        else
            ylabel({'{\bf Signal Phase}','Echo Time / TR'})
        end
    else
        yticklabels({}); 
    end
    if prep == 1
        xticklabels({});
    else
        xlabel('Off-Resonance (\pi/TR)');
    end
    
    axis square
       
    if prep==1
    if n==1
        title('Balanced SFFP');    
    else
        title(sprintf('Spoiled SSFP - F(%i)',echoes(ec)));
    end
    end
    
    a=gca;
    a.FontSize = 16;
    
    grid on
    if prep==2
        cnt=cnt+1;
    end
    end
end
end

export_fig(sprintf('%s/figure7.png',outdir),'-m4');
end