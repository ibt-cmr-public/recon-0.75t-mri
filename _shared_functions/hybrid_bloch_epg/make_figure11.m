%% make_figure11 Slice profile plots for low and high flip angle 
%
% Decomposes the RF_matrix operator into effective flip angle and pulse
% phase. Then obtains the effective RF pulse from these parameters and
% checks the remainder to the RF_matrix operator if it is indeed diagonal
% and obtains its phase. That's phi_pre.
%
% Copyright (c) 2021 ETH Zurich, Christian Guenthner, Thomas Amthor, Mariya Doneva, Sebastian Kozerke

FA_plot = [30 70]/180*pi;
rf_mat_plot = nan(3,3,number_isochromats,length(FA_plot));

rf_plot = sinc_pulse(max(FA_plot),'slice_thickness',slice_thickness,'Nsp',number_isochromats ,'fov',field_of_view);
for n=1:length(FA_plot)
    % calculate rf slice profile matrices for different flipangles assuming
    % pure amplitude scaling of the sinc-pulse

    [rf_mat_plot(:,:,:,n),pos] = RF_matrix(rf_plot,FA_plot(n),'Nsp',number_isochromats ,'fov',field_of_view);
end

%%
ksp_plot = 2*pi / rf_plot.slice_thickness;
sp = rf_mat_plot(:,:,:,:);

% define a spatial frequency axis
spfft_x = todim(linspace(-15,15,5001) * ksp_plot,5);%((1:size(spfft,3)) - floor(size(spfft,3)/2) - 1) * pi / rf.pos(end);

% fourier transform rf matrices
spfft = permute(sum(sp .* exp(-1i .* rf_plot.pos .* spfft_x),3) / length(spfft_x),[1 2 5 4 3]);

% obtain effective flip angle with phase from excitation slice profile
% sp (1,3,:)
fa = asin(abs(sp(1,3,:,:))).*exp(1i*(angle(sp(1,3,:,:))+pi/2));

% get the effective rotation matrices
rot_mat = RF(fa);

% determine the residual matrix
for i=1:2
    for m=1:size(sp,3)
        residual(:,:,m,i) = rot_mat(:,:,m,i) \ sp(:,:,m,i);
    end
    % check if its indeed diagonal
    fprintf('%i: sum(||Residual Matrix| - eye(3)|^2) = %e\n',i,sum(sum(sum(abs(abs(residual(:,:,:,i)) - eye(3)).^2,1),2),3));
end
% obtain the residual phase
phi_residual = angle(residual(1,1,:,:));


%% and plot ...
%
pic = char(hex2dec('03C0'));
figure
a = gca;
colors = a.ColorOrder;
set(gcf,'Position',[100 100 700 700]);
set(gcf,'Color',[1 1 1])
%  
for n=1:2    
    subtightplot(2,1,n,.05,[.1 .05],.15)
    sel = @(x,a,b)squeeze(x(a,b,:,n));

    yyaxis left
    a = gca;
    a.YColor = [0 0 0];
    plot(rf_plot.pos(:) * 1000, sel(abs(fa),1,1) /pi * 180,'k','LineWidth', 2);
    ylabel('Effective Flip Angle (°)');
    yyaxis right
    a = gca;
    a.YColor = colors(1,:);
    plot(rf_plot.pos(:) * 1000, sel(angle(fa),1,1),'LineWidth', 2,'Color',colors(1,:)); hold on
    plot(rf_plot.pos(:) * 1000, sel(phi_residual,1,1),':','LineWidth', 2,'Color',colors(1,:)); hold off
    ylabel('Phase (rad)');

    xlim([-1.5 1.5]*slice_thickness*1000);
    xlabel('Position (mm)')

    ylim([-pi pi])
    yticks([-pi -pi/2 0 pi/2 pi])
    yticklabels({...
        ['-' pic],['-' pic '/2'],'0',[pic '/2'],[pic]})

    if n==1
        a.XAxis.Visible=1;
        a.XAxis.TickLabels=[];
        a.XAxis.Label=[];
    end

    grid on
    set(gca,'FontSize',12);        
end

export_fig(sprintf('%s/figure11.png',outdir),'-m4')