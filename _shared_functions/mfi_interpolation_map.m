function interpolation_map = mfi_interpolation_map(mfi_frequencies, tAcq, B0)
%%mfi_interpolation_map returns the interpolation map for a set of
%%multi-frequency interpolation (MFI) frequencies, acquisition time points
%%(tAcq) and B0 map.
%
% Inputs:
% mfi_frequencies       multi-frequency interpolation grid frequencies(kHz)
% tAcq                  acquisition time-points (ms)
% B0                    "b0-map" in kHz!
% 
% This function follows the paper of 
% Man, L.-C., Pauly, J. M. & Macovski, A. Multifrequency interpolation for 
% fast off-resonance correction. Magn. Reson. Med. 37, 785-792 (1997).
%
% (c) Christian Guenthner, ETH Zurich, 2023

sz = size(B0);
% clip our b0 map to the MFI frequency range
B0 = max(min(B0(:),max(mfi_frequencies)),min(mfi_frequencies));
% get coefficients
Cmat = freqInterpCoeffLSQ(mfi_frequencies,B0(:),tAcq * 2 * pi);   
% reshape to match size of B0 map input (MFI x size(B0,1) x ...)
interpolation_map = reshape(Cmat.',[size(Cmat,2) sz]);
end

function [Cmat]=freqInterpCoeffLSQ(freq,gridfreq,t)
%% Least-Squares Solution to Encoding Coefficiencts
%1. Man, L.-C., Pauly, J. M. & Macovski, A. Multifrequency interpolation for fast off-resonance correction. Magn. Reson. Med. 37, 785�792 (1997).
    
% make sure everything is double-precision
freq = double(freq);
gridfreq = double(gridfreq);
tt = double(t);
    
    
% mesh actual frequencies (freq) and grid frequencies
[dw,ts]   =meshgrid(freq,tt);
[gf,ts_gf]=meshgrid(gridfreq,tt);

% setup actual encoding problem
emat = exp(-1i.*dw.*ts).';
% setup gridded encoding problem
Tmat = exp(-1i.*gf.*ts_gf).';

% and get the interpolant
Cmat = Tmat*pinv(emat);
end