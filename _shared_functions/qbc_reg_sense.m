function [d_Reg, settings]=qbc_reg_sense(data, S, qbc_img, varargin)
%% qbc_reg_sense Body coil image regularized coil-combination
% 
% Note: this algorithm does not do SENSE unfolding.
%
% broadly follows
% "SENSE reconstruction using feed forward regularization"
% Fuderer, van den Brink, Jurrissen; ISMRM 2004
%
% Inputs:
% data      the data to coil combine (dim: 4 needs to be the coil dimension)
% S         coil sensitivity estimate (dim 1..4 need to match data)
% qbc_img   body coil image estimate (dim: 1..3 need to match data)
%
% Optional Parameters (supply as 'name',<value> pairs)
% 'lambda'  list of regularization strengths to try
% 'offset'  offsets qbc image by a small amount to reduce close-to-zero
%           division errors
% 'power'   qbc image regularization power. Fuderer et al. proposes -2 
%           ("R" in their abstract proportional to square of QBC scan).
%           imho, p=1 works much better 
%           (i.e. "R" inverse proportional to |QBC|). Default: 1
%
% Returns:
% d_Reg     regularized SENSE coil-combined image
% settings  the algorithm settings structure.
%
% (c) Christian Guenthner, ETH Zurich, 2023

p = inputParser;
% list of regularization strengths to try
p.addOptional('lambda',10.^linspace(-7,2,51));
% we also included a small offset to reduce artifacts from close-to-zero qbc
p.addOptional('offset',1e-4);
p.addOptional('power',1); 
p.parse(varargin{:});

% 
lambda = p.Results.lambda;
offset = p.Results.offset;
power = p.Results.power;
settings = p.Results;

sz = sizen(data,5);
% data vector:
d = double(reshape(permute(data(:,:,:,:,:),[4 1 2 3 5]),[],prod(sz(5:end))));
% setup sparse matrix operators
Sp = reshape(permute(S,[4 1 2 3]),[],1);
k = 1:length(Sp);
coil_id = mod(k-1,sz(4));
px_id = floor((k-1)/sz(4));
Sp  = sparse(coil_id + 1 + px_id * sz(4),px_id + 1 ,double(Sp));
% sparse qbc regularization
px_id = 1:prod(sz(1:3));
Qbc = sparse(px_id,px_id,double(abs(qbc_img(:)).^power + offset));

sz(4) = 1;
d_Reg = zeros([length(lambda) sz]);

% iterate over reg. strengths:
for n=1:length(lambda)
% do recon (S'*S + R^-1)^-1*S' * d;
d_Reg(n,:) = reshape((Sp' * Sp + lambda(n) * Qbc)\Sp' * d,1,[]);
end

% permute the regularization dimension to the back
d_Reg = permute(d_Reg,[2:ndims(d_Reg) 1]);