function F=tenmul(F,u,dims,conjflag)
%% tenmul multiply a tensor F with matrices u along dimensions dims
%
% Inputs:
% F         A tensor
% u         A cell array of matrices
% dims      A vector containing the dimensions at which to apply u
% conjflag  specify if u should be complex transposed before multiplication
%
% (c) Christian Guenthner, ETH Zurich, 2023

if nargin<3 || isempty(dims); dims = 1:length(u); end
if nargin<4 || isempty(conjflag); conjflag = false; end
if length(conjflag)==1;  conjflag = conjflag.* ones(1,length(dims)); end
    for n=1:length(dims)
        if isempty(u{n}); continue; end
        
        dim = dims(n);
        order = [dim setdiff(1:ndims(F),dim)];
        % permute tensor
        F = permute(F,order);
        sz = size(F);
        
        % multiply
        if ~conjflag(n)
            F = u{n} * F(:,:);
        else
            F = u{n}' * F(:,:);
        end
        sz(1) = size(F,1);
        
        % reshape & permute back
        F = ipermute(reshape(F,sz),order);
    end
end