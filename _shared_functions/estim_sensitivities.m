function S = estim_sensitivities(data)
%% estimates coil sensitivities using BART's ecalib command
%
% This is a wrapper script for BART's ecalib command. Make sure to have a
% valid BART installation running, path variables and your MATLAB search
% path are setup to call BART (type `bart` in the MATLAB command line to 
% verify, you should see a list of BART commands).
%
% Input:
% data      dimension 4 is coil dimension
%
% Output:
% Returns: normalized sensitivities.
% 
% References: Martin Uecker and Michael Lustig. 
% Estimating Absolute-Phase Maps Using ESPIRiT and Virtual Conjugate Coils. 
% Magnetic Resonance in Medicine 77:1201-1207 (2017)
%
% (c) Christian Guenthner, ETH Zurich, 2023

% move dynamics to time dimension
I = 1:max(ndims(data),13); 
I(5)=11; I(11)=5;
data = permute(data,I);

% get k-space
ksp = bart('fft 7 ', data);
% and estimate sensitivity
S = bart('ecalib -m1 -c0 ', ksp);

% normalize sensitivities
S = S./sum(abs(S).^2,4);