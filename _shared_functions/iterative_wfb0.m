function [wf,b0_map,dTE] = iterative_wfb0(data,TE,varargin)
%% iterative_wfb0 Iterative B0 reconstruction from multi-echo Dixon data w/ 
% fat-spectrum deblurring.
%
% Input:
% data: 4-d array (dim 1...3: spatial dimensions, dim 4: TE1/TE2 data)
% TE:   array of echo times
%
% Returns:
% wf:     water/fat separated maps
% b0_map: final b0_map estimate
% dTE:    difference in echo times
% 
% Optional Parameters (supply as 'name',<value> pairs)
% 'field_strength'      The field strength at which data was acquired 
%                       (def: 0.75T)
% 'fat_shifts'          The fat model chemical shift as a column-vector
%                       (1xN). (Default: Philips 7-peak fat model)
% 'fat_areas'           The area of each peak as a column-vector
% 'sigma'               Specifies the amount of blurring of the b0 map
%                       along all three dimensions (columns) for each 
%                       iteration (rows).
%                       (Default: 16 iterations w/ progressively reduced
%                       blurring)
% 'plot'                Toggle if plotting should be performed for each
%                       iteration ('all'), only for the final result
%                       ('last') or not at all ('off'). Default: 'all'
% 'unwrap'              Function handle to unwrap phase-difference map with
%                       (default:
%                       @(x)unwrap_snaphu_mag(ones(size(x)).*exp(1i.*x)))
%                       Use `@(x)x` to disable unwrapping.
% 'offset_frequency'    Demodulate by offset frequency before water/fat
%                       separation (useful in case water/fat are swapped)
% 'reference_echo'      Nr. of echo used to correct for image phase (def:1)
% 'b0_map'              Initial b0 map (Def: empty = start from 0s)
%
% (c) Christian Guenthner, ETH Zurich, 2023

p = inputParser();
p.addOptional('field_strength',0.75);
%p.addOptional('fat_offresonance',todim([440/3],2));
%p.addOptional('fat_areas',1);
% 7-peak offresonance model ((c) Philips)
p.addOptional('fat_shifts',todim([-105.9927  -82.5458   22.8045 -118.8403  -96.6782  -75.4797  -58.7778],2)/0.75);
p.addOptional('fat_areas',todim([0.6250    0.0950    0.0420    0.0850    0.0710    0.0660    0.0160],2));
% progressively reduce amount of B0-map blurring (heuristic approach).
% numbers are relative to the image size!
p.addOptional('sigma',[[linspace(60,10,10) 10 8 6 4 2 1]'.*ones(1,2) ones(16,1)*2]);
p.addOptional('plot','all',@(x)strcmpi(x,{'all','final','off'}));
p.addOptional('offset_frequency',0);
p.addOptional('reference_echo',1);
p.addOptional('b0_map',[]);
p.addOptional('unwrap',@(x)unwrap_snaphu_mag(ones(size(x)).*exp(1i.*x)));
p.parse(varargin{:});

% make sure we cut off all zero-padded areas from our data
nonzero = ~all(data(:,1,1,:)==0,4);
data = data(nonzero,:,:,:);

TE = todim(TE,4);
dTE = TE(2)-TE(1);
sz = size(data);

% copy settings
offresonances = p.Results.fat_shifts * p.Results.field_strength;
areas = p.Results.fat_areas;
sigma = p.Results.sigma;

% output the model
fprintf('Fat Model:\n');
for n=1:length(offresonances)
    fprintf('%2i: %0.3f @ %7.3f Hz\n',n,areas(n),offresonances(n));
end
fmean_fat = sum(areas .* offresonances)./sum(areas);
[~,I] = max(areas);
fpeak_fat = offresonances(I);
fprintf('Weighted Average Fat Frequency: %f Hz\n\n',fmean_fat);
fprintf('Max. Intensity Fat Frequency: %f Hz\n\n',fpeak_fat);

% out-of-phase time of water/fat
Topwf = 1e3/fmean_fat/2;

% starting value for b0 map
if isempty(p.Results.b0_map)
    b0_map = zeros(sz(1:3));
else
    b0_map = p.Results.b0_map;
end

% correct by phase of TE1 image
data = data .* exp(-1i * angle(data(:,:,:,p.Results.reference_echo)) -1i .* 2*pi* (b0_map + p.Results.offset_frequency) .* TE/1e3);

% determine water/fat encoding matrix 
encmat = [];
for n=1:length(TE)
    encmat(n,:) = cat(2, 1, sum( areas .* exp(1i .* 2*pi .* todim(TE(n),3) .* (offresonances) ./ 1e3),2)/sum(areas,2));
end

% plot iterations:
if ~strcmpi(p.Results.plot,'off')
    try;close(3);catch; end
    figure(3);
    set(gcf,'Position',[      680         558        1236         420]);
end

% iterate
for iter=1:size(sigma,1)

    % estimate water fat images
    wf = permute(data,[4 1 2 3]);
    sz = size(wf);
    sz(1) = 2;
    wf = ipermute(reshape(pinv(encmat) * wf(:,:),sz),[4 1 2 3]);
    
    % synthesize b0 free water fat image
    wfsynth  = permute(abs(wf),[4 1 2 3]);
    sz = size(wfsynth);
    sz(1) = length(TE);
    wfsynth = ipermute(reshape(encmat * wfsynth(:,:),sz),[4 1 2 3]);
    
    % determine difference between measured and synth
    di = data ./ wfsynth;

    % estimate b0
    if size(di,4)>2
        b0_map_hires = getf0(di,dTE,Topwf,p.Results.unwrap);
    else
        b0_map_hires = angle(di(:,:,:,2:end)./di(:,:,:,1));
        % unwrap phase-difference map using snaphu
        b0_map_hires = p.Results.unwrap(b0_map_hires);
        % calculate actual b0 in [Hz]
        b0_map_hires = b0_map_hires./dTE/2/pi*1e3;
    end
    % downsample
    if ~any(sigma(iter,:)==0)
        b0_low_res = imgaussfilt3(b0_map_hires,sigma(iter,:));
    else
        b0_low_res = b0_map_hires;
    end
    
    % update our map
    b0_map = b0_map + b0_low_res;

    % demodulate data
    data = data .* exp(-1i .* b0_low_res .* TE * 2*pi/1e3);

    % update plot
    if strcmpi(p.Results.plot,'all') || (strcmpi(p.Results.plot,'last') && iter==size(sigma,1))
        % data was just demodulated, so update our wf image before we plot:
        wf = permute(data,[4 1 2 3]);
        sz = size(wf);
        sz(1) = 2;
        wf = ipermute(reshape(pinv(encmat) * wf(:,:),sz),[4 1 2 3]);
        sz = size(wf);
        wf = addzeros(wf,nonzero,sz);
        sz(4) = 1;
        b0h = addzeros(b0_map_hires,nonzero,sz);
        b0l = addzeros(b0_low_res,nonzero,sz);
        b0 = addzeros(b0_map,nonzero,sz);

        sl = min(max(round(size(wf,3)/2),1),size(wf,3));
        subplot(1,5,1); 
        imagesc(abs(wf(:,:,sl,1))); title('Water');
        subplot(1,5,2); 
        imagesc(abs(wf(:,:,sl,2))); title('Fat');
        subplot(1,5,3); 
        imagesc(b0h(:,:,sl)); title('High-Res B0 Update')
        subplot(1,5,4); 
        imagesc(b0l(:,:,sl)); title('Low-Res B0 Update')
        subplot(1,5,5); 
        imagesc(b0(:,:,sl)); title('Current B0 Map')
        
        for n=1:5;subplot(1,5,n);
            axis('square'); 
            set(gca,'xtick',[],'ytick',[],'xticklabels',[],'yticklabels',[]);
        end

        colormap(gray(256))    
        drawnow
    end
end

% re-estimate water fat from final demodulated scan data:
wf = permute(data,[4 1 2 3]);
sz = size(wf);
sz(1) = 2;
wf = ipermute(reshape(pinv(encmat) * wf(:,:),sz),[4 1 2 3]);

% add zero-pads again:
sz = size(wf);
wf = addzeros(wf,nonzero,sz);
sz(4)=1;
b0_map = addzeros(b0_map,nonzero,sz);

end

function f0=getf0(data, dTE, wfs, unw)
    % get phase difference maps
    ddata = data(:,:,:,2:3)./data(:,:,:,1:2);
    
    % unwrap phase differences such that water/fat is unwrapped
    udata = unw(angle(ddata)./dTE.*wfs);

    % we have multiple images of the same thing. Determine the average
    % shift wrt to 2*pi and correct for it
    pishift = round(mean(mean(udata,1),2)/2/pi)*2*pi;
    pdata = (udata-pishift) ./ wfs / 2 /pi *1e3;

    % let's average over all results
    f0 = mean(pdata,4);
end

function wfz=addzeros(wf,nonzero,sz)
% adds the zeros again that we stripped in the beginning
    wfz = zeros([size(nonzero,1),sz(2:4)]);
    wfz(nonzero,:,:,:) = wf;
end