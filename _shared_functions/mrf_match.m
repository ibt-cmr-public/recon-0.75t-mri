function m = mrf_match(dict, b1p_map, data)
%% mrf_match performs matching of data against a tucker-compressed dictionary 
%% with B1+ interpolation based of a B1+ map
%
% - Rounds the supplied B1+ map to 1% values
% - Upscaling of B1+ to match MRF scan resolution
% - Obtains B1+ corrected dictionaries by linear interpolation of 
%   the B1 basis functions
% - separate matching of water and fat channels
%
% Input:
% dict      dictionary structure with 
%           .D    the core tensor of the HOSVD decomposed dictionary
%           .U    the transformation matrices for time, b1 and T1/T2
%           .T1   the T1 range
%           .T2   the T2 range
%           .b1p  the B1+ range
% b1p_map   the B1+ map (we try to scale it to the same resolution)
% data      the water/fat separated data
%           dim: 1..3 spatial
%           dim: 9    water/fat channels
%           dim: 11   coefficients of temporal basis function
%
% Output:
% m         match structure with T1/T2/rho maps, scaling value & index maps
%
% (c) Christian Guenthner, ETH Zurich, 2023


% dictionary b1+ resolution:
b1p = dict.b1p;
% resize the B1+ map and round to 1% steps
b1p_rounded = round(max(min(abs(imresize(b1p_map, size(data,1)/size(b1p_map,1))),max(b1p)),min(b1p)),2);
% get unique values
b1p_u = unique(b1p_rounded(:));

% prepare data (order it time x water/fat x spatial)
order = [11 9 1:4];
order = [order setdiff(1:ndims(data),order)];
data = permute(data,order);
sz = size(data);
Nc = sz(2);

% setup arrays for matching results
m.T1 = zeros(Nc,sz(3),sz(4));
m.T2 = zeros(Nc,sz(3),sz(4));
m.I  = zeros(Nc,sz(3),sz(4));
m.v  = zeros(Nc,sz(3),sz(4));
m.rho= zeros(Nc,sz(3),sz(4));

% iterate over unique b1p values
Irec = [];
for n=1:length(b1p_u)
    % interpolate dictionary
    
    Ub1 = interp1(b1p,dict.U{2},b1p_u(n),'linear');
    % get dictionary
    D = squeeze(tenmul(dict.D,{Ub1,dict.U{3}},2:3,false));
    
    % make sure it is normalized
    norm = interp1(b1p,squeeze(dict.norm),b1p_u(n),'linear');
    D = D .* norm;
    norm = sqrt(sum(abs(dict.U{1} * D).^2,1));
    D = squeeze(D ./ norm);
    
    % select voxel
    Iv = find(b1p_rounded(:) == b1p_u(n));
    Irec = cat(1,Irec,Iv(:));
    
    % for both water/fat channels:
    for fatc = 1:Nc
        % match by maximum dot product
        [m.v(fatc,Iv),m.I(fatc,Iv)] = max(abs(D' * squeeze(data(:,fatc,Iv))),[],1);    
        % and obtain corresponding T1/T2 values
        m.T1(fatc,Iv) = dict.T1(m.I(fatc,Iv));
        m.T2(fatc,Iv) = dict.T2(m.I(fatc,Iv));
        m.rho(fatc,Iv) = m.v(fatc,Iv) ./ norm(m.I(fatc,Iv));
    end
end

% and permute the arrays for easier visualization
m.T1 = permute(m.T1,[2 3 1]);
m.T2 = permute(m.T2,[2 3 1]);
m.v = permute(m.v,[2 3 1]);
m.I = permute(m.I,[2 3 1]);
m.rho = permute(m.rho,[2 3 1]);

end