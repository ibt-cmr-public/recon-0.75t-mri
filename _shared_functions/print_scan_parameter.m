function print_scan_parameter(file, write_to_file)
%% print_scan_parameter reads a .raw data file and extracts most important scan parameter
% if write_to_file is true, output is directed into a .txt file with equal
% name as the scan data.
%
% (c) Christian Guenthner, ETH Zurich, 2023

% is write_to_file flag provided?
if nargin<2 || isempty(write_to_file); write_to_file = 0; end
% by default, print to standard out (screen)
fid = 1;

% load raw data
r = MRecon(file);

% open file stream
if write_to_file
    fid = fopen([r.Parameter.Filename.Data(1:end-4) '.txt'],'w');
end

% short cuts to read raw parameters
g = @(x,varargin)r.Parameter.GetValue(sprintf(x,varargin{:}));
% short cut for formatted output
o = @(y,x,varargin)fprintf(fid,'%22s: %s\n',y,sprintf(x,varargin{:}));

% do the output
o('File','%s',r.Parameter.Filename.Data);
o('Scan Duration','%s',g('IF_str_total_scan_time'));

% which trajectory did we use?
aq_mode = g('EX_ACQ_mode');
o('Acquisition Type','%s',aq_mode);
aq_mode = g('EX_ACQ_mode');
seq_type = g('EX_ACQ_imaging_sequence');
o('Sequence Type','%s',seq_type);
switch(seq_type)
    case 'FFE'
        o('Contrast Mode','%s',g('EX_FFE_ceffe'));
end
o('Scan Mode','%s',g('EX_ACQ_scan_mode'));
o('Readout Duration','%.3f ms',g('AQ`base:dur'));
switch(aq_mode)
    case 'spiral'
        o('No Interleaves','%i',g('VAL01_SPIRAL_nr_spirals'));
end
% flip angle, TR, TE
o('Max. Flip Angle','%.1f%s',r.Parameter.Scan.FlipAngle,char(176));
o('Repetition Time','%.1f ms',r.Parameter.Scan.TR);
if length(r.Parameter.Scan.TE)==1
    o('Echo Time','%.1f ms',r.Parameter.Scan.TE);
else
    % this we need for multi-echo scans
    for n=1:length(r.Parameter.Scan.TE)
        o(sprintf('Echo Time %i',n),'%.1f ms',r.Parameter.Scan.TE(n));
    end
end

% is it a dixon scan?
if ~strcmpi(g('EX_ACQ_dixon'),'no')
    dixon_type = g('EX_ACQ_dixon_acq');
    o('Dixon Type','%s',dixon_type); 
    switch(dixon_type)
        case 'multi-acquisition'
            o('Echo Time Increment','%.1f ms',g('VAL01_ACQ_delta_TE'));
    end
end

o('FOV','%.1fx%.1fx%.1f',r.Parameter.Scan.FOV); 
o('Acquisition Voxel Size','%.2fx%.2fx%.2f',r.Parameter.Scan.AcqVoxelSize);
o('Slice Gap','%.1f ',unique(r.Parameter.Scan.SliceGap));

o('SENSE','%.2fx%.2fx%.2f',r.Parameter.Scan.SENSEFactor);

if strcmpi(g('VAL01_DYN_enable'),'yes')
    o('Nr Dynamics','%i',g('VAL01_DYN_nr_scans'));
end
o('Nr Averages','%i',g('VAL01_ACQ_measurements'));

if ~strcmpi(g('EX_ACQ_B1_map'),'no')
    b1_type = g('EX_ACQ_B1_map_type');
    o('B1 Map Type','%s',b1_type);
    switch(b1_type)
        case 'DREAM'
            o('B1 Flip Angle','%.1f%s',g('EX_ACQ_B1_map_flip_angle'),char(176));
            o('STE First?','%s',g('EX_ACQ_B1_map_ste_first'));
    end
end

if ~strcmpi(g('EX_ACQ_B0_map'),'no')
    b0_type = g('EX_ACQ_B0_map_type');
    o('B0 Map Type','%s',b0_type);
    switch(b0_type)
        case 'multi-acquisition'
            o('Delta TE','%.1f ms',g('EX_ACQ_B0_map_delta_TE'));
    end
end

if ~strcmpi(g('VAL01_ACQ_fipri_mode'),'no')
    o('MRF File','%s',g('EX_ACQ_fipri_input_filename'));
    shots = g('VAL01_ACQ_fipri_shots');
    o('MRF Shots','%i',shots);
    TR = g('MPF_fipri_tr_arr'); TR = TR(1:shots);
    TE = g('MPF_fipri_te_arr'); TE = TE(1:shots);
    FA = g('VAL01_ACQ_fipri_angles'); FA = FA(1:shots);
    o('MRF TR Range','%.1f-%.1f',min(TR),max(TR));
    o('MRF TE Range','%.1f-%.1f',min(TE),max(TE));
    o('MRF Flipangle Range','%.1f-%.1f',min(FA),max(FA));
end
    
if write_to_file
    fclose(fid);
end


end

