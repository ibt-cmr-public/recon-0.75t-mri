function [wf,b0_map,dTE,ecs] = iterative_wfb0_mfi(data, varargin)
%% iterative_wfb0 Iterative water/fat/B0 estimation from multi-acquisition 
%% spiral data with multi-frequency interpolation
%
% Input:
% data: 10-d array (dim 1...3: spatial dimensions
%                   dim 9:  echoes,water,fat,deblurred fat
%                   dim 10: MFI frequencies)
%
% Required Parameters (supply as 'name',<value> pairs)
% 'mfi_frequencies'     Frequencies of MFI recon (kHz)
% 'tAcq'                Time-points of k-space samples (ms)
% 'TE'                  Echo time points (ms)
% 'fat_shifts'          The fat model chemical shift as a column-vector
%                       (1xN). (Default: Philips 7-peak fat model, unit: Hz/T)
% 'fat_areas'           The area of each peak as a column-vector
%
% Required parameters are best passed as a struct (see output of mf_spiral)
%
% Returns:
% wf:     b0-deblurred water/fat separated maps
% b0_map: final b0_map estimate (Hz)
% dTE:    difference in echo times (ms)
% ecs:    b0-deblurred echo images
% 
% Optional Parameters (supply as 'name',<value> pairs)
% 'field_strength'      The field strength at which data was acquired 
%                       (def: 0.75T, unit (T))
% 'sigma'               Specifies the amount of blurring of the b0 map
%                       along all three dimensions (columns) for each 
%                       iteration (rows).
%                       (Default: 16 iterations w/ progressively reduced
%                       blurring)
% 'plot'                Toggle if plotting should be performed for each
%                       iteration ('all'), only for the final result
%                       ('last') or not at all ('off'). Default: 'all'
% 'unwrap'              Function handle to unwrap phase-difference map with
%                       (default:
%                       @(x)unwrap_snaphu_mag(ones(size(x)).*exp(1i.*x)))
%                       Use `@(x)x` to disable unwrapping.
% 'offset_frequency'    Demodulate by offset frequency before water/fat
%                       separation (useful in case water/fat are swapped)
% 'reference_echo'      Nr. of echo used to correct for image phase (def:1)
% 'b0_map'              Initial b0 map (Def: empty = start from 0s)
%
% (c) Christian Guenthner, ETH Zurich, 2023

p = inputParser();
% these are actually required parameters.
% we're not using addRequired here to allow passing these as a struct
% (see mf_spiral.m)
p.addOptional('TE',[]);
p.addOptional('fat_shifts',[]);
p.addOptional('fat_areas',[]);
p.addOptional('mfi_frequencies',[]);
p.addOptional('tAcq',[]);

% optional parameters begin here
p.addOptional('field_strength',0.75);
% progressively reduce amount of B0-map blurring (heuristic approach).
% numbers are relative to the image size!
p.addOptional('sigma',[[linspace(60,10,10) 10 8 6 4 2 1]'.*ones(1,2) ones(16,1)*2]);
p.addOptional('plot','all',@(x)strcmpi(x,{'all','final','off'}));
p.addOptional('offset_frequency',0);
p.addOptional('reference_echo',1);
p.addOptional('b0_map',[]);
p.addOptional('unwrap',@(x)unwrap_snaphu_mag(ones(size(x)).*exp(1i.*x)));
p.KeepUnmatched=true;
p.parse(varargin{:});

TE = p.Results.TE;
dTE = TE(2)-TE(1);
sz = size(data);

% copy settings
offresonances = p.Results.fat_shifts * p.Results.field_strength;
areas = p.Results.fat_areas;
sigma = p.Results.sigma;

% output the model
fprintf('Fat Model:\n');
for n=1:length(offresonances)
    fprintf('%2i: %0.3f @ %7.3f Hz\n',n,areas(n),offresonances(n));
end
fmean_fat = sum(areas .* offresonances)./sum(areas);
[~,I] = max(areas);
fpeak_fat = offresonances(I);
fprintf('Weighted Average Fat Frequency: %f Hz\n\n',fmean_fat);
fprintf('Max. Intensity Fat Frequency: %f Hz\n\n',fpeak_fat);

% out-of-phase time of water/fat
Topwf = 1e3/fmean_fat/2;

% starting value for b0 map
if isempty(p.Results.b0_map)
    b0_map = zeros(sz(1:3));
else
    b0_map = p.Results.b0_map;
end

% correct by phase of TE1 image
%data = data .* exp(-1i * angle(data(:,:,:,p.Results.reference_echo)) -1i .* 2*pi* (b0_map + p.Results.offset_frequency) .* TE/1e3);

% determine water/fat encoding matrix 
encmat = [];
for n=1:length(TE)
    encmat(n,:) = cat(2, 1, sum( areas .* exp(1i .* 2*pi .* todim(TE(n),3) .* (offresonances) ./ 1e3),2)/sum(areas,2));
end

% plot iterations:
if ~strcmpi(p.Results.plot,'off')
    try;close(3);catch; end
    figure(3);
    set(gcf,'Position',[      680         558        1236         420]);
end

% permute data to make dimensions consistent with iterative_wfb0 (cartesian
% recon code)
data = permute(data,[1:3 9 4:8 10]);

% initialize water/fat and echo images with 0Hz demodulated images:
wf  = data(:,:,:,length(TE)+([1 3]),1,1,1,1,1,1);
ecs = data(:,:,:,1:length(TE)      ,1,1,1,1,1,1);

% and strip first element (not used for mfi)
data= data(:,:,:,:                 ,1,1,1,1,1,2:end);

% iterate
for iter=1:size(sigma,1)    
    % synthesize b0 free water fat image
    wfsynth  = permute(abs(wf),[4 1 2 3]);
    sz = size(wfsynth);
    sz(1) = length(TE);
    wfsynth = ipermute(reshape(encmat * wfsynth(:,:),sz),[4 1 2 3]);
    
    % determine difference between measured and synth
    di = ecs ./ wfsynth;

    % estimate b0
    if size(di,4)>2
        b0_map_hires = getf0(di,dTE,Topwf,p.Results.unwrap);
    else
        b0_map_hires = angle(di(:,:,:,2:end)./di(:,:,:,1));
        % unwrap phase-difference map using snaphu
        b0_map_hires = p.Results.unwrap(b0_map_hires);
        % calculate actual b0 in [Hz]
        b0_map_hires = b0_map_hires./dTE/2/pi*1e3;
    end

    % downsample
    if ~any(sigma(iter,:)==0)
        b0_low_res = imgaussfilt3(b0_map_hires,sigma(iter,:));
    else
        b0_low_res = b0_map_hires;
    end
    
    % update our map
    b0_map = b0_map + b0_low_res;

    % now get mfi interpolation coefficients for current b0_map
    Cmfi = mfi_interpolation_map(p.Results.mfi_frequencies, p.Results.tAcq, b0_map/1e3);
    Cmfi = permute(Cmfi, [2:10 1]);

    % perform multi-frequency interpolation step
    dmfi = sum(Cmfi .* data,10);

    % update our echo and water images
    wf  = dmfi(:,:,:,length(TE)+([1 3]));
    ecs = dmfi(:,:,:,1:length(TE)      );

    % update plot
    if strcmpi(p.Results.plot,'all') || (strcmpi(p.Results.plot,'last') && iter==size(sigma,1))
        b0h = b0_map_hires;
        b0l = b0_low_res;
        b0 = b0_map;

        sl = min(max(round(size(wf,3)/2),1),size(wf,3));
        subplot(1,5,1); 
        imagesc(abs(wf(:,:,sl,1))); title('Water');
        subplot(1,5,2); 
        imagesc(abs(wf(:,:,sl,2))); title('Fat');
        subplot(1,5,3); 
        imagesc(b0h(:,:,sl)); title('High-Res B0 Update')
        subplot(1,5,4); 
        imagesc(b0l(:,:,sl)); title('Low-Res B0 Update')
        subplot(1,5,5); 
        imagesc(b0(:,:,sl)); title('Current B0 Map')
        
        for n=1:5;subplot(1,5,n);
            axis('square'); 
            set(gca,'xtick',[],'ytick',[],'xticklabels',[],'yticklabels',[]);
        end

        colormap(gray(256))    
        drawnow
    end
end

end

function f0=getf0(data, dTE, wfs, unw)
    % get phase difference maps
    ddata = data(:,:,:,2:3)./data(:,:,:,1:2);
    
    % unwrap phase differences such that water/fat is unwrapped
    udata = unw(angle(ddata)./dTE.*wfs);

    % we have multiple images of the same thing. Determine the average
    % shift wrt to 2*pi and correct for it
    pishift = round(mean(mean(udata,1),2)/2/pi)*2*pi;
    pdata = (udata-pishift) ./ wfs / 2 /pi *1e3;

    % let's average over all results
    f0 = mean(pdata,4);
end