function shimoffset = get_shim_field(shims, fov, T, sz)
%% get_shim_field obtains the b0 offset from a set of linear shim values
%
% Warning: this function does not generalize to arbitrary angulations. It
% uses transformation matrices, however, still requires manual shifting,
% rotation and is only setup to work on square field of views as
% encountered in spiral scans. 
%
% Use with caution.
%
% Inputs: 
% shims  Linear Shim settings: use MR.Parameter.GetValue('PR_HS_shimtool_shims');
% fov    The in-plane field-of-view (fov = MR.Parameter.Scan.FOV(3))
% T      Transformation from mps to xyz coordinates (T = MR.Transform('mps','xyz'))
% sz     size of the b0 ma (only for a 2d slice!)
%
% Outputs:
% shimoffset in Hz
%
% (c) Christian Guenthner, ETH Zurich, 2023

shims = shims(1:3);

[X,Y] = meshgrid(1:sz(1),1:sz(2));

T = T * fov/sz(1);
% overwrite translation: we acquired our data in isocenter.
T(1,4)=0;
T(2,4)=-fov/2;
T(3,4)=+fov/2;
T(4,4)=1;
xyz = T* cat(1,X(:)',Y(:)',zeros(size(Y(:)')),ones(size(Y(:)')));

gammabar = 42577.469/1000; %[MHz / T]
shimoffset = gammabar * reshape(shims * xyz(1:3,:),sz(1:2)); %[mT / T * MHz = 1/ms]
% we need to manually flip the shimoffset map for it to be equal to our B0
% map angulation
shimoffset = flip(shimoffset,1);