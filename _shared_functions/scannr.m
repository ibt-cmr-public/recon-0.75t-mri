function [ file, matches ] = scannr( id, directory, type )
%SCANNR Returns the path scan with given ID	"scannr(id,directory,type)"
%
%EXAMPLE:
% A=scannr(id);     % find raw-file in current working directory
% A=scannr(id,dir); % find raw-file in directory "dir"
% A=scannr(id,[],type); % find file with ending "type" in current working directory
% A=scannr(id,dir,type); % find file with ending "type" in "directory"
%  
% Function calls without return argument print found file name(s) to console.
% Function calls with a single return argument fail, if multiple files
% match the scannr. 
% 
% [files,matches]=scannr(...) % returns two cell arrays:
%   files:   cell array of matching files
%   matches: struct cell array with meta information about the files
%       .matched_tokens matched tokens from the regex call. By default this
%       is the PARSED SCAN NAME (Abbreviated by Export)
%
%    + struct elements returned from "dir" function call
%       .name     the file name (without directory prefix)
%       .folder   the absolute path to the directory
%       .date     the create date
%       .bytes    file size
%
% please report bugs & bug-fixes to:
% Christian Guenthner, guenthner@biomed.ee.ethz.ch
% 
% Version History: 
%  2020-06-29 Updated Regex to match ending exactly
%  2019-09-27 Initial Version 


	if nargin<2 || isempty(directory); directory='./'; end
	if nargin<3 || isempty(type); type = 'raw'; end
    
	% allow id to be a string to remove ambiguis matches (e.g. 12_3)
    if ~ischar(id)
        id = sprintf('%i_\\d*_',id);
    end
    
    % append a / if we forgot to add it to the directory name
    if directory(end)~='/'
        directory = [directory '/'];
    end
        
    % get list of files
	filelist = dir(directory);
	if isempty(filelist)
		error('Empty directory');
	end
    
	% for each file test 
    
    reg_exp = sprintf('\\w*_\\w*_\\w*_%s(.*).%s(.*)',id,type);

    files = {};
    matches = [];
    cnt = 0;
    for n=1:length(filelist)
        %typ filename fl_26092019_2050148_4_1_200_07_ws015V4
        [B,A]=regexp(filelist(n).name,reg_exp,'match','tokens','ignorecase');
        if ~isempty(B) && isempty(A{1}{end})
            cnt=cnt+1;
            matches{cnt} = filelist(n);
            matches{cnt}.matched_tokens = A;
            files{cnt} = [directory filelist(n).name];
        end
    end

    if cnt==0
        file = [];
        matches = [];
        if nargout == 0
            fprintf('No file found.\n');
        end
    elseif cnt==1
        file = [directory matches{1}.name];
        if nargout == 0
            fprintf('Found one file:\n');
            fprintf('%i: %s\n',1,file);
        end
    else
        % multiple matches. If we have more than 1 output argument, assume
        % we can deal with multiple file outputs
        if nargout>1
            % return a cell with all filenames & match struct
            file=files;
        else
            fprintf('Multiple files match query:\n');
            for n=1:length(matches)
                fprintf('%i: %s\n',n,matches{n}.name);
            end
            % if no output specified, list matches
            if nargout ~= 0
                error('Multiple files matched the regular expression "%s"',reg_exp);
            end
        end
    end
end