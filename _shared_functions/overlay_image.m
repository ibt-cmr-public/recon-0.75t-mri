function overlay_image(d1,lim1,mask1,cmap1,d2,lim2,mask2,cmap2)
% display_image plot d1 with color limits lim1, mask mask1 and colormap
% cmap1 first then overlay d2 whereever mask2 is 1 with limits lim2 and
% colormap cmap2
% 
% (c) Christian Guenthner, ETH Zurich, 2023 

% 
mask1 = double(mask1);
mask1(mask1==0)=nan;

d1(~isfinite(d1))=0;
d2(~isfinite(d2))=0;

% map data linearly to our plot range and clamp between 0 and 1
% make sure we map it to exactly as many numbers as there are colors in
% cmap1/2.
d1 = floor(min(min(max((d1 - lim1(1))./diff(lim1),0),1) * size(cmap1,1),size(cmap1,1)-1))/size(cmap1,1);
d2 = floor(min(min(max((d2 - lim2(1))./diff(lim2),0),1) * size(cmap2,1),size(cmap2,1)-1))/size(cmap2,1);

% merge colormaps
cmap = cat(1,cmap1,cmap2);

% merge data
d = d1/2 .* mask1;
d(mask2==1) = d2(mask2==1)/2 + 0.5;

% plot
imagesc(d);
axis square
set(gca,'visible','off')
colormap(gca,cmap)
caxis([0 1])