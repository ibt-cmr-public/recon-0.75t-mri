function [b1p,r,S] = dream_b1p(filename, S)
%% dream_b1p loads a DREAM B1+ scan from raw data and reconstructs parameter maps
%
r = load_and_normalize(filename);
if nargin<2 || isempty(S)
    % we didn't supply sensitivities. estimate them
    S = estim_sensitivities(r.Data);
end
% roemer coil combination
reco = sum(r.Data .* S,4);

for n=1:size(reco(:,:,:),3)
    reco(:,:,n) = imgaussfilt(abs(reco(:,:,n)),5);
end

% get the used dream flip angle
dream_fa = r.Parameter.GetValue('RC_b1_map_flip_angle');
% reconstruct estimated flip angle map
famap = DREAM_fa(reco,7);
% get b1 by deviding map by nominal FA.
b1p = famap ./ dream_fa;
end


function d=DREAM_fa(data,dim)
%% Estimates the flip angle of DREAM B1+ mapping sequence
%
% from MRM 68:1517-1526 (2012)
% Kay Nehrke & Peter Boernert
    I = [dim setdiff(1:max(ndims(data),dim),dim)];
    d = permute(data,I);
    sz = size(d);
    d = reshape(d,2,[]);
    
    d = atan(sqrt(abs(2*d(2,:)./d(1,:)))) / pi * 180;
    sz(1)=1;
    d = ipermute(reshape(d,sz),I);            
end
