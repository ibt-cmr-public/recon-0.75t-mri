function out = mf_spiral(file, varargin)
%% mf_spiral performs water/fat seperation in k-space,
%% deblurrs the fat channel, and applies multi-frequency demodulation
% Performs demoudulation with a set of off-resonance frequencies 
% before water/fat separation in k-space. This yields for each demodulation 
% frequency a water/fat separated image, which can be used for itertive
% water/fat separation, b0-estimation and b0-deblurring using
% multi-frequency interpolation.
%
% Input:
% file          filename to Philips .raw data
% 
% Optional Parameter:
% 'mfi_frequencies'     The demodulation frequencies for MFI (kHz)
% 'field_strength'      The field strength at which data was acquired 
%                       (def: 0.75T)
% 'fat_shifts'          The fat model chemical shift as a column-vector
%                       (1xN). (Default: Philips 7-peak fat model)
% 'fat_areas'           The area of each peak as a column-vector
%
% Output structure:
% .recon_data   reconstructed echo & water/fat spiral images with multi-
%               frequency demodulation applied (not yet recombined)
%               dim 10: 1..#ecs = echo images
%                       #ecs+1  = water images
%                       #ecs+2  = fat images
%                       #ecs+3  = deblurred fat images
%               #ecs = number of echoes.
% 

% parse inputs
p = inputParser;
p.addOptional('fat_frequencies',[]);
p.addOptional('fat_areas',[]);
p.addOptional('field_strength',0.75);
p.addOptional('mfi_frequencies',linspace(-200,100,19)'/1000); %kHz
p.addOptional('remove_oversampling','YES');
p.parse(varargin{:});

% copy them to local variables
areas = p.Results.fat_areas;
offresonance = p.Results.fat_frequencies;
mfi_frequencies = p.Results.mfi_frequencies;

% load raw data with mrecon
MR = MRecon(file);
MR.Parameter.Recon.CoilCombination='NO';
MR.Parameter.Recon.GeometryCorrection='NO';
MR.Parameter.Recon.RemoveMOversampling=p.Results.remove_oversampling;
MR.Parameter.Recon.RemovePOversampling=p.Results.remove_oversampling;
MR.ReadData;
MR.RandomPhaseCorrection;
MR.RemoveOversampling;
MR.PDACorrection;
MR.DcOffsetCorrection;
MR.MeasPhaseCorrection;
MR.SortData;                            

% if the user did not specify a fat model, then try to load it from the raw
% data
if isempty(offresonance) || isempty(areas)
    warning('Obtaining Fat Model from Philips Scanner Export');
    
    offresonance = MR.Parameter.GetValue('RC_dixon_fat_freqs');
    areas = MR.Parameter.GetValue('RC_dixon_fat_weights');    
end

% and print it
fprintf('Fat Model:\n');
for n=1:length(offresonance)
    fprintf('%2i: %0.3f @ %7.3f Hz\n',n,areas(n),offresonance(n));
end
fprintf('Weighted Average Fat Frequency: %f Hz\n\n',sum(areas .* offresonance)./sum(areas));

% this is the raw k-space data
data = MR.Data{1};

% scale to noise level
noise = std(MR.Data{5},[],1);
data = data ./ noise;

% get echo times
TE = unique(MR.Parameter.Labels.TE);
if strcmpi(MR.Parameter.GetValue('EX_ACQ_dixon'),'yes')
    dTE = MR.Parameter.GetValue('EX_ACQ_dixon_delta_TE');
    echoes = MR.Parameter.GetValue('EX_ACQ_dixon_acquisitions');
elseif strcmpi(MR.Parameter.GetValue('EX_ACQ_b0_map'),'yes')
    dTE = MR.Parameter.GetValue('EX_ACQ_b0_map_delta_TE');
    echoes = 2;
end

% obtain time-points for each k-space point
tacq = ((1:size(MR.Data{1},1))-1) * MR.Parameter.GetValue('AQ`base:interval');

% perform water/fat separation in k-space
% demodulate all images for different offresonance frequencies and
% append them to the current data (the first set is not demodulated!)
TEimage = todim([TE+(0:(echoes-1))*dTE],10);
data = cat(3,data,data.* exp(-1i .* todim(mfi_frequencies,3) .* (todim(tacq,1) + TEimage) .* 2 * pi));

% reorder 
order = [10 2 3 4 1];
order = [order setdiff(1:10,order)];
datap = permute(data,order);
sz = size(datap);
datap = reshape(datap,sz(1),prod(sz(2:4)),sz(5));

% determine water/fat encoding matrix for each k-space point
encmat = [];
for n=1:echoes
    encmat(n,:,:) = cat(2,ones(1,1,length(tacq)),sum( areas .* exp(1i .* 2*pi .* todim(TE+dTE*(n-1)+tacq,3) .* offresonance ./ 1000),2)/sum(areas,2));
end

% Invert the water/fat encoding problem separately for each time-point
tmp=zeros([2 size(datap,2) size(datap,3)]);
for t=1:size(encmat,3)
    tmpd = datap(:,:,t);    
    tmp(:,:,t) = pinv(encmat(:,:,n)) * tmpd;    
end

% reshape.
sz(1)=2;
tmp = reshape(tmp,sz);
tmp = ipermute(tmp,order);

% deblurr fat channel using fat spectrum
tmp(:,:,:,:,:,:,:,:,:,end+1) = tmp(:,:,:,:,:,:,:,:,:,2) .* sum(areas) ./sum( areas .* exp(1i .* 2*pi .* todim(tacq,1) .* offresonance ./ 1000),2 );
tmp = cat(10,data,tmp);

% put everything as before into the 10th dimension
sz = size(tmp);
sz(10)=sz(10)*sz(3);
sz(3) = 1;
tmp = reshape(permute(tmp,[1 2 11 4:10 3]),sz);

% replace data
MR.Data{1} = tmp;

% continue with recon
MR.GridData;
MR.RingingFilter;
MR.ZeroFill;
MR.K2IM;
MR.EPIPhaseCorrection;
MR.K2IP;
MR.GridderNormalization;
MR.SENSEUnfold;
MR.PartialFourier;
MR.ConcomitantFieldCorrection;
MR.DivideFlowSegments;
MR.CombineCoils;
MR.Average;
MR.GeometryCorrection;
MR.RemoveOversampling;
MR.FlowPhaseCorrection;
MR.ReconTKE;
MR.ZeroFill;
MR.RotateImage;
% end of "normal" recon

% reshape frames to ...x(echoes/water/fat/deblurred-fat)x(MFI-freqs) format
data = MR.Data{1};
sz = size(data);
sz(9)=length(TEimage)+3;
sz(10)=sz(10)/sz(9);
data = reshape(data,sz);

% and collate data for output:
out = struct();
out.filename = file;
% reconstructed images
out.Data = data;            
out.mfi_frequencies = mfi_frequencies; % Hz 
% fat model
out.fat_areas = areas;
out.fat_shifts = offresonance / p.Results.field_strength; % chemical shift in Hz / T
% echo time and sampling time-points
out.TE = TEimage;
out.tAcq = tacq;
out.HWResonanceFreq = MR.Parameter.GetValue('HW_resonance_freq');
out.T = MR.Transform('ijk','xyz');
