function MR = load_and_normalize_spiral(file, offresonance)
%% load_and_normalize short wrapper function to load Philip's raw data files
%% and to perform spiral data demodulation with a given off-resonance freq.
% Normalizes raw data by noise channel, disables geometry correct and
% coil-combination, and demodulates spiral data
%
% This function requires a valid MRecon license!
%
% (c) Christian Guenthner, ETH Zurich, 2023

MR = MRecon(file);
MR.Parameter.Recon.CoilCombination='NO';
MR.Parameter.Recon.GeometryCorrection='NO';
MR.ReadData;
MR.RandomPhaseCorrection;
MR.RemoveOversampling;
MR.PDACorrection;
MR.DcOffsetCorrection;
MR.MeasPhaseCorrection;
MR.SortData;                            

if iscell(MR.Data)
    noiselevel = std(MR.Data{5},[],1);
    MR.Data{1} = MR.Data{1} ./ noiselevel;
end
%
tacq = ((1:size(MR.Data{1},1))-1) * MR.Parameter.GetValue('AQ`base:interval');
%df_fat = 121/1000;
MR.Data{1} = MR.Data{1} .* exp(1i .* 2*pi * todim(tacq,1) * offresonance / 1000);

%
MR.GridData;
MR.RingingFilter;
MR.ZeroFill;
MR.K2IM;
MR.EPIPhaseCorrection;
MR.K2IP;
MR.GridderNormalization;
MR.SENSEUnfold;
MR.PartialFourier;
MR.ConcomitantFieldCorrection;
MR.DivideFlowSegments;
MR.CombineCoils;
MR.Average;
MR.GeometryCorrection;
MR.RemoveOversampling;
MR.FlowPhaseCorrection;
MR.ReconTKE;
MR.ZeroFill;
MR.RotateImage;

MR.Data = MR.Data{1};

end