function display_image(d,p,mask)
% display_image plot real image d with p the upper-bound percentile for the
% color axis
% 
if nargin<3 || isempty(mask); mask=1; end
mask = double(mask);
mask(mask==0)=nan;

d(~isfinite(d))=0;

imagesc(d .* mask);
axis square
set(gca,'visible','off')
colormap(gca,gray(256))
caxis([0 quantile(abs(d .* mask),p,'all')])