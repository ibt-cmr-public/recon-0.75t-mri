function sz = sizen(data,n)
%% like size, only for at least the first n dimensions
    nd = ndims(data);
    sz = ones(1,max(n,nd));
    sz(1:nd) = size(data);    
end

